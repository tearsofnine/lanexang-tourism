package com.ipanda.lanexangtourism.other;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.google.android.gms.maps.MapView;

public class SetFragmentMenu {

    public SetFragmentMenu() {

    }

    public MapView SetLayout(View view, Context context, Bundle savedInstanceState, String[] NameMenuCategory, int MenuID, Activity mActivity){

        TextView textViewTitle = (TextView) view.findViewById(R.id.text_titleID);
        textViewTitle.setText(NameMenuCategory[(MenuID-1)]);

        TextView textViewTopic = (TextView) view.findViewById(R.id.text_topicID);

        textViewTopic.setText(context.getResources().getString(R.string.TextSearch)+" "+NameMenuCategory[(MenuID-1)]);


        ImageView imageViewCategory = (ImageView) view.findViewById(R.id.img_menu_categoryID);

        String imageViewCategoryID = "category_" + MenuID ;
        int imageViewCategoryResID = context.getResources().getIdentifier(imageViewCategoryID, "drawable", context.getPackageName());

        imageViewCategory.setImageResource(imageViewCategoryResID);

        MapView mapView = (MapView) view.findViewById(R.id.mapView);

        SetGoogleMapMin setGoogleMapMin = new SetGoogleMapMin(mapView,savedInstanceState,mActivity,false,-1,0,0);

        return mapView;

    }

}
