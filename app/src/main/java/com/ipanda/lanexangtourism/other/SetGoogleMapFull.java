package com.ipanda.lanexangtourism.other;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ipanda.lanexangtourism.model.ModelItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class SetGoogleMapFull extends Activity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private float _zoom = 0f;

    protected LocationManager locationManager;
    protected Activity _mActivity;
    public MapView _mapView;
    private boolean _useNear;
    private int _TypeItem;
    private Circle circle;
    private LatLng DummyNearLocation;
    private LatLng _itemLocation;
    private int local_distance =0;
    private UiSettings uiSettings;
    private  ArrayList<ModelItem> _modelItems;
    private ArrayList<Marker> mapMark;
    private int _progressChangedValue;
    private String _current_iditem;


    public SetGoogleMapFull(MapView mapView, Bundle savedInstanceState, Activity mActivity, int TypeItem, ArrayList<ModelItem> modelItems,int progressChangedValue,float zoom,LatLng itemLocation,String current_iditem){
        _mActivity = mActivity;
        _mapView = mapView;
//        _useNear =useNear;
        _TypeItem = TypeItem;
        _modelItems= modelItems;
        _progressChangedValue = progressChangedValue;
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        _zoom = zoom;
        _itemLocation =itemLocation;
        _current_iditem = current_iditem;
    }

    @Override
    public void onLocationChanged(Location location) {
        //updateLocationUI();
        //getDeviceLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocationPermission();
        updateLocationUI();
        mMap.clear();
        addMarker(_progressChangedValue);
//        for (Marker item : mapMark) {
//            if( GetDistance(DummyNearLocation,item.getPosition()) <= (local_distance==0?2500:local_distance)){
//                if(!item.getTitle().equals(_TypeItem))
//                    item.setVisible(false);
//            }
//        }
        //mapMark.get(mark_current_id).setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_1", 80, 80)));

    }

    private void addMarker(int distance){
        mapMark =new ArrayList<Marker>();
        int i =0;
        int distance_mylocation = distance*1000;
        for(ModelItem item : _modelItems)
        {
            if(item.getModelTypeOfItem().getTypeID() == _TypeItem)
            {
                if(item.getItemID().equals(_current_iditem))
                {
                    mapMark.add(mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLatitude(), item.getLongitude())).title(item.getModelTypeOfItem().getTypeID() + "").snippet(i + "")
                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + item.getModelTypeOfItem().getTypeID(), 80, 80)))));

                }
                else
                {
                    mapMark.add(mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLatitude(), item.getLongitude())).title(item.getModelTypeOfItem().getTypeID() + "").snippet(i + "")
                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + item.getModelTypeOfItem().getTypeID(), 32, 32))).visible(distance_mylocation >= item.getDistance_metr()?true:false)));
                }
            }
            else
            {
                mapMark.add(mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLatitude(), item.getLongitude())).title(item.getModelTypeOfItem().getTypeID() + "").snippet(i + "")
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + item.getModelTypeOfItem().getTypeID(), 32, 32))).visible(false)));
            }
            i++;
        }

    }
    private Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(_mActivity.getResources(),_mActivity.getResources().getIdentifier(iconName, "drawable", _mActivity.getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, dpToPx(width), dpToPx(height), false);
        return resizedBitmap;
    }
    public int dpToPx(int dp) {
        float density = _mActivity.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(_mActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            locationManager = (LocationManager) _mActivity.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            updateLocationUI();
            getDeviceLocation(_zoom);
        } else {
            ActivityCompat.requestPermissions(_mActivity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
    public void getDeviceLocation(float zoom_level) {

//        try {
//            if (mLocationPermissionGranted) {
//                Location locationResult ;
//                locationResult= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                if(locationResult == null)
//                    locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                DummyNearLocation = new LatLng(locationResult.getLatitude(), locationResult.getLongitude());
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(locationResult.getLatitude(),
//                                locationResult.getLongitude()),zoom_level));
//                circle = mMap.addCircle(new CircleOptions().center(DummyNearLocation).radius((local_distance==0?25000:local_distance*1000)).strokeWidth(2).strokeColor(Color.BLUE).fillColor(Color.parseColor("#500084d3")));
//                circle.setVisible(false);
//                _mapView.invalidate();
//            }
//        } catch(SecurityException e){
//            Log.e("Exception: %s", e.getMessage());
//        }
        DummyNearLocation = _itemLocation;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(_itemLocation,zoom_level));
        circle = mMap.addCircle(new CircleOptions().center(DummyNearLocation).radius((local_distance==0?25000:local_distance*1000)).strokeWidth(2).strokeColor(Color.BLUE).fillColor(Color.parseColor("#500084d3")));
        circle.setVisible(false);
        _mapView.invalidate();
    }

    @SuppressLint("MissingPermission")
    public void CheckCircle() {
        if(circle.isVisible()){
            circle.setVisible(false);
            mMap.setMyLocationEnabled(false);
        }
        else {
            circle.setVisible(true);
            mMap.setMyLocationEnabled(true);
        }
    }

    public void UpdateMark(int distance) {
        mapMark.clear();
        mMap.clear();
        addMarker(distance);
        circle.remove();
        circle = mMap.addCircle(new CircleOptions().center(DummyNearLocation).radius(distance*1000).strokeWidth(2).strokeColor(Color.BLUE).fillColor(Color.parseColor("#500084d3")));

        _mapView.invalidate();
    }
    public void UpdateCircle(int distance) {
        local_distance = distance;
        circle.remove();
        circle = mMap.addCircle(new CircleOptions().center(DummyNearLocation).radius(distance*1000).strokeWidth(2).strokeColor(Color.BLUE).fillColor(Color.parseColor("#500084d3")));
        _mapView.invalidate();
    }
    public Marker SearchMark(String idOfItem)
    {
        Marker marker = null;
        for (Marker item : mapMark) {
            if(_modelItems.get(Integer.parseInt(item.getSnippet())).getItemID().equals(idOfItem)) {
                return item;
            }
        }
        return null;
    }
    public void ShowMarker(String TypeofItem,int distance,int state,String current_item) {
        String[] TypeofItemA = TypeofItem.split("\\|");
        int distance_mylocation = distance*1000;

        for(String itemA : TypeofItemA) {
            for (Marker item : mapMark) {
                if (item.getTitle().equals(itemA)/* && !itemA.equals(String.valueOf(_TypeItem))*/) {
                    if (item.isVisible() && distance_mylocation >= _modelItems.get(Integer.parseInt(item.getSnippet())).getDistance_metr()) {
                        if(_modelItems.get(Integer.parseInt(item.getSnippet())).getItemID().equals(current_item))
                            item.setVisible(true);
                        else
                            item.setVisible(false);
                    }
                    else {
                        if(state == 1)
                            item.setVisible(distance_mylocation >= _modelItems.get(Integer.parseInt(item.getSnippet())).getDistance_metr() ? true : false);
                    }
                }
            }
        }
        //Toast.makeText(_mActivity,mapMark.size() , Toast.LENGTH_SHORT).show();
    }
    public boolean checknear(int distance)
    {
        int distance_mylocation = distance*1000;
        boolean checknear_state = false;
        for (Marker item : mapMark) {
            if(distance_mylocation <= _modelItems.get(Integer.parseInt(item.getSnippet())).getDistance_metr()) {
                checknear_state = true;
            }
            else
                checknear_state = false;
        }
        return checknear_state;
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {

                mMap.setIndoorEnabled(true);
                uiSettings = mMap.getUiSettings();
                uiSettings.setMapToolbarEnabled(false);
                uiSettings.setCompassEnabled(false);

                mMap.setMinZoomPreference(10.0f);
                mMap.setMaxZoomPreference(17.0f);
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                switch (_mActivity.getSharedPreferences("PREF_MAP_TYPE", Context.MODE_PRIVATE).getString("MAP_TYPE", "standard")) {
                    case "standard":
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        break;
                    case "satellite":
                        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        break;
                    case "hybrid":
                        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        break;

                }
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public int GetDistance(LatLng mylocation,LatLng marker){
        Location loc1 = new Location("");
        loc1.setLatitude(mylocation.latitude);
        loc1.setLongitude(mylocation.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(marker.latitude);
        loc2.setLongitude(marker.longitude);

        return (int) loc1.distanceTo(loc2)/1000;
    }

    public LatLng getRandomLocation(LatLng point, int radius) {

        List<LatLng> randomPoints = new ArrayList<>();
        List<Float> randomDistances = new ArrayList<>();
        Location myLocation = new Location("");
        myLocation.setLatitude(point.latitude);
        myLocation.setLongitude(point.longitude);

        //This is to generate 10 random points
        for(int i = 0; i<10; i++) {
            double x0 = point.latitude;
            double y0 = point.longitude;

            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            randomPoints.add(randomLatLng);
            Location l1 = new Location("");
            l1.setLatitude(randomLatLng.latitude);
            l1.setLongitude(randomLatLng.longitude);
            randomDistances.add(l1.distanceTo(myLocation));
        }
        //Get nearest point to the centre
        int indexOfNearestPointToCentre = randomDistances.indexOf(Collections.min(randomDistances));
        return randomPoints.get(indexOfNearestPointToCentre);
    }
}
