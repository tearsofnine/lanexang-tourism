package com.ipanda.lanexangtourism.other;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;

import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class SetGoogleMapMin extends Activity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;
    private static final int DEFAULT_ZOOM = 17;

    protected LocationManager locationManager;
    protected Activity _mActivity;
    public MapView _mapView;
    private boolean _useNear;
    private int _TypeItem;
    private double _Lat, _Lng;

    public SetGoogleMapMin(MapView mapView, Bundle savedInstanceState, Activity mActivity, boolean useNear, int TypeItem, double Lat, double Lng) {
        _mActivity = mActivity;
        _mapView = mapView;
        _useNear =useNear;
        _TypeItem = TypeItem;
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        _Lat = Lat;
        _Lng = Lng;

    }

    public void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(_mActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            locationManager = (LocationManager) _mActivity.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            updateLocationUI();
            getDeviceLocation();
        } else {
            ActivityCompat.requestPermissions(_mActivity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
    public void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {


                mMap.setIndoorEnabled(true);
                UiSettings uiSettings = mMap.getUiSettings();

                uiSettings.setRotateGesturesEnabled(false);
                uiSettings.setScrollGesturesEnabled(false);
                uiSettings.setTiltGesturesEnabled(false);
                uiSettings.setZoomGesturesEnabled(false);
                uiSettings.setMapToolbarEnabled(false);
                //mMap.setMyLocationEnabled(true);
//                mMap.setOnMyLocationChangeListener((GoogleMap.OnMyLocationChangeListener) this);
                //mMap.setOnMyLocationChangeListener(null);

                //mMap.setMyLocationEnabled(true);
                //mMap.getUiSettings().setMyLocationButtonEnabled(true);

            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public void getDeviceLocation() {

        try {
            if (mLocationPermissionGranted) {
                Location locationResult ;
                locationResult= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(locationResult == null)
                    locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                mMap.clear();
                if(_useNear) {
                    mMap.addMarker(new MarkerOptions().position(
                            new LatLng(_Lat, _Lng))
                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_"+_TypeItem, 64, 64)))
                    );
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(_Lat, _Lng), DEFAULT_ZOOM));
                }
                else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(locationResult.getLatitude(),
                                    locationResult.getLongitude()), DEFAULT_ZOOM));
                }
                _mapView.invalidate();
                }
            } catch(SecurityException e){
                Log.e("Exception: %s", e.getMessage());
            }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        getLocationPermission();
        updateLocationUI();
        getDeviceLocation();

    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocationUI();
        getDeviceLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(_mActivity.getResources(),_mActivity.getResources().getIdentifier(iconName, "drawable", _mActivity.getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, dpToPx(width),dpToPx(height), false);
        return resizedBitmap;
    }
    public int dpToPx(int dp) {
        float density = _mActivity.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
}
