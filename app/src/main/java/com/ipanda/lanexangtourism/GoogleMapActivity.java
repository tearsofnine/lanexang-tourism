package com.ipanda.lanexangtourism;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskGoogleMap;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceGoogleMap;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.util.AddFavorite;
import com.ipanda.lanexangtourism.other.SetGoogleMapFull;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GoogleMapActivity extends AppCompatActivity implements CallBackServiceGoogleMap, NavigationView.OnNavigationItemSelectedListener {

    private MapView mapView;
    private ImageView Btn_distance,img_favorite;
    private ImageView Btn_close_distance;
    private ImageView img_item_list_great_places;
    private CardView card_distance;
    private SeekBar distance_bar;
    private TextView text_distance;
    private TextView title_item;
    private TextView topic_item;
    private TextView txtdistance;
    private RelativeLayout card_desc;
    private Button[] buttons_array;
    private SetGoogleMapFull setGoogleMapFull;
    private Marker previousMarker = null;
    private ArrayList<ModelItem> modelItems = null;
    private String url_json = "/lanexang_tourism/user/GetData/getDataItme?idTypeOfItems=ALL";
    private int itemType;
    private int numFilter = 5;
    private int progressChangedValue =25;
    private Bundle _savedInstanceState;
    private float zoom=0;
    private String[] NameMenuCategory;
    private TextView[] TextView_Array;
    private LinearLayout[] ClickNavigation_Array;
    private int numMenu = 8;
    private ProgressDialog alertDialog = null;
    private String app_mode;
    private boolean current_item = false;
    private ModelItem modelItem_decall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_google_map);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");

        NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
        itemType = getIntent().getExtras().getInt("itemType");
        current_item = getIntent().getExtras().getBoolean("current_item");
        modelItem_decall = this.getIntent().getParcelableExtra("ITEMS_DETAIL");

        if(itemType == 1 || itemType == 2 || itemType == 3 || itemType == 7)
            toolbar_title.setText(NameMenuCategory[1]);
        else if(itemType == 5)
            toolbar_title.setText(NameMenuCategory[3]);
        else if(itemType == 6)
            toolbar_title.setText(NameMenuCategory[4]);
        else if(itemType == 8)
            toolbar_title.setText(NameMenuCategory[2]);
        else if(itemType == 9 || itemType == 11 || itemType == 12 || itemType == 13)
            toolbar_title.setText(NameMenuCategory[5]);
        else
            toolbar_title.setText(R.string.great_places);

        card_distance = (CardView) findViewById(R.id.card_distance);
        card_distance.setVisibility(View.INVISIBLE);

        card_desc = (RelativeLayout) findViewById(R.id.card_desc);
        card_desc.setVisibility(View.INVISIBLE);

        String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
        url_json +="&Lat="+getIntent().getExtras().getString("Lat")+"&Lng="+getIntent().getExtras().getString("Lng")+"&LatU="+getIntent().getExtras().getString("LatU")+"&LngU="+getIntent().getExtras().getString("LngU");
        new AsyncTaskGoogleMap(this).execute(getResources().getString(R.string.host_ip)+url_json+ "&UserId=" + UserId);

    }

    public void Initializing() {

        mapView = (MapView) findViewById(R.id.GooglemapView);
        double lat = Double.parseDouble(getIntent().getExtras().getString("Lat"));
        double lng = Double.parseDouble(getIntent().getExtras().getString("Lng"));

        setGoogleMapFull = new SetGoogleMapFull(mapView,_savedInstanceState,this,itemType, modelItems,progressChangedValue,12.5f,new LatLng(lat,lng),modelItem_decall == null?"-1":modelItem_decall.getItemID());

        Btn_distance = (ImageView) findViewById(R.id.btn_distance);
        Btn_close_distance = (ImageView) findViewById(R.id.ic_close_distance);

        distance_bar = (SeekBar) findViewById(R.id.distance_bar);
        text_distance = (TextView) findViewById(R.id.text_distance);
        title_item = (TextView) findViewById(R.id.title_item);
        topic_item = (TextView) findViewById(R.id.topic_item);
        txtdistance = (TextView)findViewById(R.id.distance);
        img_favorite = findViewById(R.id.img_favorite);

        img_item_list_great_places = (ImageView) findViewById(R.id.img_item_list_great_places);

        buttons_array =new Button[numFilter];
        for(int i=0; i<numFilter; i++) {
            String btn_filterID = "btn_filter_" + (i+1) ;
            int btn_filterResID = getResources().getIdentifier(btn_filterID, "id", getPackageName());
            buttons_array[i]=(Button) findViewById(btn_filterResID);
            buttons_array[i].setTag(0);
        }

        TextView_Array = new TextView[numMenu];
        ClickNavigation_Array = new LinearLayout[numMenu];
        for(int i=0; i<numMenu; i++) {

            String TextViewID = "TvCategory_" + (i + 1);
            int TextViewResID = getResources().getIdentifier(TextViewID, "id", getPackageName());

            String LinearLayoutID = "menu_category_" + (i + 1);
            int LinearLayoutResID = getResources().getIdentifier(LinearLayoutID, "id", getPackageName());

            TextView_Array[i] = (TextView) findViewById(TextViewResID);
            ClickNavigation_Array[i] = (LinearLayout) findViewById(LinearLayoutResID);

        }

        CheckItemTpye(itemType);
        if(modelItem_decall != null) {
            if (modelItem_decall.getModelFilePaths().getPathImg().equals("") || modelItem_decall.getModelFilePaths().getPathImg() == "null") {
                img_item_list_great_places.setImageResource(R.drawable.typeitem_9);
            } else {
                Picasso.get().load(modelItem_decall.getModelFilePaths().getPathImg()).into(img_item_list_great_places);
            }

            title_item.setText(modelItem_decall.getTopicTH());
            topic_item.setText(getResources().getString(R.string.text_address)+" "+modelItem_decall.getAddressTH());
            txtdistance.setText(modelItem_decall.getDistance());


            img_favorite.setColorFilter(null);
            if (modelItem_decall.isFavorites()){
                img_favorite.setColorFilter(getResources().getColor(R.color.red));
            } else {
                img_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
            }
        }


    }

    private void SetClickFilter(int select){

        if(buttons_array[select].getTag().equals(0)) {
            buttons_array[select].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            buttons_array[select].setTextColor(getResources().getColor(R.color.colorWhite));
            buttons_array[select].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_edge_s, null));
            buttons_array[select].setTag(1);
        }
        else {
            buttons_array[select].setBackgroundColor(getResources().getColor(R.color.colorWhite));
            buttons_array[select].setTextColor(getResources().getColor(R.color.colorPrimary));
            buttons_array[select].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_edge, null));
            buttons_array[select].setTag(0);
        }
    }
    public void CheckItemTpye(int TypeItem){
        switch (TypeItem) {
            case 1:
            case 2:
            case 3:
            case 7:
                SetClickFilter(2);
                break;
            case 6:
                SetClickFilter(0);
                break;
            case 8:
                SetClickFilter(3);
                break;
            case 9:
            case 11:
            case 12:
            case 13:
                SetClickFilter(1);
                break;
        }
    }
    public void ClickFilter(View view) {
        switch (view.getId()) {
            case R.id.btn_filter_1:
                SetClickFilter(0);
                setGoogleMapFull.ShowMarker("6",progressChangedValue,Integer.parseInt(buttons_array[0].getTag().toString()),modelItem_decall == null?"-1":modelItem_decall.getItemID() );
                break;
            case R.id.btn_filter_2:
                SetClickFilter(1);
                setGoogleMapFull.ShowMarker("9|11|12|13",progressChangedValue,Integer.parseInt(buttons_array[1].getTag().toString()),modelItem_decall == null?"-1":modelItem_decall.getItemID() );
                break;
            case R.id.btn_filter_3:
                SetClickFilter(2);
                setGoogleMapFull.ShowMarker("1|2|3|7",progressChangedValue,Integer.parseInt(buttons_array[2].getTag().toString()),modelItem_decall == null?"-1":modelItem_decall.getItemID() );
                break;
            case R.id.btn_filter_4:
                SetClickFilter(3);
                setGoogleMapFull.ShowMarker("8",progressChangedValue,Integer.parseInt(buttons_array[3].getTag().toString()),modelItem_decall == null?"-1":modelItem_decall.getItemID() );
                break;
            case R.id.btn_filter_5:
                SetClickFilter(4);
                setGoogleMapFull.ShowMarker("5",progressChangedValue,Integer.parseInt(buttons_array[4].getTag().toString()),modelItem_decall == null?"-1":modelItem_decall.getItemID() );
                break;
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        ArrayList<ModelItem> ItemhasBack = modelItems;
        returnIntent.putParcelableArrayListExtra("ItemhasBack",ItemhasBack);
        returnIntent.putExtra("currentItem",current_item);
        returnIntent.putExtra("hasBackPressed",true);
        setResult(Activity.RESULT_OK,returnIntent);
    }

    private Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, dpToPx(width), dpToPx(height), false);
        return resizedBitmap;
    }
    public int dpToPx(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }
    @Override
    public void onCallService() {
    }
    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage(""+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }
    @Override
    public void onRequestCompleteListener(ArrayList<ModelItem> modelItems) {
        if (modelItems != null && modelItems.size() > 0) {
            Log.i("Check data", "" + modelItems.size());
            this.modelItems = modelItems;

            Initializing();

            SetNameMenu();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ImageView menuRight = (ImageView) findViewById(R.id.menuRight);
            menuRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.closeDrawer(GravityCompat.END);
                    } else {
                        drawer.openDrawer(GravityCompat.END);
                    }
                }
            });

            card_desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ItemsDetailActivity.class);
                    intent.putExtra("ITEMS_DETAIL", modelItems.get(Integer.parseInt(previousMarker.getSnippet())));
                    startActivityForResult(intent,1);
//                    Toast.makeText(getApplicationContext(),modelItems.get(Integer.parseInt(previousMarker.getSnippet())).getItemID(), Toast.LENGTH_SHORT).show();
                }
            });
            Btn_close_distance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Btn_distance.setImageResource( R.drawable.ic_distance);
                    Btn_distance.setTag("ic_distance");
                    card_distance.setVisibility(View.INVISIBLE);
                    setGoogleMapFull.CheckCircle();

                    if (previousMarker != null) {
                        previousMarker.setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + previousMarker.getTitle(), 32, 32)));

                        previousMarker = null;
                    }
                }
            });

            Btn_distance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(previousMarker != null) {
                        previousMarker.setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + previousMarker.getTitle(), 32, 32)));
                        previousMarker = null;
                    }
                    if(card_desc.getVisibility() == View.VISIBLE)
                        card_desc.setVisibility(View.INVISIBLE);

                    for(int i=0; i<numFilter; i++) {
                        buttons_array[i].setBackgroundColor(getResources().getColor(R.color.colorWhite));
                        buttons_array[i].setTextColor(getResources().getColor(R.color.colorPrimary));
                        buttons_array[i].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_edge, null));
                        buttons_array[i].setTag(0);
                    }

                    switch ((String) Btn_distance.getTag()) {
                        case "ic_distance":
                            Btn_distance.setImageResource( R.drawable.ic_distance_s);
                            Btn_distance.setTag("ic_distance_s");
                            card_distance.setVisibility(View.VISIBLE);
                            setGoogleMapFull.getDeviceLocation(zoom);
                            setGoogleMapFull.CheckCircle();

                            break;
                        case "ic_distance_s":
                            Btn_distance.setImageResource( R.drawable.ic_distance);
                            Btn_distance.setTag("ic_distance");
                            card_distance.setVisibility(View.INVISIBLE);
                            setGoogleMapFull.CheckCircle();
                            break;
                    }
                }
            });

            distance_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    progressChangedValue = progress;
                    text_distance.setText(progress+" กม");
                    setGoogleMapFull.UpdateCircle(progress);
                }
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }
                public void onStopTrackingTouch(SeekBar seekBar) {
                    setGoogleMapFull.UpdateMark(progressChangedValue);
                    //text_distance.setText(progressChangedValue+" กม");
                }
            });
            mapView.onStart();
            mapView.getMapAsync(new OnMapReadyCallback() {

                @Override
                public void onMapReady(final GoogleMap map) {


                    map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        private float currentZoom = -1;
                        @Override
                        public void onCameraChange(CameraPosition pos) {
                            if (pos.zoom != currentZoom){
                                zoom = pos.zoom;
                                // do you action here
                            }
                        }
                    });
                    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            if(card_desc.getVisibility() == View.VISIBLE)
                                card_desc.setVisibility(View.INVISIBLE);
                            if (previousMarker != null) {
                                previousMarker.setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_" + previousMarker.getTitle(), 32, 32)));

                                previousMarker = null;
                            }
                        }
                    });
                    map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {

                            if(card_desc.getVisibility() == View.INVISIBLE) {
                                card_desc.setVisibility(View.VISIBLE);
                            }
                            if (previousMarker != null) {
                                previousMarker.setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_"+previousMarker.getTitle(), 32, 32)));
                            }

                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("typeitem_"+marker.getTitle(), 80, 80)));
                            previousMarker = marker;
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17.5f));
                            mapView.invalidate();
                            if(modelItems.get(Integer.parseInt(marker.getSnippet())).getModelFilePaths().getPathImg().equals("") || modelItems.get(Integer.parseInt(marker.getSnippet())).getModelFilePaths().getPathImg() == "null")
                            {
                                img_item_list_great_places.setImageResource(R.drawable.typeitem_9);
                            }
                            else
                            {
                                Picasso.get().load(modelItems.get(Integer.parseInt(marker.getSnippet())).getModelFilePaths().getPathImg()).into(img_item_list_great_places);
                            }

                            title_item.setText(modelItems.get(Integer.parseInt(marker.getSnippet())).getTopicTH());
                            topic_item.setText(getResources().getString(R.string.text_address)+" "+modelItems.get(Integer.parseInt(marker.getSnippet())).getAddressTH());
                            txtdistance.setText(modelItems.get(Integer.parseInt(marker.getSnippet())).getDistance());
                            img_favorite.setColorFilter(null);
                            if (modelItems.get(Integer.parseInt(marker.getSnippet())).isFavorites()){
                                img_favorite.setColorFilter(getResources().getColor(R.color.red));
                            } else {
                                img_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                            }

                            img_favorite.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (app_mode.equals("offline")){
                                        alertDialogCheckOffLineModes();
                                    }else {
                                        if(modelItems.get(Integer.parseInt(marker.getSnippet())).getModelTypeOfItem().getTypeID() != 9) {
                                            if (getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken", "") != "") {
                                                img_favorite.setColorFilter(null);
                                                if (modelItems.get(Integer.parseInt(marker.getSnippet())).isFavorites()) {
                                                    img_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                                                    modelItems.get(Integer.parseInt(marker.getSnippet())).setFavorites(false);

                                                    if (modelItems.get(Integer.parseInt(marker.getSnippet())).getItemID().equals(getIntent().getExtras().getString("Iditem"))) {
                                                        current_item = false;
                                                    }
                                                } else {
                                                    img_favorite.setColorFilter(getResources().getColor(R.color.red));
                                                    modelItems.get(Integer.parseInt(marker.getSnippet())).setFavorites(true);
                                                    if (modelItems.get(Integer.parseInt(marker.getSnippet())).getItemID().equals(getIntent().getExtras().getString("Iditem"))) {
                                                        current_item = true;
                                                    }

                                                }

                                                String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                                                String url_add_favorite = getResources().getString(R.string.host_ip) + "/lanexang_tourism/user/GetData/AddFavorite?typeItem=" + modelItems.get(Integer.parseInt(marker.getSnippet())).getModelTypeOfItem().getTypeID() + "&idUsers=" + UserId + "&idItems=" + modelItems.get(Integer.parseInt(marker.getSnippet())).getItemID() + "";
                                                new AddFavorite(GoogleMapActivity.this).execute(url_add_favorite);
                                            } else {
                                                Intent intent = new Intent(GoogleMapActivity.this, SocialLoginHelperActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                }
                            });

                            return true;
                        }
                    });

                    mapView.onResume();

                    if(itemType == 1 || itemType == 2 || itemType == 3 || itemType == 7)
                        setGoogleMapFull.ShowMarker("1|2|3|7",progressChangedValue,1,modelItem_decall == null?"-1":modelItem_decall.getItemID());
                    if(itemType == 9 || itemType == 10 || itemType == 11 || itemType == 12 || itemType == 13)
                        setGoogleMapFull.ShowMarker("9|10|11|12|13",progressChangedValue,1,modelItem_decall == null?"-1":modelItem_decall.getItemID());


                    card_desc.setVisibility(modelItem_decall == null?View.INVISIBLE:View.VISIBLE);
                    previousMarker = setGoogleMapFull.SearchMark(modelItem_decall == null?"-1":modelItem_decall.getItemID());
                    dismissProgressDialog();

                    if(setGoogleMapFull.checknear(50))
                        alertDialogConnectInternet();
                }


            });



        }

    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onResume();
        mapView.onSaveInstanceState(outState);
    }
    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
    public void SetNameMenu(){
        String[] NameMenuCategory;
        Resources res = getResources();
        NameMenuCategory = res.getStringArray(R.array.NameCategory);
        for(int i=0;i<NameMenuCategory.length;i++) {
            TextView_Array[i].setText(NameMenuCategory[i]);
        }
    }
    public void ClickNavigation(View view){
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        switch (view.getId()){
            case R.id.menu_category_1:
                intent.putExtra("retrunMain2fragment",0);
                break;
            case R.id.menu_category_2:
                intent.putExtra("retrunMain2fragment",1);
                break;
            case R.id.menu_category_3:
                intent.putExtra("retrunMain2fragment",2);
                break;
            case R.id.menu_category_4:
                intent.putExtra("retrunMain2fragment",3);
                break;
            case R.id.menu_category_5:
                intent.putExtra("retrunMain2fragment",4);
                break;
            case R.id.menu_category_6:
                intent.putExtra("retrunMain2fragment",5);
                break;
            case R.id.menu_category_7:
                intent.putExtra("retrunMain2fragment",6);
                break;
            case R.id.menu_category_8:
                intent.putExtra("retrunMain2fragment",7);
                break;
        }

        startActivity(intent);
        finish();
    }

    public void alertDialogCheckOffLineModes(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                url_json +="&Lat="+getIntent().getExtras().getString("Lat")+"&Lng="+getIntent().getExtras().getString("Lng")+"&LatU="+getIntent().getExtras().getString("LatU")+"&LngU="+getIntent().getExtras().getString("LngU");
                new AsyncTaskGoogleMap(this).execute(getResources().getString(R.string.host_ip)+url_json+ "&UserId=" + UserId);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void alertDialogConnectInternet(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_not_near));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
