package com.ipanda.lanexangtourism.connect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ipanda.lanexangtourism.model.ModelEmergency;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelProgramTour;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;
import com.ipanda.lanexangtourism.model.ModelTrip;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;
import com.ipanda.lanexangtourism.model.ModelTypeOfTrip;
import com.ipanda.lanexangtourism.model.util.Version;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "Lanexang";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        List listCreateTable = new ArrayList();
        String versionDatabase = "Create Table IF NOT EXISTS Version (id INTEGER PRIMARY KEY NOT NULL, version INTEGER NOT NULL)";
        String item = "Create Table IF NOT EXISTS Items (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,types TEXT,idItems TEXT,idTrip TEXT NULL, idTypeOfItems TEXT,type_th TEXT,types_en TEXT,types_lao TEXT,topic_th TEXT,topic_en TEXT,topic_lao TEXT," +
                "address_th TEXT,address_en TEXT,address_lao TEXT,details_th TEXT,details_en TEXT,details_lao TEXT,dateOpen TEXT,dateClose TEXT,latitude TEXT,longitude TEXT,tel TEXT,timeStart TEXT,timeEnd TEXT,path TEXT," +
                "idCountries TEXT,provinces_name_th TEXT,distance TEXT,distance_metr TEXT,favorites TEXT," +
                "FOREIGN KEY(idTrip) REFERENCES Trip(idTrip))";
        String program = "Create Table IF NOT EXISTS ProgramTour (idProgramTour TEXT PRIMARY KEY, name_th TEXT, name_en TEXT, name_lao TEXT, path TEXT)";
        String trip = "Create Table IF NOT EXISTS Trip(idTrip TEXT PRIMARY KEY,idProgramTour TEXT,trip_th TEXT,trip_en TEXT,trip_lao TEXT,path TEXT,areaTH TEXT,areaEN TEXT,areaLAO TEXT, TEXT,keys TEXT,types_th TEXT,types_en TEXT,types_lao TEXT," +
                "FOREIGN KEY(idProgramTour) REFERENCES ProgramTour(idProgramTour))";
        String itemSearch = "Create Table IF NOT EXISTS ItemSearch (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,types TEXT,idItems TEXT,idTrip TEXT NULL, idTypeOfItems TEXT,type_th TEXT,types_en TEXT,types_lao TEXT,topic_th TEXT,topic_en TEXT,topic_lao TEXT," +
                "address_th TEXT,address_en TEXT,address_lao TEXT,details_th TEXT,details_en TEXT,details_lao TEXT,dateOpen TEXT,dateClose TEXT,latitude TEXT,longitude TEXT,tel TEXT,timeStart TEXT,timeEnd TEXT,path TEXT," +
                "idCountries TEXT,provinces_name_th TEXT,distance TEXT,distance_metr TEXT,favorites TEXT," +
                "FOREIGN KEY(idTrip) REFERENCES Trip(idTrip))";
        String emergencyTravel ="Create Table IF NOT EXISTS TypeOfTravel (idTypeOfTravel TEXT PRIMARY KEY NOT NULL,type_th TEXT, type_en TEXT, type_lao TEXT)";
        String itemEmergencyTravel = "Create Table IF NOT EXISTS ItemTravel (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, idEmergency TEXT, idTypeOfTravel TEXT, topic_th TEXT,topic_en TEXT,topic_lao TEXT," +
                "address_th TEXT,address_en TEXT,address_lao TEXT,details_th TEXT,details_en TEXT,details_lao TEXT,dateOpen TEXT,dateClose TEXT,latitude TEXT,longitude TEXT,tel TEXT,timeStart TEXT,timeEnd TEXT,path TEXT," +
                "favorites TEXT," +
                "FOREIGN KEY(idTypeOfTravel) REFERENCES TypeOfTravel(idTypeOfTravel))";
        String emergency = "Create Table IF NOT EXISTS ItemEmergency(idEmergency TEXT PRIMARY KEY NOT NULL,idTypeOfEmergency TEXT,topic_th TEXT,topic_en TEXT,topic_lao TEXT,address_th TEXT,address_en TEXT,address_lao TEXT," +
                "details_th TEXT,details_en TEXT,details_lao TEXT,dateOpen TEXT,dateClose TEXT,tel TEXT,latitude TEXT,longitude TEXT,timeStart TEXT,timeEnd TEXT,path TEXT)";

        String information = "Create Table IF NOT EXISTS Information(idTravelInformation TEXT PRIMARY KEY NOT NULL,idCountries TEXT,topic_th,topic_en,topic_lao TEXT,history_th TEXT,government_th TEXT,language_th TEXT,population_th TEXT,religion_th TEXT," +
                "history_en TEXT,government_en TEXT,language_en TEXT,population_en TEXT,religion_en TEXT,history_lao TEXT,government_lao TEXT,language_lao TEXT,population_lao TEXT,religion_lao TEXT,path TEXT)";

        listCreateTable.add(versionDatabase);
        listCreateTable.add(item);
        listCreateTable.add(program);
        listCreateTable.add(trip);
        listCreateTable.add(itemSearch);
        listCreateTable.add(emergencyTravel);
        listCreateTable.add(itemEmergencyTravel);
        listCreateTable.add(emergency);
        listCreateTable.add(information);

        for (Object sql : listCreateTable) {
            db.execSQL(String.valueOf(sql));
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("On Upgrade",">>>>>>>>>>>>>>>>>> Drop");
        db.execSQL("Drop Table if exists Items");
        db.execSQL("Drop Table if exists ProgramTour");
        db.execSQL("Drop Table if exists Trip");
        db.execSQL("Drop Table if exists ItemSearch");
        db.execSQL("Drop Table if exists TypeOfTravel");
        db.execSQL("Drop Table if exists ItemTravel");
        db.execSQL("Drop Table if exists ItemEmergency");
        db.execSQL("Drop Table if exists Information");
        this.onCreate(db);

//        db.execSQL("delete from Items");
//        db.execSQL("delete from ProgramTour");
//        db.execSQL("delete from Trip");
//        db.execSQL("delete from ItemSearch");
//        db.execSQL("delete from TypeOfTravel");
//        db.execSQL("delete from ItemTravel");
//        db.execSQL("delete from ItemEmergency");
//        db.execSQL("delete from Information");
//        this.onCreate(db);

    }

    public boolean addVersion(Version version){
        long result = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", version.getId());
            values.put("version", version.getUpGrade());
            result = db.insert("Version", null, values);
            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean updateVersion(Version version) {
        long result = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("id", version.getId());
            values.put("version", version.getUpGrade());
            result = db.update("Version", values, "id = ?", new String[] {String.valueOf(version.getId())});
            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public Version getVersion(int id){
        Version version = new Version();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select id, version from Version Where id = ?", new String[] {String.valueOf(id)});
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            version = new Version(cursor.getInt(0),cursor.getInt(1));
        }
        db.close();
        return version;
    }

    public boolean addItems(ArrayList<ModelItem> items) {
        long result = -1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            for (ModelItem item : items) {
                values.put("idItems",item.getItemID());
                values.put("idTypeOfItems",item.getModelTypeOfItem().getTypeID());
                values.put("type_th",item.getModelTypeOfItem().getTypeTH());
                values.put("types_en",item.getModelTypeOfItem().getTypeEN());
                values.put("types_lao",item.getModelTypeOfItem().getTypeLAO());
                values.put("topic_th",item.getTopicTH());
                values.put("topic_en",item.getTopicEN());
                values.put("topic_lao",item.getTopicLAO());
                values.put("address_th",item.getAddressTH());
                values.put("address_en",item.getAddressEN());
                values.put("address_lao",item.getAddressLAO());
                values.put("details_th",item.getDetailTH());
                values.put("details_en",item.getDetailEN());
                values.put("details_lao",item.getDetailLAO());
                values.put("dateOpen",item.getDateOpen());
                values.put("dateClose",item.getDateClose());
                values.put("latitude",item.getLatitude());
                values.put("longitude",item.getLongitude());
                values.put("tel",item.getTel());
                values.put("timeStart",item.getTimeStart());
                values.put("timeEnd",item.getTimeEnd());
                values.put("favorites",item.isFavorites());
                values.put("path",item.getModelFilePaths().getPathImg());

                result = db.insert("ItemSearch", null, values);
            }

            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addProgramTour(ArrayList<ModelProgramTour> arrayList){
        long result = -1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            ContentValues conTrip = new ContentValues();
            ContentValues conItem = new ContentValues();

            for (ModelProgramTour tour : arrayList) {
                values.put("idProgramTour", tour.getProgramTourID());
                values.put("name_th", tour.getProgramTourTH());
                values.put("name_en", tour.getProgramTourEN());
                values.put("name_lao", tour.getProgramTourLAO());
                values.put("path", tour.getPathImg());
                result = db.insert("ProgramTour", null, values);

                for (ModelTrip trip: tour.getModelTrips()) {
                    conTrip.put("idTrip",trip.getTripID());
                    conTrip.put("idProgramTour", tour.getProgramTourID());
                    conTrip.put("trip_th",trip.getTripTH());
                    conTrip.put("trip_en",trip.getTripEN());
                    conTrip.put("trip_lao",trip.getTripLAO());
                    conTrip.put("path",trip.getPathImg());
                    conTrip.put("areaTH",trip.getAreaTH());
                    conTrip.put("areaEN",trip.getAreaEN());
                    conTrip.put("areaLAO",trip.getAreaLAO());
                    conTrip.put("keys",trip.getKey());
                    conTrip.put("types_th",trip.getModelTypeOfTrip().getTypeTH());
                    conTrip.put("types_en",trip.getModelTypeOfTrip().getTypeEN());
                    conTrip.put("types_lao",trip.getModelTypeOfTrip().getTypeLAO());
                    result = db.insert("Trip", null, conTrip);

                    for (ModelItem item: trip.getModelItems()){
                        conItem.put("idItems",item.getItemID());
                        conItem.put("idTrip",trip.getTripID());
                        conItem.put("idTypeOfItems",item.getModelTypeOfItem().getTypeID());
                        conItem.put("type_th",item.getModelTypeOfItem().getTypeTH());
                        conItem.put("types_en",item.getModelTypeOfItem().getTypeEN());
                        conItem.put("types_lao",item.getModelTypeOfItem().getTypeLAO());
                        conItem.put("topic_th",item.getTopicTH());
                        conItem.put("topic_en",item.getTopicEN());
                        conItem.put("topic_lao",item.getTopicLAO());
                        conItem.put("address_th",item.getAddressTH());
                        conItem.put("address_en",item.getAddressEN());
                        conItem.put("address_lao",item.getAddressLAO());
                        conItem.put("details_th",item.getDetailTH());
                        conItem.put("details_en",item.getDetailEN());
                        conItem.put("details_lao",item.getDetailLAO());
                        conItem.put("dateOpen",item.getDateOpen());
                        conItem.put("dateClose",item.getDateClose());
                        conItem.put("latitude",item.getLatitude());
                        conItem.put("longitude",item.getLongitude());
                        conItem.put("tel",item.getTel());
                        conItem.put("timeStart",item.getTimeStart());
                        conItem.put("timeEnd",item.getTimeEnd());
                        conItem.put("path",item.getModelFilePaths().getPathImg());
                        result = db.insert("Items", null, conItem);

                    }

                }
            }

            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addEmergencyTravel(ArrayList<ModelTypeOfTravel> modelTypeOfTravels){
        long result = -1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues conType = new ContentValues();
            ContentValues conItem = new ContentValues();

            for (ModelTypeOfTravel type : modelTypeOfTravels) {
                conType.put("idTypeOfTravel",type.getIdTypeOfTravel());
                conType.put("type_th",type.getTypeTH());
                conType.put("type_en",type.getTypeEN());
                conType.put("type_lao",type.getTypeLAO());
                result = db.insert("TypeOfTravel", null, conType);

                for (ModelItem item: type.getModelEmergency()) {
                    conItem.put("idEmergency",item.getItemID());
                    conItem.put("idTypeOfTravel",type.getIdTypeOfTravel());
                    conItem.put("topic_th",item.getTopicTH());
                    conItem.put("topic_en",item.getTopicEN());
                    conItem.put("topic_lao",item.getTopicLAO());
                    conItem.put("address_th",item.getAddressTH());
                    conItem.put("address_en",item.getAddressEN());
                    conItem.put("address_lao",item.getAddressLAO());
                    conItem.put("details_th",item.getDetailTH());
                    conItem.put("details_en",item.getDetailEN());
                    conItem.put("details_lao",item.getDetailLAO());
                    conItem.put("dateOpen",item.getDateOpen());
                    conItem.put("dateClose",item.getDateClose());
                    conItem.put("latitude",item.getLatitude());
                    conItem.put("longitude",item.getLongitude());
                    conItem.put("tel",item.getTel());
                    conItem.put("timeStart",item.getTimeStart());
                    conItem.put("timeEnd",item.getTimeEnd());
                    conItem.put("path",item.getModelFilePaths().getPathImg());
                    conItem.put("favorites",item.isFavorites());
                    result = db.insert("ItemTravel", null, conItem);
                }

            }

            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }

    public boolean addEmergency(ArrayList<ModelEmergency> modelEmergencies){
        long result = -1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues conItem = new ContentValues();

            for (ModelEmergency item: modelEmergencies) {
                conItem.put("idEmergency",item.getEmergencyID());
                conItem.put("idTypeOfEmergency",item.getIdTypeOfEmergency());
                conItem.put("topic_th",item.getTopicTH());
                conItem.put("topic_en",item.getTopicEN());
                conItem.put("topic_lao",item.getTopicLAO());
                conItem.put("address_th",item.getAddressTH());
                conItem.put("address_en",item.getAddressEN());
                conItem.put("address_lao",item.getAddressLAO());
                conItem.put("details_th",item.getDetailTH());
                conItem.put("details_en",item.getDetailEN());
                conItem.put("details_lao",item.getDetailLAO());
                conItem.put("dateOpen",item.getDateOpen());
                conItem.put("dateClose",item.getDateClose());
                conItem.put("tel",item.getTel());
                conItem.put("latitude",item.getLatitude());
                conItem.put("longitude",item.getLongitude());
                conItem.put("timeStart",item.getTimeStart());
                conItem.put("timeEnd",item.getTimeEnd());
                conItem.put("path",item.getModelFilePaths().getPathImg());
                result = db.insert("ItemEmergency", null, conItem);
            }

            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }

    }

    public boolean addInformation(ArrayList<ModelTravelInformation> modelTravelInformations){
        long result = -1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues conItem = new ContentValues();

            for (ModelTravelInformation item: modelTravelInformations) {
                conItem.put("idTravelInformation",item.getIdTravelInformation());
                conItem.put("idCountries",item.getCountriesID());
                conItem.put("topic_th",item.getTopic_th());
                conItem.put("topic_en",item.getTopic_en());
                conItem.put("topic_lao",item.getTopic_lao());
                conItem.put("history_th",item.getHistory_th());
                conItem.put("government_th",item.getGovernment_th());
                conItem.put("language_th",item.getLanguage_th());
                conItem.put("population_th",item.getPopulation_th());
                conItem.put("religion_th",item.getReligion_th());
                conItem.put("history_en",item.getHistory_en());
                conItem.put("government_en",item.getGovernment_en());
                conItem.put("language_en",item.getLanguage_en());
                conItem.put("population_en",item.getPopulation_en());
                conItem.put("religion_en",item.getReligion_en());
                conItem.put("history_lao",item.getHistory_lao());
                conItem.put("government_lao",item.getGovernment_lao());
                conItem.put("language_lao",item.getLanguage_lao());
                conItem.put("population_lao",item.getPopulation_lao());
                conItem.put("religion_lao",item.getReligion_lao());
                conItem.put("path",item.getModelFilePaths().getPathImg());
                result = db.insert("Information", null, conItem);
            }

            db.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == -1) {
            return false;
        }else{
            return true;
        }
    }



    public ArrayList<ModelItem> getModelItem(String idTrip) {
        ArrayList<ModelItem> list = new ArrayList<ModelItem>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select idItems,idTrip,idTypeOfItems,type_th,types_en,types_lao,topic_th,topic_en,topic_lao,address_th," +
                "address_en,address_lao,details_th,details_en,details_lao,dateOpen,dateClose,latitude,longitude,tel,timeStart,timeEnd,path from Items where idTrip = "+idTrip+"", null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelItem item = new ModelItem();
                ModelTypeOfItem type = new ModelTypeOfItem();
                ModelFilePaths paths = new ModelFilePaths();
                item.setItemID(cursor.getString(0));
                type.setTypeID(cursor.getInt(2));
                type.setTypeTH(cursor.getString(3));
                type.setTypeEN(cursor.getString(4));
                type.setTypeLAO(cursor.getString(5));
                item.setModelTypeOfItem(type);
                item.setTopicTH(cursor.getString(6));
                item.setTopicEN(cursor.getString(7));
                item.setTopicLAO(cursor.getString(8));
                item.setAddressTH(cursor.getString(9));
                item.setAddressEN(cursor.getString(10));
                item.setAddressLAO(cursor.getString(11));
                item.setDetailTH(cursor.getString(12));
                item.setDetailEN(cursor.getString(13));
                item.setDetailLAO(cursor.getString(14));
                item.setDateOpen(cursor.getString(15));
                item.setDateClose(cursor.getString(16));
                item.setLatitude(Double.parseDouble(cursor.getString(17)));
                item.setLongitude(Double.parseDouble(cursor.getString(18)));
                item.setTel(cursor.getString(19));
                item.setTimeStart(cursor.getString(20));
                item.setTimeEnd(cursor.getString(21));
                paths.setPathImg(cursor.getString(22));
                item.setModelFilePaths(paths);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelProgramTour> getModelProgramTour(){
        ArrayList<ModelProgramTour> list = new ArrayList<ModelProgramTour>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select idProgramTour, name_th,name_en,name_lao,path  from ProgramTour", null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelProgramTour tour = new ModelProgramTour();
                tour.setProgramTourID(Integer.parseInt(cursor.getString(0)));
                tour.setProgramTourTH(cursor.getString(1));
                tour.setProgramTourEN(cursor.getString(2));
                tour.setProgramTourLAO(cursor.getString(3));
                tour.setPathImg(cursor.getString(4));
                list.add(tour);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelTrip> getModelTrip(int idProgramTour){
        ArrayList<ModelTrip> list = new ArrayList<ModelTrip>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select idTrip,idProgramTour,trip_th,trip_en,trip_lao,path,areaTH,areaEN,areaLAO,keys,types_th,types_en,types_lao from Trip where idProgramTour="+idProgramTour+"", null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelTrip trip = new ModelTrip();
                ModelTypeOfTrip modelTypeOfTrip = new ModelTypeOfTrip();
                trip.setTripID(Integer.parseInt(cursor.getString(0)));
                trip.setTripTH(cursor.getString(2));
                trip.setTripEN(cursor.getString(3));
                trip.setTripLAO(cursor.getString(4));
                trip.setPathImg(cursor.getString(5));
                trip.setAreaTH(cursor.getString(6));
                trip.setAreaEN(cursor.getString(7));
                trip.setAreaLAO(cursor.getString(8));
                trip.setKey(cursor.getColumnIndex(String.valueOf(9)));
                modelTypeOfTrip.setTypeTH(cursor.getString(10));
                modelTypeOfTrip.setTypeEN(cursor.getString(11));
                modelTypeOfTrip.setTypeLAO(cursor.getString(12));
                trip.setModelTypeOfTrip(modelTypeOfTrip);

                list.add(trip);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelItem> getModelTypeOfItem(String sql){
        ArrayList<ModelItem> list = new ArrayList<ModelItem>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT idItems,idTypeOfItems,type_th,types_en,types_lao,topic_th,topic_en,topic_lao,address_th,address_en,address_lao,details_th,details_en,details_lao,dateOpen,dateClose,latitude,longitude,tel,timeStart,timeEnd,path,favorites FROM ItemSearch WHERE "+sql+"",null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelItem item = new ModelItem();
                ModelTypeOfItem type = new ModelTypeOfItem();
                ModelFilePaths paths = new ModelFilePaths();
                item.setItemID(cursor.getString(0));
                type.setTypeID(cursor.getInt(1));
                type.setTypeTH(cursor.getString(2));
                type.setTypeEN(cursor.getString(3));
                type.setTypeLAO(cursor.getString(4));
                item.setModelTypeOfItem(type);
                item.setTopicTH(cursor.getString(5));
                item.setTopicEN(cursor.getString(6));
                item.setTopicLAO(cursor.getString(7));
                item.setAddressTH(cursor.getString(8));
                item.setAddressEN(cursor.getString(9));
                item.setAddressLAO(cursor.getString(10));
                item.setDetailTH(cursor.getString(11));
                item.setDetailEN(cursor.getString(12));
                item.setDetailLAO(cursor.getString(13));
                item.setDateOpen(cursor.getString(14));
                item.setDateClose(cursor.getString(15));
                item.setLatitude(Double.parseDouble(cursor.getString(16)));
                item.setLongitude(Double.parseDouble(cursor.getString(17)));
                item.setTel(cursor.getString(18));
                item.setTimeStart(cursor.getString(19));
                item.setTimeEnd(cursor.getString(20));
                paths.setPathImg(cursor.getString(21));
                item.setModelFilePaths(paths);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelTypeOfTravel> getModelTypeOfTravel(){
        ArrayList<ModelTypeOfTravel> list = new ArrayList<ModelTypeOfTravel>();
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbItem = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT idTypeOfTravel,type_th,type_en,type_lao FROM TypeOfTravel",null);
        Cursor cItem = dbItem.rawQuery("SELECT idEmergency,idTypeOfTravel,topic_th,topic_en,topic_lao,address_th,address_en,address_lao,details_th,details_en,details_lao,dateOpen,dateClose,latitude,longitude,tel,timeStart,timeEnd,path,favorites FROM ItemTravel ",null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelTypeOfTravel travel = new ModelTypeOfTravel();
                travel.setIdTypeOfTravel(Integer.parseInt(cursor.getString(0)));
                travel.setTypeTH(cursor.getString(1));
                travel.setTypeEN(cursor.getString(2));
                travel.setTypeLAO(cursor.getString(3));


                ArrayList<ModelItem> list_emergency = new ArrayList<>();
                if (cItem != null && cItem.getCount() > 0){
                    cItem.moveToFirst();
                    do{
                        if (cursor.getString(0).equals(cItem.getString(1))){
                            ModelItem item = new ModelItem();
                            ModelTypeOfItem type = new ModelTypeOfItem();
                            ModelFilePaths paths = new ModelFilePaths();
                            item.setItemID(cItem.getString(0));
                            type.setTypeID(cItem.getInt(1));
                            item.setModelTypeOfItem(type);
                            item.setTopicTH(cItem.getString(2));
                            item.setTopicEN(cItem.getString(3));
                            item.setTopicLAO(cItem.getString(4));
                            item.setAddressTH(cItem.getString(5));
                            item.setAddressEN(cItem.getString(6));
                            item.setAddressLAO(cItem.getString(7));
                            item.setDetailTH(cItem.getString(8));
                            item.setDetailEN(cItem.getString(9));
                            item.setDetailLAO(cItem.getString(10));
                            item.setDateOpen(cItem.getString(11));
                            item.setDateClose(cItem.getString(12));
                            item.setLatitude(Double.parseDouble(cItem.getString(13)));
                            item.setLongitude(Double.parseDouble(cItem.getString(14)));
                            item.setTel(cItem.getString(15));
                            item.setTimeStart(cItem.getString(16));
                            item.setTimeEnd(cItem.getString(17));
                            paths.setPathImg(cItem.getString(18));
                            item.setModelFilePaths(paths);
                            list_emergency.add(item);
                        }

                    }while (cItem.moveToNext());
                }
                travel.setModelEmergency(list_emergency);
                list.add(travel);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelEmergency> getModelEmergency(String select){
        ArrayList<ModelEmergency> list = new ArrayList<ModelEmergency>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT idEmergency,idTypeOfEmergency,topic_th,topic_en,topic_lao,address_th,address_en,address_lao,details_th,details_en,details_lao,dateOpen,dateClose,latitude,longitude,tel,timeStart,timeEnd,path FROM ItemEmergency WHERE idTypeOfEmergency ="+select+"",null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelEmergency item = new ModelEmergency();
                ModelFilePaths paths = new ModelFilePaths();
                item.setEmergencyID(cursor.getString(0));
                item.setIdTypeOfEmergency(cursor.getString(1));
                item.setTopicTH(cursor.getString(2));
                item.setTopicEN(cursor.getString(3));
                item.setTopicLAO(cursor.getString(4));
                item.setAddressTH(cursor.getString(5));
                item.setAddressEN(cursor.getString(6));
                item.setAddressLAO(cursor.getString(7));
                item.setDetailTH(cursor.getString(8));
                item.setDetailEN(cursor.getString(9));
                item.setDetailLAO(cursor.getString(10));
                item.setDateOpen(cursor.getString(11));
                item.setDateClose(cursor.getString(12));
                item.setLatitude(Double.parseDouble(cursor.getString(13)));
                item.setLongitude(Double.parseDouble(cursor.getString(14)));
                item.setTel(cursor.getString(15));
                item.setTimeStart(cursor.getString(16));
                item.setTimeEnd(cursor.getString(17));
                paths.setPathImg(cursor.getString(18));
                item.setModelFilePaths(paths);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public ArrayList<ModelTravelInformation> getInformations(String select){
        ArrayList<ModelTravelInformation> list = new ArrayList<ModelTravelInformation>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT idTravelInformation,idCountries,topic_th,history_th,government_th,language_th,population_th,religion_th,history_en,government_en,language_en,population_en,religion_en,history_lao,government_lao,language_lao,population_lao,religion_lao,path,topic_en,topic_lao FROM Information WHERE idCountries ="+select+"",null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelTravelInformation item = new ModelTravelInformation();
                ModelFilePaths paths = new ModelFilePaths();
                item.setIdTravelInformation(cursor.getString(0));
//                item.setIdTypeOfEmergency(cursor.getString(1));
                item.setTopic_th(cursor.getString(2));
                item.setHistory_th(cursor.getString(3));
                item.setGovernment_th(cursor.getString(4));
                item.setLanguage_th(cursor.getString(5));
                item.setPopulation_th(cursor.getString(6));
                item.setReligion_th(cursor.getString(7));
                item.setHistory_en(cursor.getString(8));
                item.setGovernment_en(cursor.getString(9));
                item.setLanguage_en(cursor.getString(10));
                item.setPopulation_en(cursor.getString(11));
                item.setReligion_en(cursor.getString(12));
                item.setHistory_lao(cursor.getString(13));
                item.setGovernment_lao(cursor.getString(14));
                item.setLanguage_lao(cursor.getString(15));
                item.setPopulation_lao(cursor.getString(16));
                item.setReligion_lao(cursor.getString(17));
                paths.setPathImg(cursor.getString(18));
                item.setTopic_en(cursor.getString(19));
                item.setTopic_lao(cursor.getString(20));
                item.setModelFilePaths(paths);

                list.add(item);
            }while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

}
