package com.ipanda.lanexangtourism;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.ipanda.lanexangtourism.adapter.AdapterSticker;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.StickerView;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CameraActivity extends AppCompatActivity implements AdapterSticker.RecyclerViewClickListener {

    private static final int REQUEST_PICTURE_CAPTURE = 123;

    private StickerView stickerView;
    private RecyclerView recyclerView;
    private AdapterSticker adapterSticker;
    private RelativeLayout RelativeLayout_Sticker;
    private int NumOfSticker = 24;
    private ImageView img_sticker_view;

    private String pictureFilePath;
    private Uri photoURI;
    private File pictureFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbar_title.setText(this.getIntent().getStringExtra("TopicName"));

        stickerView = (StickerView) findViewById(R.id.sticker_view);
        img_sticker_view = (ImageView) findViewById(R.id.img_sticker_view);
        stickerView.configDefaultIcons();
        stickerView.setLocked(false);
        stickerView.setConstrained(false);

        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView_List_Sticker);
        RelativeLayout_Sticker = (RelativeLayout) findViewById(R.id.RelativeLayout_Sticker);
        addSticker();


        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra( MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            pictureFile = null;
            try {
                pictureFile = getPictureFile("temp_file.jpg");
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (pictureFile != null) {
                photoURI = FileProvider.getUriForFile(this, "com.ipanda.lanexangtourism.fileprovider", pictureFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(Intent.createChooser(cameraIntent, "Take a picture with"), REQUEST_PICTURE_CAPTURE);
            }
        }

    }

    public void onClickSaveImageSticker(View view) {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp + ".jpg";

        File pictureFile = null;
        try {
            pictureFile = getPictureFile(imageFileName);
        } catch (IOException ex) {
            Toast.makeText(this,
                    "Photo file can't be created, please try again",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Uri photoURI = FileProvider.getUriForFile(this, "com.ipanda.lanexangtourism.fileprovider", pictureFile);

        stickerView.save(pictureFile);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(photoURI);
        sendBroadcast(mediaScanIntent);
        Toast.makeText(getApplicationContext(), "Save photo to a gallery", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    public void onClickViewSticker(View view){
        if(RelativeLayout_Sticker.getVisibility() == View.VISIBLE)
            RelativeLayout_Sticker.setVisibility(View.GONE);
        else
            RelativeLayout_Sticker.setVisibility(View.VISIBLE);
    }

    public File getPictureFile(String pictureFile) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = new File(storageDir,  pictureFile );

        return image;
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode == REQUEST_PICTURE_CAPTURE && resCode == RESULT_OK) {
            rotateImage(setReducedImageSize());
        }else {
            onBackPressed();
        }
//        rotateImage(setReducedImageSize());
    }

    public void addSticker(){
        ArrayList<Drawable> mStickers = new ArrayList<>();
        for(int i = 0; i< NumOfSticker;i++)
        {
            String stickersID = "sticker" +(i+1);
            int stickersResID = getResources().getIdentifier(stickersID, "drawable", getPackageName());
            mStickers.add(getResources().getDrawable(stickersResID));
        }

        adapterSticker = new AdapterSticker(getApplicationContext(), mStickers,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setAdapter(adapterSticker);

    }

    @Override
    public void onStickerClicked(int position) {
        int stickersResID = getResources().getIdentifier("sticker" +(position+1), "drawable", getPackageName());
        Drawable drawable1 = ContextCompat.getDrawable(this, stickersResID);
        stickerView.addSticker(new DrawableSticker(drawable1), Sticker.Position.BOTTOM | Sticker.Position.RIGHT);
        RelativeLayout_Sticker.setVisibility(View.GONE);

    }

    public void onClickedShareFacebook(View view){

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp + ".jpg";

        File pictureFile = null;
        try {
            pictureFile = getPictureFile(imageFileName);
        } catch (IOException ex) {
            Toast.makeText(this,
                    "Photo file can't be created, please try again",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Uri photoURI = FileProvider.getUriForFile(this, "com.ipanda.lanexangtourism.fileprovider", pictureFile);

        stickerView.save(pictureFile);

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(photoURI);
        sendBroadcast(mediaScanIntent);

        Toast.makeText(getApplicationContext(), "Save photo to a gallery and Share", Toast.LENGTH_SHORT).show();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        shareIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(shareIntent,this.getIntent().getStringExtra("TopicName")));
        onBackPressed();
    }
    private Bitmap setReducedImageSize(){
        int targetImageViewWidth = 1920;
        int targetImageViewHeight = 1080;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth/targetImageViewWidth,cameraImageHeight/targetImageViewHeight);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;

//        Bitmap photoReducesSizeBitmp = BitmapFactory.decodeFile("",bmOptions);
//        img_sticker_view.setImageBitmap(photoReducesSizeBitmp);
        return BitmapFactory.decodeFile(pictureFile.toString(),bmOptions);

    }

    private void rotateImage(Bitmap bitmap){
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(pictureFile.toString());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation){
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                default:
            }

            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
            img_sticker_view.setImageBitmap(rotatedBitmap);

        }catch (IOException e){
            e.printStackTrace();
        }


    }

}


