package com.ipanda.lanexangtourism;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ipanda.lanexangtourism.fragment_information.Fragment_information_province_detail;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;

public class InformationProvinceDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_province_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String[] NameMenuCategory;
        NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
        toolbar_title.setText(NameMenuCategory[6]);

        FragmentManager fragmentManager = getSupportFragmentManager();
        ModelTravelInformation modelTravelInformation = getIntent().getParcelableExtra("modelTravelInformation");
        Class fragmentClass = Fragment_information_province_detail.class;
        try {
            Bundle bundle = new Bundle();
            bundle.putParcelable("modelTravelInformation", modelTravelInformation);

            Fragment fragment_information_province_detail = (Fragment) fragmentClass.newInstance();
            fragment_information_province_detail.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.content_information_province_detail,fragment_information_province_detail).commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
