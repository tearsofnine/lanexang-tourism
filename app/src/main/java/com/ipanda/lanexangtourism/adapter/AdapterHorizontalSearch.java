package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;

import java.util.ArrayList;

public class AdapterHorizontalSearch extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<String> arrayList_DemoSearches;

    public AdapterHorizontalSearch(Context context, ArrayList<String> arrayList_DemoSearches){
        this.context = context;
        this.arrayList_DemoSearches = arrayList_DemoSearches;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList_DemoSearches.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList_DemoSearches.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        final DemoSearch search = (DemoSearch) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = mInflater.inflate(R.layout.layout_buttom_great_places, parent, false);

        //convertView.setBackgroundColor(context.getResources().getColor(R.color.clr2));

        TextView txtTitle = (TextView) convertView.findViewById(R.id.title_item);
        TextView txtTopic = (TextView) convertView.findViewById(R.id.topic_item);
        ImageView imgFavorite = (ImageView) convertView.findViewById(R.id.img_favorite);
        ImageView imgPosition = (ImageView) convertView.findViewById(R.id.img_position);


//        txtTitle.setText(search.getTitle());
//        txtTopic.setText(search.getAddress());
        imgFavorite.setImageResource(R.drawable.ic_favorite);
        imgPosition.setImageResource(R.drawable.ic_position_w);


        return convertView;
    }
}
