package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.TripDescriptionActivity;
import com.ipanda.lanexangtourism.model.ModelTrip;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class AdapterTrip extends BaseAdapter implements LayoutInflater.Filter ,Filterable{

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<ModelTrip> modelTrips;
    private ArrayList<ModelTrip> mDataFiltered;
    private int json_index;


    public AdapterTrip(Context context, ArrayList<ModelTrip> modelTrips,int json_index) {
        this.context = context;
        this.modelTrips = modelTrips;
        this.mDataFiltered = modelTrips;
        this.json_index = json_index;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return modelTrips.size();
    }

    @Override
    public Object getItem(int position) {
        return modelTrips.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ModelTrip trip = (ModelTrip) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.layout_item_trip, parent, false);

        ImageView imageView = convertView.findViewById(R.id.img_item_trip);
        TextView textViewTitle = convertView.findViewById(R.id.text_item_title);
        TextView textViewFrom = convertView.findViewById(R.id.text_item_form);
        TextView textViewType = convertView.findViewById(R.id.type_trip);

        Picasso.get().load(trip.getPathImg()).into(imageView);

        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                textViewTitle.setText(trip.getTripTH());
                textViewType.setText(trip.getModelTypeOfTrip().getTypeTH());
                textViewFrom.setText(trip.getAreaTH());
                break;
            case "en":
                if (trip.getTripEN().equals("") || trip.getTripEN() == null || trip.getTripEN().equals("null")) {
                    textViewTitle.setText(trip.getTripTH());


                } else {
                    textViewTitle.setText(trip.getTripEN());
                }
                textViewType.setText(trip.getModelTypeOfTrip().getTypeEN());
                textViewFrom.setText(trip.getAreaEN());
                break;
            case "lo":
                if (trip.getTripLAO().equals("") || trip.getTripLAO() == null || trip.getTripLAO().equals("null")) {
                    textViewTitle.setText(trip.getTripTH());

                } else {
                    textViewTitle.setText(trip.getTripLAO());
                }
                textViewType.setText(trip.getModelTypeOfTrip().getTypeLAO());
                textViewFrom.setText(trip.getAreaLAO());
                break;
        }




        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TripDescriptionActivity.class);
                intent.putExtra("MODEL_TRIP",trip);
                intent.putExtra("JSON_POSITION",json_index);
                context.startActivity(intent);
            }
        });


        RelativeLayout layout = convertView.findViewById(R.id.RelativeLayout_btn_shape_trip);
        if (trip.getModelTypeOfTrip().getTypeEN().equals("1-Day Trip")){
            layout.setBackground(context.getResources().getDrawable(R.drawable.button_shape_trip_blue));
        }else {
            layout.setBackground(context.getResources().getDrawable(R.drawable.button_shape_trip_skyblue));
        }

        return convertView;
    }



    @Override
    public boolean onLoadClass(Class clazz) {
        return false;
    }

    @Override
    public Filter getFilter() {
        return countryFilter;
    }

    private Filter countryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            ArrayList<ModelTrip> suggestions = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(mDataFiltered);
                results.values = modelTrips;
                results.count = modelTrips.size();
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ModelTrip item : mDataFiltered) {
                    if (item.getModelTypeOfTrip().getTypeEN().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);

                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            modelTrips = (ArrayList<ModelTrip>) results.values;
            notifyDataSetChanged();
        }

    };


}
