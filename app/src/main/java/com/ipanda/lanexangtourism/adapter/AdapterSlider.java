package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;

public class AdapterSlider extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public AdapterSlider(Context context){
        this.context = context;
    }
    public int[] slide_images = {
            R.drawable.logo,
            R.drawable.slide2,
            R.drawable.slide3
    };
    public String[] slide_headings = {
            "ยินดีต้อนรับสู่\nLANEXANG INFO",
            "ให้การค้นหาสถานที่เป็น\nเรื่องง่าย",
            "การท่องเที่ยวที่สะดวกขึ้น"
    };
    public String[] slide_desc ={
            "เที่ยวชิล ๆ พื้นที่สี่เหลี่ยมวัฒนธรรมล้านช้าง และเขตพัฒนาการท่องเที่ยววิถีชีวิตลุ่มแม่น้ำโขง ระหว่างราชอาณาจักรไทย – สปป.ลาว กับสถานที่สุดประทับใจ",
            "ค้นหาสถานที่ท่องเที่ยว กิจกรรมการท่องเที่ยวน่าสนใจ ร้านอาหารและโรงแรมที่ใช่เพื่อคุณ ได้อย่างง่ายดาย",
            "แหล่งรวบรวมข้อมูลเพื่อให้การท่องเที่ยวของคุณง่ายขึ้น "
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }
    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == (ConstraintLayout) o;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.slide_image);
        TextView slideHeading = (TextView) view.findViewById(R.id.slide_heading);
        TextView slideDescription = (TextView) view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_desc[position]);

        container.addView(view);

        return view;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
