package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelUsers;
import com.ipanda.lanexangtourism.model.util.RecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterUserMatching extends RecyclerView.Adapter<AdapterUserMatching.MyViewHolderUser>{

    private Context context;
    private ArrayList<ModelUsers> modelUsers;

    public AdapterUserMatching(ArrayList<ModelUsers> modelUsers,Context context) {
        this.modelUsers = modelUsers;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolderUser onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user_matching, parent, false);

        return new MyViewHolderUser(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderUser holderUser, int position) {
        final ModelUsers users = (ModelUsers) modelUsers.get(position);

//        Picasso.get().load(users.getPersonPhoto()).placeholder(R.drawable.ic_man).error(R.drawable.ic_man).into(holderUser.profile_img);
        Picasso.get().load(users.getPersonPhoto()).placeholder(R.drawable.ic_man).error(R.drawable.ic_man).into(holderUser.profile_img);
        holderUser.text_full_name.setText(users.getName().replace("||"," "));
        holderUser.login_with.setText(users.getLoginWith());

    }

    @Override
    public int getItemCount() {
        return modelUsers == null ? 0 : modelUsers.size();
    }

    class MyViewHolderUser extends RecyclerView.ViewHolder {

        ImageView profile_img;
        TextView text_full_name,login_with;

        public MyViewHolderUser(View itemView) {
            super(itemView);
            profile_img = itemView.findViewById(R.id.profile_img);
            text_full_name = itemView.findViewById(R.id.text_full_name);
            login_with = itemView.findViewById(R.id.login_with);
        }

    }


}


