package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelProgramTour;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterProgramTour extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<ModelProgramTour> arrayList_ProgramTour;

    public AdapterProgramTour(Context context,ArrayList<ModelProgramTour> arrayList_ProgramTour){

        this.context = context;
        this.arrayList_ProgramTour = arrayList_ProgramTour;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList_ProgramTour.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList_ProgramTour.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ModelProgramTour tour = (ModelProgramTour) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


       View view = mInflater.inflate(R.layout.layout_list_program_tour, parent, false);

        ImageView imageView = view.findViewById(R.id.img_program_tour);
        TextView textProgramTour = view.findViewById(R.id.text_program_tour);

        Picasso.get().load(tour.getPathImg()).error(R.drawable.image_placeholder).into(imageView);

        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                textProgramTour.setText(tour.getProgramTourTH());
                break;
            case "en":
                if (tour.getProgramTourEN().equals("") || tour.getProgramTourEN() == null || tour.getProgramTourEN().equals("null") ) {
                    textProgramTour.setText(tour.getProgramTourTH());
                } else {
                    textProgramTour.setText(tour.getProgramTourEN());
                }
                textProgramTour.setText(tour.getProgramTourEN());
                break;
            case "lo":
                if (tour.getProgramTourLAO().equals("") || tour.getProgramTourLAO() == null || tour.getProgramTourLAO().equals("null") ) {
                    textProgramTour.setText(tour.getProgramTourTH());
                } else {
                    textProgramTour.setText(tour.getProgramTourLAO());
                }
                break;
        }


        return view;
    }
}
