package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterViewPagerItmes extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<ModelFilePaths> modelFilePaths;
    private Integer [] images = {R.drawable.sticker1,R.drawable.sticker2,R.drawable.sticker3};

    public AdapterViewPagerItmes(Context context, ArrayList<ModelFilePaths> modelFilePaths){
        this.context = context;
        this.modelFilePaths = modelFilePaths;
    }

    @Override
    public int getCount() {
        return modelFilePaths.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_items_viewpager, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_item_viewpager);

        Picasso.get().load(modelFilePaths.get(position).getPathImg()).into(imageView);
//        imageView.setImageResource(images[position]);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}
