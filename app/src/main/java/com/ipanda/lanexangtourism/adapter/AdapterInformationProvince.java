package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;

import java.util.ArrayList;

public class AdapterInformationProvince extends BaseAdapter {

    private ArrayList<ModelTravelInformation> modelTravelInformation;
    Context mContext;
    public AdapterInformationProvince(Context context,  ArrayList<ModelTravelInformation> modelTravelInformation){
        this.mContext= context;
        this.modelTravelInformation = modelTravelInformation;
    }
    @Override
    public int getCount() {
        return modelTravelInformation.size();
    }

    @Override
    public Object getItem(int position) {
        return modelTravelInformation.get(position);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final ModelTravelInformation item = (ModelTravelInformation) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null)
            view = mInflater.inflate(R.layout.layout_item_information_province, parent, false);

        TextView strName_province = (TextView)view.findViewById(R.id.NameProvince);

        switch (mContext.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                strName_province.setText(item.getTopic_th());
                break;
            case "en":
                strName_province.setText(item.getTopic_en());
                break;
            case "lo":
                strName_province.setText(item.getTopic_lao());
                break;
        }


        return view;
    }
}
