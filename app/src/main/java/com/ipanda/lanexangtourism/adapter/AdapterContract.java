package com.ipanda.lanexangtourism.adapter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelEmergency;

import java.util.ArrayList;

public class AdapterContract extends BaseAdapter {

    private ArrayList<ModelEmergency> modelEmergency;
    private Context mContext;
    private String select;
    private Location locationResult;
    private String app_mode;

    public AdapterContract(Context context, ArrayList<ModelEmergency> modelEmergency, String select, Location locationResult, String app_mode) {
        this.mContext = context;
        this.modelEmergency = modelEmergency;
        this.select = select;
        this.locationResult = locationResult;
        this.app_mode = app_mode;

    }

    public int getCount() {
        return modelEmergency.size();
    }

    public Object getItem(int position) {
        return modelEmergency.get(position);
    }

    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final ModelEmergency item = (ModelEmergency) getItem(position);

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null)
            view = mInflater.inflate(R.layout.layout_item_contract, parent, false);

        TextView textstrName_Contract = (TextView) view.findViewById(R.id.name_contract);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_contract);
        ImageView contact_navi = (ImageView) view.findViewById(R.id.contact_navi);
        ImageView callPhone = (ImageView) view.findViewById(R.id.callPhone);

        String ImageID = "ic_" + select;
        int ImageResID = mContext.getResources().getIdentifier(ImageID, "drawable", mContext.getPackageName());

        imageView.setImageResource(ImageResID);

        String Topic = "";
        switch (mContext.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                textstrName_Contract.setText(item.getTopicTH());
                Topic = item.getTopicTH();
                break;
            case "en":
                if (item.getTopicEN().equals("") || item.getTopicEN() == null || item.getTopicEN().equals("null")) {
                    textstrName_Contract.setText(item.getTopicTH());
                    Topic = item.getTopicTH();
                } else {
                    textstrName_Contract.setText(item.getTopicEN());
                    Topic = item.getTopicEN();
                }
                break;
            case "lo":
                if (item.getTopicLAO().equals("") || item.getTopicLAO() == null || item.getTopicLAO().equals("null")) {
                    textstrName_Contract.setText(item.getTopicTH());
                    Topic = item.getTopicTH();
                } else {
                    textstrName_Contract.setText(item.getTopicLAO());
                    Topic = item.getTopicLAO();
                }
                break;
        }

        contact_navi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app_mode.equals("offline")){
                    alertDialogCheckOffLineMode();
                }else {
                    String url = "http://maps.google.com/maps?saddr=" + item.getLatitude() + "," + item.getLongitude() + "&daddr=" + locationResult.getLatitude() + "," + locationResult.getLongitude() + "&mode=driving";
                    Uri gmmIntentUri = Uri.parse(url);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    mContext.startActivity(mapIntent);
//                Toast.makeText(mContext, "Lat: "+item.getLatitude() + " Lng " +item.getLongitude()+"Me -> Lat"+locationResult.getLatitude(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        String finalTopic = Topic;
        callPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getTel().length() != 0) {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                    alertDialog.setTitle("LANEXANG INFO");
                    alertDialog.setMessage(mContext.getResources().getString(R.string.text_callPhone) + "\n" + finalTopic + "\n" + item.getTel() + " ?");
                    alertDialog.setIcon(R.drawable.ic_tel);
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + item.getTel()));
                            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                mContext.startActivity(callIntent);
                            }
                            Toast.makeText(mContext, "Phone: " + item.getTel(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    //enter code here
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            }

        });

        return view;
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage("    "+ mContext.getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(mContext.getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
