package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterDescription extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<ModelItem> modelItems;

    private LinearLayout linearLayout;

    public AdapterDescription(Context context,  ArrayList<ModelItem> modelItems) {
        this.context = context;
        this.mInflater  = LayoutInflater.from(context);
        this.modelItems = modelItems;
    }

    @Override
    public int getCount() {
        return modelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return modelItems.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        final ModelItem item = (ModelItem) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.layout_list_trip_description, parent, false);

        ImageView imgDescription = (ImageView) convertView.findViewById(R.id.img_item_description);
        TextView textViewTime = (TextView) convertView.findViewById(R.id.description_time);
        TextView textViewDetail = (TextView) convertView.findViewById(R.id.description_details);
        TextView textViewTopic = (TextView) convertView.findViewById(R.id.description_topic);
        TextView textViewAddress = (TextView) convertView.findViewById(R.id.description_address);
        LinearLayout linearLayout =(LinearLayout) convertView.findViewById(R.id.linearlayout_trip_desc);

        Picasso.get().load(item.getModelFilePaths().getPathImg()).into(imgDescription);
        textViewTime.setText(item.getTimeStart()+" - "+item.getTimeEnd());

        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                textViewDetail.setText(item.getDetailTH());
                textViewTopic.setText(item.getTopicTH());
                textViewAddress.setText(context.getResources().getString(R.string.text_address)+" "+item.getAddressTH());
                break;
            case "en":
                if (item.getTopicEN().equals("") || item.getTopicEN() == null || item.getTopicEN().equals("null")) {
                    textViewDetail.setText(item.getDetailTH());
                    textViewTopic.setText(item.getTopicTH());
                    textViewAddress.setText(context.getResources().getString(R.string.text_address)+" "+item.getAddressTH());
                } else {
                    textViewDetail.setText(item.getDetailEN());
                    textViewTopic.setText(item.getTopicEN());
                    textViewAddress.setText(context.getResources().getString(R.string.text_address)+" "+item.getAddressEN());
                }
                break;
            case "lo":
                if (item.getTopicLAO().equals("") || item.getTopicLAO() == null || item.getTopicLAO().equals("null")) {
                    textViewDetail.setText(item.getDetailTH());
                    textViewTopic.setText(item.getTopicTH());
                    textViewAddress.setText(context.getResources().getString(R.string.text_address)+" "+item.getAddressTH());
                } else {
                    textViewDetail.setText(item.getDetailLAO());
                    textViewTopic.setText(item.getTopicLAO());
                    textViewAddress.setText(context.getResources().getString(R.string.text_address)+" "+item.getAddressLAO());
                }
                break;
        }





        if(position % 2 == 0)
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorBGTripDesc));
        else
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));

        return convertView;
    }
}
