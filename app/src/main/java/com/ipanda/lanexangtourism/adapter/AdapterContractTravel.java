package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterContractTravel extends BaseAdapter {
    private Context mContext;
    private ImageView imageView;
    private ArrayList<ModelTypeOfTravel> modelTypeOfTravel;

    public AdapterContractTravel(Context context, ArrayList<ModelTypeOfTravel> modelTypeOfTravel){
        this.mContext= context;
        this.modelTypeOfTravel = modelTypeOfTravel;

    }

    public int getCount() {
        return modelTypeOfTravel.size();
    }

    public Object getItem(int position) {
        return modelTypeOfTravel.get(position);
    }
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final ModelTypeOfTravel item = (ModelTypeOfTravel) getItem(position);
        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view == null)
            view = mInflater.inflate(R.layout.layout_item_contract_travel, parent, false);

        ImageView imageView = view.findViewById(R.id.travelImg);
        TextView travelName = view.findViewById(R.id.travelName);

        Picasso.get().load(mContext.getResources().getIdentifier("travelid"+ item.getIdTypeOfTravel(), "drawable", mContext.getPackageName())).into(imageView);
        switch (mContext.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                travelName.setText(item.getTypeTH());
                break;
            case "en":
                travelName.setText(item.getTypeEN());
                break;
            case "lo":
                travelName.setText(item.getTypeLAO());
                break;
        }


        return view;
    }
}
