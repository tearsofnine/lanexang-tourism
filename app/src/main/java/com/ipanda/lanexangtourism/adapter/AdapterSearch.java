package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.util.ItemClickFavorite;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterSearch extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<ModelItem> modelItems;
    private ItemClickFavorite mItemClickFavorite;

    public AdapterSearch(Context context, ArrayList<ModelItem> modelItems,ItemClickFavorite mItemClickFavorite){
        this.context = context;
        this.modelItems = modelItems;
        mInflater = LayoutInflater.from(context);
        this.mItemClickFavorite = mItemClickFavorite;
    }

    @Override
    public int getCount() {
        return modelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return modelItems.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ModelItem item = (ModelItem) getItem(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = mInflater.inflate(R.layout.list_filter, parent, false);

            ImageView imageView = convertView.findViewById(R.id.img_item_list_great_places);
            TextView txtTitle = (TextView) convertView.findViewById(R.id.title_item);
            TextView txtTopic = (TextView) convertView.findViewById(R.id.topic_item);
            TextView txtdistance = (TextView) convertView.findViewById(R.id.distance);

            ImageView imgFavorite = (ImageView) convertView.findViewById(R.id.img_favorite);
            ImageView imgPosition = (ImageView) convertView.findViewById(R.id.img_position);

            TextView text_favorite = (TextView) convertView.findViewById(R.id.TextFavorite);
            TextView text_near = (TextView) convertView.findViewById(R.id.TextNear);

            text_favorite.setText(context.getResources().getString(R.string.text_favorite));
            text_near.setText(context.getResources().getString(R.string.text_near));

            Picasso.get().load(item.getModelFilePaths().getPathImg()).into(imageView);


            switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                case "th":
                    txtTitle.setText(item.getTopicTH());
                    txtTopic.setText(item.getAddressTH());
                    break;
                case "en":
                    if (item.getTopicEN().equals("") || item.getTopicEN() == null || item.getTopicEN().equals("null")) {
                        txtTitle.setText(item.getTopicTH());
                        txtTopic.setText(item.getAddressTH());
                    } else {
                        txtTitle.setText(item.getTopicEN());
                        txtTopic.setText(item.getAddressEN());
                    }
                    break;
                case "lo":
                    if (item.getTopicLAO().equals("") || item.getTopicLAO() == null || item.getTopicLAO().equals("null")) {
                        txtTitle.setText(item.getTopicTH());
                        txtTopic.setText(item.getAddressTH());
                    } else {
                        txtTitle.setText(item.getTopicLAO());
                        txtTopic.setText(item.getAddressLAO());
                    }
                    break;
            }


            txtdistance.setText(item.getDistance());
            imgFavorite.setImageResource(R.drawable.ic_favorite);
            imgFavorite.setColorFilter(null);
            if (item.isFavorites()){
                imgFavorite.setColorFilter(context.getResources().getColor(R.color.red));
            }
            else
            {
                imgFavorite.setColorFilter(context.getResources().getColor(R.color.darkGrey));
            }

            imgPosition.setImageResource(R.mipmap.ic_position);

        imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickFavorite.itemClicked(item,imgFavorite);
            }
        });



        return convertView;
    }
}
