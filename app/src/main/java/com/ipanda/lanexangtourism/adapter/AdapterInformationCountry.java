package com.ipanda.lanexangtourism.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.squareup.picasso.Picasso;

public class AdapterInformationCountry extends BaseAdapter {

    private String[] img_Country;
    private Context mContext;
    private String[] NameCountry;
    public AdapterInformationCountry(Context context,String[] imgCountry){
        this.mContext= context;
        this.img_Country = imgCountry;
        NameCountry = mContext.getResources().getStringArray(R.array.NameCountry);
    }
    @Override
    public int getCount() {

        return NameCountry.length;
    }

    @Override
    public Object getItem(int position) {
        return NameCountry[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null)
            view = mInflater.inflate(R.layout.layout_item_information_country, parent, false);

        TextView textStrname_Country = (TextView)view.findViewById(R.id.Namecountry);
        ImageView imageView = (ImageView)view.findViewById(R.id.img_country);

        textStrname_Country.setText(NameCountry[position]);
        Picasso.get().load(mContext.getResources().getIdentifier(img_Country[position], "drawable", mContext.getPackageName())).into(imageView);


        return view;
    }
}
