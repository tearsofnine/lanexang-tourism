package com.ipanda.lanexangtourism;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.model.ModelEmergency;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelProgramTour;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;
import com.ipanda.lanexangtourism.model.ModelTrip;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;
import com.ipanda.lanexangtourism.model.ModelTypeOfTrip;
import com.ipanda.lanexangtourism.model.util.Version;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static android.support.constraint.Constraints.TAG;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout[] LinearLayoutLanguage;
    private LinearLayout[] LinearLayoutSelectLanguage;
    private Button btn_online,btn_offline,btn_download;
    private CardView map_type_1,map_type_2,map_type_3;
    private DatabaseHelper myDB;
    private ProgressDialog alertDialog = null;
    private SimpleDateFormat dateFormat;
    private Calendar calendar;

    private TextView datetime_id;
    private ArrayList<ModelProgramTour> arrayListsProgramTour;
    private ArrayList<ModelItem> arrayListItem;
    private ArrayList<ModelTypeOfTravel> arrayListTypeOfTravel;
    private ArrayList<ModelEmergency> arrayListsEmergency;
    private ArrayList<ModelTravelInformation> arrayListsInformation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        btn_download = findViewById(R.id.btn_download);
        btn_download.setOnClickListener(this);
        myDB = new DatabaseHelper(this);
        datetime_id = findViewById(R.id.datetime_id);


        String app_last_sync = getSharedPreferences("PREF_APP_LAST_SYNC", Context.MODE_PRIVATE).getString("APP_LAST_SYNC","");
        datetime_id.setText(app_last_sync);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar_title.setText(R.string.action_settings);

        Initializing(3,getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th"));
        SetAppMode(getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","online"));
        SetMapType(getSharedPreferences("PREF_MAP_TYPE", Context.MODE_PRIVATE).getString("MAP_TYPE","standard"));

        Button button = findViewById(R.id.btn_regis);

        if(getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken","") != "") {
            button.setText(getResources().getText(R.string.Seting_tVLogoutBtn));
        }
        else {
            button.setText(getResources().getText(R.string.Seting_tVRegisBtn));
        }

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(SettingActivity.this, SocialLoginHelperActivity.class);
                    startActivity(intent);
            }
        } );


        btn_online.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SetAppMode("online");
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
        btn_offline.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                boolean verifyDownload = getSharedPreferences("PREF_APP_DOWNLOAD", Context.MODE_PRIVATE).getBoolean("APP_DOWNLOAD",false);
                if (verifyDownload){
                    SetAppMode("offline");
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }else {
                    alertDialogVerifyDownLoadOffLineMode();
                }

            }
        });

        map_type_1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SetMapType("standard");
            }
        });
        map_type_2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SetMapType("satellite");
            }
        });
        map_type_3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SetMapType("hybrid");
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void Initializing(int numlanguage,String language) {
        LinearLayoutLanguage = new LinearLayout[numlanguage];
        LinearLayoutSelectLanguage = new LinearLayout[numlanguage];
        for(int i=0; i<numlanguage; i++) {
            String LanguageID = "language_" + (i+1) ;
            int LanguageResID = getResources().getIdentifier(LanguageID, "id", getPackageName());

            String SelectLanguageID = "language_category_" + (i+1) ;
            int SelectLanguageResID = getResources().getIdentifier(SelectLanguageID, "id", getPackageName());

            LinearLayoutLanguage[i]=(LinearLayout)findViewById(LanguageResID);
            LinearLayoutSelectLanguage[i]=(LinearLayout)findViewById(SelectLanguageResID);

            LinearLayoutLanguage[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClickSelectLanguage(v);
                }
            });


        }
        String[] _language;
        _language = getResources().getStringArray(R.array.language);
        LinearLayoutSelectLanguage[Arrays.asList(_language).indexOf(language)].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circular_background_select, null));

        btn_online = findViewById(R.id.btn_online);
        btn_offline = findViewById(R.id.btn_offline);
        btn_download= findViewById(R.id.btn_download);

        map_type_1 = findViewById(R.id.map_standard);
        map_type_2 = findViewById(R.id.map_satellite);
        map_type_3 = findViewById(R.id.map_hybrid);
    }

    private void SetMapType(String type){
        switch (type) {
            case "standard":
                map_type_1.setCardBackgroundColor(getResources().getColor(R.color.clr2));
                map_type_2.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                map_type_3.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                getSharedPreferences("PREF_MAP_TYPE", Context.MODE_PRIVATE).edit().putString("MAP_TYPE", "standard").apply();
                break;
            case "satellite":
                map_type_1.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                map_type_2.setCardBackgroundColor(getResources().getColor(R.color.clr2));
                map_type_3.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                getSharedPreferences("PREF_MAP_TYPE", Context.MODE_PRIVATE).edit().putString("MAP_TYPE", "satellite").apply();
                break;
            case "hybrid":
                map_type_1.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                map_type_2.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
                map_type_3.setCardBackgroundColor(getResources().getColor(R.color.clr2));
                getSharedPreferences("PREF_MAP_TYPE", Context.MODE_PRIVATE).edit().putString("MAP_TYPE", "hybrid").apply();
                break;
        }
    }

    private void SetAppMode(String mode) {
        switch (mode) {
            case "online":
                btn_online.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_app_mode_s, null));
                btn_online.setTextColor((getResources().getColor(R.color.colorWhite)));

                btn_offline.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_app_mode, null));
                btn_offline.setTextColor((getResources().getColor(R.color.clr2)));
                getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).edit().putString("APP_MODE", "online").apply();
                break;
            case "offline":
                btn_online.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_app_mode, null));
                btn_online.setTextColor((getResources().getColor(R.color.clr2)));

                btn_offline.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_app_mode_s, null));
                btn_offline.setTextColor((getResources().getColor(R.color.colorWhite)));
                getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).edit().putString("APP_MODE", "offline").apply();

                break;
        }

    }

    private void SetClickNavigation(int select) {
        for(int i=0;i<LinearLayoutLanguage.length;i++){
            if(select == (i+1))
                LinearLayoutSelectLanguage[i].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circular_background_select, null));
            else
                LinearLayoutSelectLanguage[i].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circular_background_unselect, null));
        }
    }

    public void ClickSelectLanguage(View view){
        Configuration config;
        config = new Configuration();

        SharedPreferences sp = getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        switch (view.getId()) {
            case R.id.language_1:
                SetClickNavigation(1);
                config.locale = new Locale("th");
                editor.putString("LANGUAGE", "th");
                break;
            case R.id.language_2:
                SetClickNavigation(2);
                config.locale = new Locale("en");
                editor.putString("LANGUAGE", "en");
                break;
            case R.id.language_3:
                SetClickNavigation(3);
                config.locale = new Locale("lo");
                editor.putString("LANGUAGE", "lo");
                break;
        }

        editor.apply();
        getResources().updateConfiguration(config, null);
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View v) {
        boolean result = isNetworkConnected();
        try {
            if (result){
                SQLiteOpenHelper helper = new DatabaseHelper(this);
                SQLiteDatabase db = helper.getWritableDatabase();
//                myDB.addVersion(new Version(1,1));
                Version myDBVersion = myDB.getVersion(1);
                myDB.updateVersion(new Version(myDBVersion.getId(),myDBVersion.getUpGrade()+1));
                myDB.onUpgrade(db,myDBVersion.getUpGrade(),(myDBVersion.getUpGrade()+1));
                contentDownLoadOffline();
                calendar = Calendar.getInstance();
                dateFormat = new SimpleDateFormat("dd MM yyyy");
                String date = dateFormat.format(calendar.getTime());
                getSharedPreferences("PREF_APP_LAST_SYNC", Context.MODE_PRIVATE).edit().putString("APP_LAST_SYNC", getResources().getString(R.string.text_last_update)+" "+date).apply();
                datetime_id.setText(getResources().getString(R.string.text_last_update)+" "+date);
                getSharedPreferences("PREF_APP_DOWNLOAD", Context.MODE_PRIVATE).edit().putBoolean("APP_DOWNLOAD", true).apply();
            }else {
                alertDialogCheckOffLineMode();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void showProgressDialogDownload(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage("Download...");
        alertDialog.setCancelable(false);
        alertDialog.show();
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void contentDownLoadOffline(){
        String[] url_json = {
                getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/GetProgramTour",
                getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getDataItme?idTypeOfItems=ALL",
                getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getEmergencyTravel",
                getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getEmergencyALL",
                getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getDataTravelInformationALL"
        };
        new DownloadProgramTour().execute(url_json[0]);
        new DownloadItems().execute(url_json[1]);
        new DownloadEmergencyTravel().execute(url_json[2]);
        new DownloadEmergency().execute(url_json[3]);
        new DownloadInformation().execute(url_json[4]);

    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogDownLoadOffLineMode(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_download_offline_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogVerifyDownLoadOffLineMode(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_verify_download));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogConnectInternet(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class DownloadProgramTour extends AsyncTask<String,Integer,String>{

        public DownloadProgramTour(){
            showProgressDialogDownload();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i(TAG,"Call Service");
                return downloadContent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            arrayListsProgramTour = new ArrayList<ModelProgramTour>();
            arrayListsProgramTour = onParserContentToModel(s);
            myDB.addProgramTour(arrayListsProgramTour);
        }

        private String downloadContent(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                String result = convertInputStreamToString(is);
                return result;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String convertInputStreamToString(InputStream is) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine())!=null){
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        }

        public ArrayList<ModelProgramTour> onParserContentToModel(String dataJSon) {
//            Log.e("data json", dataJSon);
            ArrayList<ModelProgramTour> arrayList = new ArrayList<>();

            try {
                JSONObject jsonObject = new JSONObject(dataJSon);
                JSONArray jsonArray = jsonObject.optJSONArray("ProgramTour");

                if (jsonArray != null){
                    for(int i=0;i < jsonArray.length();i++) {
//                        Log.e("Message", "loop");
                        ModelProgramTour model = new ModelProgramTour();
                        JSONObject json = jsonArray.optJSONObject(i);
                        model.setProgramTourID(json.getInt("idProgramTour"));
                        model.setProgramTourTH(json.getString("name_th"));
                        model.setProgramTourEN(json.getString("name_en"));
                        model.setProgramTourLAO(json.getString("name_lao"));
                        model.setPathImg(json.getString("path"));
                        ArrayList<ModelTrip> arrayListTrips = new ArrayList<>();
                        JSONArray jsonTrip = jsonObject.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("Trip");
                        for(int j=0;j < jsonTrip.length();j++) {
//                            Log.e("Message", "loop");
                            ModelTrip modelTrip = new ModelTrip();
                            ModelTypeOfTrip type = new ModelTypeOfTrip();
                            JSONObject jTrip = jsonTrip.optJSONObject(j);
                            modelTrip.setTripID(jTrip.getInt("idTrip"));
                            modelTrip.setTripTH(jTrip.getString("trip_th"));
                            modelTrip.setTripEN(jTrip.getString("trip_en"));
                            modelTrip.setTripLAO(jTrip.getString("trip_lao"));
                            modelTrip.setPathImg(jTrip.getString("path"));
                            modelTrip.setAreaTH(jTrip.getString("areaTH"));
                            modelTrip.setAreaEN(jTrip.getString("areaEN"));
                            modelTrip.setAreaLAO(jTrip.getString("areaLAO"));
                            modelTrip.setKey(jTrip.getInt("key"));
                            type.setTypeTH(jTrip.getString("types_th"));
                            type.setTypeEN(jTrip.getString("types_en"));
                            type.setTypeLAO(jTrip.getString("types_lao"));
                            modelTrip.setModelTypeOfTrip(type);

                            ArrayList<ModelItem> arrayListItems = new ArrayList<>();
                            JSONArray jsonItems = jsonObject.optJSONArray("ProgramTour").getJSONObject(i).getJSONArray("Trip").getJSONObject(j).getJSONArray("days");
                            for(int k=0;k < jsonItems.length();k++) {
//                                Log.e("Message", "loop");
                                ModelItem items = new ModelItem();
                                ModelTypeOfItem typeOfItem = new ModelTypeOfItem();
                                ModelFilePaths paths = new ModelFilePaths();
                                JSONObject jItem = jsonItems.optJSONObject(k);

                                items.setItemID(jItem.getString("idItems"));
                                typeOfItem.setTypeID(Integer.parseInt(jItem.getString("idTypeOfItems")));
                                typeOfItem.setTypeTH(jItem.getString("type_th"));
//                                typeOfItem.setTypeEN(jItem.getString("type_en"));
//                                typeOfItem.setTypeLAO(jItem.getString("type_lao"));
                                items.setModelTypeOfItem(typeOfItem);
                                items.setTopicTH(jItem.getString("topic_th"));
                                items.setTopicEN(jItem.getString("topic_en"));
                                items.setTopicLAO(jItem.getString("topic_lao"));
                                items.setAddressTH(jItem.getString("address_th"));
                                items.setAddressEN(jItem.getString("address_en"));
                                items.setAddressLAO(jItem.getString("address_lao"));
                                items.setDetailTH(jItem.getString("details_th"));
                                items.setDetailEN(jItem.getString("details_en"));
                                items.setDetailLAO(jItem.getString("details_lao"));
                                items.setDateOpen(jItem.getString("dateOpen"));
                                items.setDateClose(jItem.getString("dateClose"));
                                items.setLatitude(jItem.getDouble("latitude"));
                                items.setLongitude(jItem.getDouble("longitude"));
                                items.setTel(jItem.getString("tel"));
                                items.setTimeStart(jItem.getString("timeStart"));
                                items.setTimeEnd(jItem.getString("timeEnd"));
                                paths.setPathImg(jItem.getString("path"));
                                items.setModelFilePaths(paths);

                                arrayListItems.add(items);
                            }

                            modelTrip.setModelItems(arrayListItems);
                            arrayListTrips.add(modelTrip);
                        }
                        model.setModelTrips(arrayListTrips);
                        arrayList.add(model);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            return arrayList;
        }


    }

    private class DownloadItems extends AsyncTask<String,Integer,String>{

        public DownloadItems(){ }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i(TAG,"Call Service");
                return downloadContent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                arrayListItem = new ArrayList<>();
                arrayListItem = onParserContentToModel(s);
                myDB.addItems(arrayListItem);
        }

        private String downloadContent(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                String result = convertInputStreamToString(is);
                return result;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String convertInputStreamToString(InputStream is) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine())!=null){
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        }

        public ArrayList<ModelItem> onParserContentToModel(String dataJSon) {
            Log.e("data json", dataJSon);
            ArrayList<ModelItem> arrayList = new ArrayList<>();
            try {

                JSONArray jsonArray = new JSONArray(dataJSon);

                if (jsonArray != null){
                    for(int i=0;i < jsonArray.length();i++) {
                        Log.e("Message", "loop");

                        ModelItem items = new ModelItem();
                        ModelTypeOfItem typeOfItem = new ModelTypeOfItem();
                        ModelFilePaths paths = new ModelFilePaths();
                        JSONObject json = jsonArray.optJSONObject(i);

                        items.setItemID(json.getString("idItems"));
                        typeOfItem.setTypeID(Integer.parseInt(json.getString("idTypeOfItems")));
                        items.setModelTypeOfItem(typeOfItem);
                        items.setTopicTH(json.getString("topic_th"));
                        items.setTopicEN(json.getString("topic_en"));
                        items.setTopicLAO(json.getString("topic_lao"));
                        items.setAddressTH(json.getString("address_th"));
                        items.setAddressEN(json.getString("address_en"));
                        items.setAddressLAO(json.getString("address_lao"));
                        items.setDetailTH(json.getString("details_th"));
                        items.setDetailEN(json.getString("details_en"));
                        items.setDetailLAO(json.getString("details_lao"));
                        items.setDateOpen(json.getString("dateOpen"));
                        items.setDateClose(json.getString("dateClose"));
                        items.setTel(json.getString("tel"));
                        items.setTimeStart(json.getString("timeStart"));
                        items.setTimeEnd(json.getString("timeEnd"));
                        items.setDistance(json.getString("distance"));
                        items.setLatitude(json.getDouble("latitude"));
                        items.setLongitude(json.getDouble("longitude"));
//                        items.setFavorites(json.getBoolean("favorites"));
                        paths.setPathImg(json.getString("path"));
                        items.setModelFilePaths(paths);

                        arrayList.add(items);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            return arrayList;
        }

    }

    private class DownloadEmergencyTravel extends AsyncTask<String,Integer,String>{

        public DownloadEmergencyTravel() {
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i(TAG,"Call Service");
                return downloadContent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            arrayListTypeOfTravel = new ArrayList<>();
            arrayListTypeOfTravel = onParserContentToModel(s);
            myDB.addEmergencyTravel(arrayListTypeOfTravel);
        }

        private String downloadContent(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                String result = convertInputStreamToString(is);
                return result;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String convertInputStreamToString(InputStream is) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine())!=null){
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        }

        public ArrayList<ModelTypeOfTravel> onParserContentToModel(String dataJSon) {
//            Log.e("data json", dataJSon);
            ArrayList<ModelTypeOfTravel> arrayList = new ArrayList<>();


            try {
                JSONObject jsonObject = new JSONObject(dataJSon);
                JSONArray jsontypeoftravel = jsonObject.optJSONArray("typeoftravel");
                JSONArray jsonEmergencyTravel = jsonObject.optJSONArray("EmergencyTravel");

                if (jsontypeoftravel != null){
                    for(int i=0;i < jsontypeoftravel.length();i++) {
//                        Log.e("Message", "loop");
                        ModelTypeOfTravel modelTypeOfTravel = new ModelTypeOfTravel();
                        ArrayList<ModelItem> list_emergency = new ArrayList<>();

                        JSONObject js_typeoftravel = jsontypeoftravel.optJSONObject(i);
                        modelTypeOfTravel.setIdTypeOfTravel(js_typeoftravel.getInt("idTypeOfTravel"));
                        modelTypeOfTravel.setTypeTH(js_typeoftravel.getString("type_th"));
                        modelTypeOfTravel.setTypeEN(js_typeoftravel.getString("type_en"));
                        modelTypeOfTravel.setTypeLAO(js_typeoftravel.getString("type_lao"));

                        for (int j = 0; j < jsonEmergencyTravel.length() ; j++){
                            ModelItem modelEmergency = new ModelItem();
                            ModelFilePaths paths = new ModelFilePaths();
                            ModelTypeOfItem modelTypeOfItem = new ModelTypeOfItem();
                            JSONObject js_item_emergency = jsonEmergencyTravel.optJSONObject(j);
                            if (js_typeoftravel.getInt("idTypeOfTravel") == js_item_emergency.getInt("idTypeOfTravel")) {

                                modelEmergency.setItemID(String.valueOf(js_item_emergency.getInt("idEmergency")));
                                modelEmergency.setTopicTH(js_item_emergency.getString("topic_th"));
                                modelEmergency.setTopicEN(js_item_emergency.getString("topic_en"));
                                modelEmergency.setTopicLAO(js_item_emergency.getString("topic_lao"));
                                modelEmergency.setAddressTH(js_item_emergency.getString("address_th"));
                                modelEmergency.setAddressEN(js_item_emergency.getString("address_en"));
                                modelEmergency.setAddressLAO(js_item_emergency.getString("address_lao"));
                                modelEmergency.setDetailTH(js_item_emergency.getString("details_th"));
                                modelEmergency.setDetailEN(js_item_emergency.getString("details_en"));
                                modelEmergency.setDetailLAO(js_item_emergency.getString("details_lao"));
                                modelEmergency.setDateOpen(js_item_emergency.getString("dateOpen"));
                                modelEmergency.setDateClose(js_item_emergency.getString("dateClose"));
                                modelEmergency.setTel(js_item_emergency.getString("tel"));
                                modelEmergency.setTimeStart(js_item_emergency.getString("timeStart"));
                                modelEmergency.setTimeEnd(js_item_emergency.getString("timeEnd"));
                                modelEmergency.setLatitude(js_item_emergency.getDouble("latitude"));
                                modelEmergency.setLongitude(js_item_emergency.getDouble("longitude"));
                                modelEmergency.setDistance(js_item_emergency.getString("distance"));
                                modelEmergency.setDistance_metr(js_item_emergency.getInt("distance_metr"));
                                modelTypeOfItem.setTypeID(js_item_emergency.getInt("idTypeOfTravel")+10);
                                modelEmergency.setFavorites(js_item_emergency.getBoolean("favorites"));
                                modelEmergency.setModelTypeOfItem(modelTypeOfItem);

                                paths.setPathImg(js_item_emergency.getString("path"));
                                modelEmergency.setModelFilePaths(paths);
                                list_emergency.add(modelEmergency);
                            }
                        }
                        modelTypeOfTravel.setModelEmergency(list_emergency);
                        arrayList.add(modelTypeOfTravel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            return arrayList;
        }
    }

    private class DownloadEmergency extends AsyncTask<String,Integer,String>{

        public DownloadEmergency(){
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i(TAG,"Call Service");
                return downloadContent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                arrayListsEmergency = new ArrayList<>();
                arrayListsEmergency = onParserContentToModel(s);
                myDB.addEmergency(arrayListsEmergency);

        }

        private String downloadContent(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                String result = convertInputStreamToString(is);
                return result;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String convertInputStreamToString(InputStream is) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine())!=null){
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        }

        public ArrayList<ModelEmergency> onParserContentToModel(String dataJSon) {
//            Log.e("data json", dataJSon);
            ArrayList<ModelEmergency> arrayList = new ArrayList<>();
            try {

                JSONArray jsonArray = new JSONArray(dataJSon);

                if (jsonArray != null){
                    for(int i=0;i < jsonArray.length();i++) {
//                        Log.e("Message", "loop");
                        ModelEmergency emergency = new ModelEmergency();
                        ModelFilePaths paths = new ModelFilePaths();
                        JSONObject json = jsonArray.optJSONObject(i);
                        emergency.setEmergencyID(json.getString("idEmergency"));
                        emergency.setIdTypeOfEmergency(json.getString("idTypeOfEmergency"));
                        emergency.setTopicTH(json.getString("topic_th"));
                        emergency.setTopicEN(json.getString("topic_en"));
                        emergency.setTopicLAO(json.getString("topic_lao"));
                        emergency.setAddressTH(json.getString("address_th"));
                        emergency.setAddressEN(json.getString("address_en"));
                        emergency.setAddressLAO(json.getString("address_lao"));
                        emergency.setDetailTH(json.getString("details_th"));
                        emergency.setDetailEN(json.getString("details_en"));
                        emergency.setDetailLAO(json.getString("details_lao"));
                        emergency.setDateOpen(json.getString("dateOpen"));
                        emergency.setDateClose(json.getString("dateClose"));
                        emergency.setTel(json.getString("tel"));
                        emergency.setTimeStart(json.getString("timeStart"));
                        emergency.setTimeEnd(json.getString("timeEnd"));
                        emergency.setLatitude(json.getDouble("latitude"));
                        emergency.setLongitude(json.getDouble("longitude"));
                        paths.setPathImg(json.getString("path"));
                        emergency.setModelFilePaths(paths);
                        arrayList.add(emergency);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            return arrayList;
        }

    }

    private class DownloadInformation extends AsyncTask<String,Integer,String>{

        public DownloadInformation(){
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.i(TAG,"Call Service");
                return downloadContent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            arrayListsInformation = new ArrayList<>();
            arrayListsInformation = onParserContentToModel(s);
            myDB.addInformation(arrayListsInformation);
            alertDialog.dismiss();
            alertDialogDownLoadOffLineMode();
        }



        private String downloadContent(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                String result = convertInputStreamToString(is);
                return result;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String convertInputStreamToString(InputStream is) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine())!=null){
                sb.append(line + "\n");
            }
            br.close();
            return sb.toString();
        }

        public ArrayList<ModelTravelInformation> onParserContentToModel(String dataJSon) {
//            Log.e("data json", dataJSon);
            ArrayList<ModelTravelInformation> arrayList = new ArrayList<>();
            try {

                JSONArray jsonArray = new JSONArray(dataJSon);

                if (jsonArray != null){
                    for(int i=0;i < jsonArray.length();i++) {
//                        Log.e("Message", "loop");

                        ModelTravelInformation items = new ModelTravelInformation();
                        ModelFilePaths paths = new ModelFilePaths();
                        JSONObject json = jsonArray.optJSONObject(i);

                        items.setIdTravelInformation(json.getString("idTravelInformation"));
                        items.setTopic_th(json.getString("topic_th"));
                        items.setTopic_en(json.getString("topic_en"));
                        items.setTopic_lao(json.getString("topic_lao"));

                        items.setHistory_th(json.getString("history_th"));
                        items.setGovernment_th(json.getString("history_th"));
                        items.setLanguage_th(json.getString("language_th"));
                        items.setPopulation_th(json.getString("population_th"));
                        items.setReligion_th(json.getString("religion_th"));

                        items.setHistory_en(json.getString("history_en"));
                        items.setGovernment_en(json.getString("history_en"));
                        items.setLanguage_en(json.getString("language_en"));
                        items.setPopulation_en(json.getString("population_en"));
                        items.setReligion_en(json.getString("religion_en"));

                        items.setHistory_lao(json.getString("history_lao"));
                        items.setGovernment_lao(json.getString("history_lao"));
                        items.setLanguage_lao(json.getString("language_lao"));
                        items.setPopulation_lao(json.getString("population_lao"));
                        items.setReligion_lao(json.getString("religion_lao"));
                        items.setCountriesID(json.getString("idCountries"));
                        paths.setPathImg(json.getString("path"));
                        items.setModelFilePaths(paths);

                        arrayList.add(items);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            return arrayList;
        }

    }


}
