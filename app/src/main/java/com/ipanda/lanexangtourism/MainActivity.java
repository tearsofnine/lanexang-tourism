package com.ipanda.lanexangtourism;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.other.SetGoogleMapMin;
import com.google.android.gms.maps.MapView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private LinearLayout[] ClickNavigation_Array;
    private TextView[] TextView_Array;
    private Class[] fragmentClassArray;
    private Toolbar toolbar;
    private TextView toolbar_title;
    private Bundle _savedInstanceState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText("LANEXANG INFO");

        Configuration config = new Configuration();
        config.locale = new Locale(getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th"));

        getResources().updateConfiguration(config, null);

        try {
            Initializing(8,getIntent().getIntExtra("retrunMain2fragment",-1));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        SetNameMenu();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                closeKeyboard(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //closeKeyboard();

            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        String app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){

        }else {

        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("LANEXANG INFO");
            builder.setIcon(R.mipmap.ic_logo_app);
            builder.setMessage("Do you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if(getIntent().getIntExtra("retrunMain2fragment",-1) == -1)
                                finish();
                            else {
                                finishAffinity();
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public void Initializing(int numMenu,int retrunMenu) throws InstantiationException, IllegalAccessException {

        TextView_Array = new TextView[numMenu];
        ClickNavigation_Array = new LinearLayout[numMenu];
        fragmentClassArray = new Class[numMenu];
        for(int i=0; i<numMenu; i++) {

            String TextViewID = "TvCategory_" + (i+1) ;
            int TextViewResID = getResources().getIdentifier(TextViewID, "id", getPackageName());

            String LinearLayoutID = "menu_category_" + (i+1) ;
            int LinearLayoutResID = getResources().getIdentifier(LinearLayoutID, "id", getPackageName());

            TextView_Array[i]=(TextView)findViewById(TextViewResID);
            ClickNavigation_Array[i]=(LinearLayout)findViewById(LinearLayoutResID);


            try {
                fragmentClassArray[i] = Class.forName("com.ipanda.lanexangtourism.fragment_menu.Fragment_menu_category_"+ (i+1));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(retrunMenu == -1) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentContainer, (Fragment) fragmentClassArray[0].newInstance()).commit();
        }
        else {
            String[] NameMenuCategory;
            NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
            SetClickNavigation(retrunMenu+1);
            toolbar_title.setText(NameMenuCategory[retrunMenu]);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentContainer, (Fragment) fragmentClassArray[retrunMenu].newInstance()).commit();

        }

    }

    public void SetNameMenu(){
        String[] NameMenuCategory;
        Resources res = getResources();
        NameMenuCategory = res.getStringArray(R.array.NameCategory);
        for(int i=0;i<NameMenuCategory.length;i++) {
            TextView_Array[i].setText(NameMenuCategory[i]);
        }
    }

    private void SetClickNavigation(int select) {
        for(int i=0;i<ClickNavigation_Array.length;i++){
            if(select == (i+1))
                ClickNavigation_Array[i].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_clickb, null));
            else
                ClickNavigation_Array[i].setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_click, null));
        }
    }

    public void ClickNavigation(View view){

    Fragment fragment = null;
    Class fragmentClass =  fragmentClassArray[0];
    String[] NameMenuCategory;
    NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
        switch (view.getId()){
            case R.id.menu_category_1:
                fragmentClass = fragmentClassArray[0];
                SetClickNavigation(1);
                toolbar_title.setText(NameMenuCategory[0]);
                break;
            case R.id.menu_category_2:
                fragmentClass = fragmentClassArray[1];
                SetClickNavigation(2);
                toolbar_title.setText(NameMenuCategory[1]);
                break;
            case R.id.menu_category_3:
                fragmentClass = fragmentClassArray[2];
                SetClickNavigation(3);
                toolbar_title.setText(NameMenuCategory[2]);
                break;
            case R.id.menu_category_4:
                fragmentClass = fragmentClassArray[3];
                SetClickNavigation(4);
                toolbar_title.setText(NameMenuCategory[3]);
                break;
            case R.id.menu_category_5:
                fragmentClass = fragmentClassArray[4];
                SetClickNavigation(5);
                toolbar_title.setText(NameMenuCategory[4]);
                break;
            case R.id.menu_category_6:
                fragmentClass = fragmentClassArray[5];
                SetClickNavigation(6);
                toolbar_title.setText(NameMenuCategory[5]);
                break;
            case R.id.menu_category_7:
                fragmentClass = fragmentClassArray[6];
                SetClickNavigation(7);
                toolbar_title.setText(NameMenuCategory[6]);
                break;
            case R.id.menu_category_8:
                fragmentClass =fragmentClassArray[7];
                SetClickNavigation(8);
                toolbar_title.setText(NameMenuCategory[7]);
                break;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    /*public void showKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }*/

    public void closeKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    MapView mapView = (MapView) findViewById(R.id.mapView);
                    SetGoogleMapMin setGoogleMapMin = new SetGoogleMapMin(mapView, _savedInstanceState, this, false, -1, 0, 0);
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case 2: {
                if (grantResults.length > 0 && grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case 3: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    Class fragmentClass =null;
                    try {
                        fragmentClass = Class.forName("com.ipanda.lanexangtourism.fragment_menu.Fragment_menu_category_8");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    Fragment fragment = null;
                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentContainer, fragment).commit();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }

            }
            break;

        }
    }

    public void switchContent(int id, Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(id, fragment, fragment.toString());
        ft.addToBackStack(null);
        ft.commit();
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("   "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
