package com.ipanda.lanexangtourism;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ipanda.lanexangtourism.fragment_contract.Fragment_contract_list_1;
import com.ipanda.lanexangtourism.fragment_contract.Fragment_contract_list_2;

public class ContactListActivity extends AppCompatActivity {

//    private Class[] fragmentClassArray;
    private Fragment_contract_list_1 fragmentClass1;
    private Fragment_contract_list_2 fragmentClass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar_contract);
        TextView toolbar_title = findViewById(R.id.toolbar_title_contract);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar_title.setText(RetrunTitle(getIntent().getStringExtra("select"))[0]);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString("select", getIntent().getStringExtra("select"));


        if(getIntent().getStringExtra("select").equals("travel")) {
            fragmentClass1 = new Fragment_contract_list_1();
            fragmentClass1.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.content_contract, fragmentClass1).commit();
        }
        else {
            fragmentClass2 = new Fragment_contract_list_2();
            fragmentClass2.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.content_contract, fragmentClass2).commit();
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String[] RetrunTitle(String select){

        String[] string = new String[2];
        switch (select){
            case "travel":
                string[0]=getString(R.string.tVtravel);
                string[1] ="0";
                break;
            case "emergency":
                string[0]=getString(R.string.tVemergency);
                string[1] ="1";
                break;
            case "hospital":
                string[0]=getString(R.string.tVhospital);
                string[1] ="2";
            break;
            case "inforcenter":
                string[0]=getString(R.string.tVtourist_infor_center);
                string[1] ="3";
                break;
        }
        return string;
    }
}
