package com.ipanda.lanexangtourism;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ipanda.lanexangtourism.fragment_information.Fragment_information_province;

public class InformationProvinceActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_province);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String[] NameMenuCategory;
        NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
        toolbar_title.setText(NameMenuCategory[6]);



//        fragment_information_province_bg

        FragmentManager fragmentManager = getSupportFragmentManager();

        Class fragmentClass = Fragment_information_province.class;
        try {
            Bundle bundle = new Bundle();
            bundle.putString("selectCountry", getIntent().getStringExtra("selectCountry"));
            bundle.putInt("selectPosition",getIntent().getExtras().getInt("selectPosition"));
            Fragment fragment_information_list_province = (Fragment) fragmentClass.newInstance();
            fragment_information_list_province.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.content_information_province,fragment_information_list_province).commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
