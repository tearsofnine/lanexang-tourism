package com.ipanda.lanexangtourism;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskDescription;
import com.ipanda.lanexangtourism.adapter.AdapterDescription;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceDescription;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTrip;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TripDescriptionActivity extends AppCompatActivity implements CallBackServiceDescription, AdapterView.OnItemClickListener {

    private String url_json = "/lanexang_tourism/user/GetData/GetProgramTour";
    private ArrayList<ModelItem> modelItems = null;
    private AdapterDescription adapter;
    private ProgressDialog alertDialog = null;
    private ModelTrip trip;
    private DatabaseHelper myDB;

    private int json_index = 0;
    private int json_KeyValue = 0;

    private ViewGroup header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_description);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myDB = new DatabaseHelper(this);

        json_index = getIntent().getIntExtra("JSON_POSITION",0);
        trip = new ModelTrip();
        trip =  this.getIntent().getParcelableExtra("MODEL_TRIP");
        json_KeyValue = trip.getKey();

        switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                toolbar_title.setText(trip.getTripTH());
                break;
            case "en":
                toolbar_title.setText(trip.getTripEN());
                break;
            case "lo":
                toolbar_title.setText(trip.getTripLAO());
                break;
        }

        String UserId = this.getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
        String app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            Toast.makeText(this, "Offline", Toast.LENGTH_SHORT).show();
            modelItems = myDB.getModelItem(String.valueOf(trip.getTripID()));
            onSetAdapter();
        }else {
            new AsyncTaskDescription(this,json_index,json_KeyValue).execute(getResources().getString(R.string.host_ip)+url_json+"?UserId="+UserId);
        }




    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelItem> modelItems) {
        if (modelItems != null && modelItems.size() > 0){
            Log.i("Check data", "" + modelItems.size());
            this.modelItems = modelItems;
            onSetAdapter();
            adapter.notifyDataSetChanged();
        }
        this.dismissProgressDialog();
    }

    private void onSetAdapter() {
        ListView listView =  findViewById(R.id.list_item_description);

        if(header == null) {
            LayoutInflater layoutinflater = getLayoutInflater();
            header = (ViewGroup) layoutinflater.inflate(R.layout.header_description, listView, false);
            TextView text_item_title = (TextView) header.findViewById(R.id.text_item_title);
            TextView text_item_form = header.findViewById(R.id.text_item_form);
            TextView type_trip = header.findViewById(R.id.type_trip);

            switch (getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                case "th":
                    type_trip.setText(trip.getModelTypeOfTrip().getTypeTH());
                    text_item_title.setText(trip.getTripTH());
                    text_item_form.setText(trip.getAreaTH());
                    break;
                case "en":
                    if (trip.getTripEN().equals("") || trip.getTripEN() == null || trip.getTripEN().equals("null")) {
                        text_item_title.setText(trip.getTripTH());

                    } else {
                        text_item_title.setText(trip.getTripEN());
                    }
                    type_trip.setText(trip.getModelTypeOfTrip().getTypeEN());
                    text_item_form.setText(trip.getAreaEN());
                    break;
                case "lo":
                    if (trip.getTripLAO().equals("") || trip.getTripLAO() == null || trip.getTripLAO().equals("null")) {
                        text_item_title.setText(trip.getTripTH());

                    } else {
                        text_item_title.setText(trip.getTripLAO());
                    }
                    type_trip.setText(trip.getModelTypeOfTrip().getTypeLAO());
                    text_item_form.setText(trip.getAreaLAO());
                    break;
            }


            RelativeLayout layout = header.findViewById(R.id.RelativeLayout_btn_shape_trip_description);
            if (trip.getModelTypeOfTrip().getTypeEN().equals("1-Day Trip")) {
                layout.setBackground(getDrawable(R.drawable.button_shape_trip_blue));
            } else {
                layout.setBackground(getDrawable(R.drawable.button_shape_trip_skyblue));
            }

            ImageView imgTrip = (ImageView) header.findViewById(R.id.img_item_trip);
            Picasso.get().load(trip.getPathImg()).into(imgTrip);
            listView.addHeaderView(header);
        }


        adapter = new AdapterDescription(this, modelItems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage(""+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, ItemsDetailActivity.class);

        if (position != 0) {
            intent.putExtra("ITEMS_DETAIL", modelItems.get((position - 1)));
            intent.putExtra("typeItem", "Item");
            startActivityForResult(intent,1);
        }else {

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                new AsyncTaskDescription(this,json_index,json_KeyValue).execute(getResources().getString(R.string.host_ip)+url_json+"?UserId="+UserId);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
