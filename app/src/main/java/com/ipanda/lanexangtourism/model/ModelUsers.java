package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelUsers implements Parcelable {

    private String idUsers;
    private String token ;
    private String email ;
    private String name;
    private String loginWith;
    private String personPhoto ;

    public ModelUsers() {

    }

    public String getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(String idUsers) {
        this.idUsers = idUsers;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginWith() {
        return loginWith;
    }

    public void setLoginWith(String loginWith) {
        this.loginWith = loginWith;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    protected ModelUsers(Parcel in) {
        idUsers = in.readString();
        token = in.readString();
        email = in.readString();
        name = in.readString();
        loginWith = in.readString();
        personPhoto = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idUsers);
        dest.writeString(token);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(loginWith);
        dest.writeString(personPhoto);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelUsers> CREATOR = new Creator<ModelUsers>() {
        @Override
        public ModelUsers createFromParcel(Parcel in) {
            return new ModelUsers(in);
        }

        @Override
        public ModelUsers[] newArray(int size) {
            return new ModelUsers[size];
        }
    };
}
