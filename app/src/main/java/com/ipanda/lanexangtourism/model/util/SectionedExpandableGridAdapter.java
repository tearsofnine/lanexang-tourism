package com.ipanda.lanexangtourism.model.util;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SectionedExpandableGridAdapter extends RecyclerView.Adapter<SectionedExpandableGridAdapter.ViewHolder> {

    //data array
    private ArrayList<Object> mDataArrayList;

    //context
    private final Context mContext;

    //listeners
    private final ItemClickListener mItemClickListener;
    private final SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_SECTION = R.layout.title_list_filter;
    private static final int VIEW_TYPE_ITEM = R.layout.list_filter;

    public SectionedExpandableGridAdapter(Context context, ArrayList<Object> dataArrayList,
                                          final GridLayoutManager gridLayoutManager, ItemClickListener itemClickListener,
                                          SectionStateChangeListener sectionStateChangeListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
        mSectionStateChangeListener = sectionStateChangeListener;
        mDataArrayList = dataArrayList;

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isSection(position)?gridLayoutManager.getSpanCount():1;
            }
        });
    }

    private boolean isSection(int position) {
        return mDataArrayList.get(position) instanceof ModelTypeOfItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(viewType, parent, false), viewType);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        switch (holder.viewType) {
            case VIEW_TYPE_ITEM :
                final ModelItem item = (ModelItem) mDataArrayList.get(position);
//                holder.img_menu_categoryID.setImageResource(item.getItemID());
                holder.title_item.setText(item.getTopicTH());
                holder.topic_item.setText(item.getAddressTH());
                holder.distance.setText(item.getDistance());

                Picasso.get().load(item.getModelFilePaths().getPathImg()).into(holder.img_item_list_great_places);
                holder.img_favorite.setImageResource(R.drawable.ic_favorite);
                holder.img_favorite.setColorFilter(mContext.getResources().getColor(R.color.red));
                holder.img_position.setImageResource(R.mipmap.ic_position);
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(item);
                    }
                });
                break;
            case VIEW_TYPE_SECTION :
                final ModelTypeOfItem type = (ModelTypeOfItem) mDataArrayList.get(position);
                holder.img_menu_categoryID.setImageResource(type.getTypeID());

                switch (mContext.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        holder.text_title.setText(type.getTypeTH());
                        break;
                    case "en":
                        if (type.getTypeEN().equals("") || type.getTypeEN() == null || type.getTypeEN().equals("null")) {
                            holder.text_title.setText(type.getTypeTH());
                        } else {
                            holder.text_title.setText(type.getTypeEN());
                        }
                        break;
                    case "lo":
                        if (type.getTypeLAO().equals("") || type.getTypeLAO() == null || type.getTypeEN().equals("null")) {
                            holder.text_title.setText(type.getTypeTH());
                        } else {
                            holder.text_title.setText(type.getTypeLAO());
                        }
                        break;
                }

                holder.text_result.setText(type.getCount());
                holder.text_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(type);
                    }
                });
                holder.sectionToggleButton.setChecked(type.isExpanded);
                holder.sectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateChanged(type, isChecked);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSection(position))
            return VIEW_TYPE_SECTION;
        else return VIEW_TYPE_ITEM;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        int viewType;
        ToggleButton sectionToggleButton;
        TextView text_title,text_result,title_item,topic_item,distance;
        ImageView img_menu_categoryID,img_item_list_great_places,img_favorite,img_position;

        public ViewHolder(View view, int viewType) {
            super(view);
            this.viewType = viewType;
            this.view = view;
            if (viewType == VIEW_TYPE_ITEM) {
                title_item =  view.findViewById(R.id.title_item);
                topic_item =  view.findViewById(R.id.topic_item);
                distance =  view.findViewById(R.id.distance);
                img_item_list_great_places =  view.findViewById(R.id.img_item_list_great_places);
                img_favorite =  view.findViewById(R.id.img_favorite);
                img_position =  view.findViewById(R.id.img_position);


            } else {
                img_menu_categoryID = view.findViewById(R.id.img_menu_categoryID);
                sectionToggleButton =  view.findViewById(R.id.toggle_button_section);
                text_title =  view.findViewById(R.id.text_title);
                text_result =  view.findViewById(R.id.text_result);

            }
        }

    }


}
