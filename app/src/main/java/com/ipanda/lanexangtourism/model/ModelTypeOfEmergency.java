package com.ipanda.lanexangtourism.model;

import java.util.ArrayList;

public class ModelTypeOfEmergency {

    private int typeOfEmergencyID;
    private String typeTH;
    private String typeEN;
    private String typeLAO;
    private ArrayList<ModelEmergency> modelEmergencies;

    public ModelTypeOfEmergency() {

    }

    public ModelTypeOfEmergency(int typeOfEmergencyID, String typeTH, String typeEN, String typeLAO, ArrayList<ModelEmergency> modelEmergencies) {
        this.typeOfEmergencyID = typeOfEmergencyID;
        this.typeTH = typeTH;
        this.typeEN = typeEN;
        this.typeLAO = typeLAO;
        this.modelEmergencies = new ArrayList<ModelEmergency>();
    }

    public int getTypeOfEmergencyID() {
        return typeOfEmergencyID;
    }

    public void setTypeOfEmergencyID(int typeOfEmergencyID) {
        this.typeOfEmergencyID = typeOfEmergencyID;
    }

    public String getTypeTH() {
        return typeTH;
    }

    public void setTypeTH(String typeTH) {
        this.typeTH = typeTH;
    }

    public String getTypeEN() {
        return typeEN;
    }

    public void setTypeEN(String typeEN) {
        this.typeEN = typeEN;
    }

    public String getTypeLAO() {
        return typeLAO;
    }

    public void setTypeLAO(String typeLAO) {
        this.typeLAO = typeLAO;
    }

    public ArrayList<ModelEmergency> getModelEmergencies() {
        return modelEmergencies;
    }

    public void setModelEmergencies(ArrayList<ModelEmergency> modelEmergencies) {
        this.modelEmergencies = modelEmergencies;
    }
}
