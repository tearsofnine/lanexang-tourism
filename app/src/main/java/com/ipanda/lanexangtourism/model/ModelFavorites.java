package com.ipanda.lanexangtourism.model;

import java.util.ArrayList;

public class ModelFavorites {

    private int favoritesID;
    private ModelUsers modelUsers = new ModelUsers();
    private ArrayList<ModelItem> modelItems;

    public ModelFavorites() {

    }

    public ModelFavorites(int favoritesID, ModelUsers modelUsers, ArrayList<ModelItem> modelItems) {
        this.favoritesID = favoritesID;
        this.modelUsers = modelUsers;
        this.modelItems = modelItems;
    }

    public int getFavoritesID() {
        return favoritesID;
    }

    public void setFavoritesID(int favoritesID) {
        this.favoritesID = favoritesID;
    }

    public ModelUsers getModelUsers() {
        return modelUsers;
    }

    public void setModelUsers(ModelUsers modelUsers) {
        this.modelUsers = modelUsers;
    }

    public ArrayList<ModelItem> getModelItems() {
        return modelItems;
    }

    public void setModelItems(ArrayList<ModelItem> modelItems) {
        this.modelItems = modelItems;
    }
}
