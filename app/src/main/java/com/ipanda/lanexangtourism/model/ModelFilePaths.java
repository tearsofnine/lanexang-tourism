package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelFilePaths implements Parcelable {
    private String id;
    private String pathImg;

    public ModelFilePaths() {
    }

    public ModelFilePaths(String pathImg) {
        this.pathImg = pathImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public static Creator<ModelFilePaths> getCREATOR() {
        return CREATOR;
    }

    protected ModelFilePaths(Parcel in) {
        id = in.readString();
        pathImg = in.readString();
    }

    public static final Creator<ModelFilePaths> CREATOR = new Creator<ModelFilePaths>() {
        @Override
        public ModelFilePaths createFromParcel(Parcel in) {
            return new ModelFilePaths(in);
        }

        @Override
        public ModelFilePaths[] newArray(int size) {
            return new ModelFilePaths[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(pathImg);
    }
}
