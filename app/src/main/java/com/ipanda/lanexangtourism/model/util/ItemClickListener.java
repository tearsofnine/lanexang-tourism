package com.ipanda.lanexangtourism.model.util;

import android.widget.ImageView;

import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;

public interface ItemClickListener {
    void itemClicked(ModelTypeOfItem type);
    void itemClicked(ModelItem item);
    void itemClickedFavorite(ModelItem item, ImageView image,int position);
    void itemClickedUnFavorite(ModelItem item, ImageView image,int position);
    void itemClickedUserMatching(ModelItem item);

}
