package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelTypeOfTravel implements Parcelable {
    private int idTypeOfTravel;
    private String typeTH;
    private String typeEN;
    private String typeLAO;
    private ArrayList<ModelItem> modelEmergency;

    public ModelTypeOfTravel() {

    }

    public int getIdTypeOfTravel() {
        return idTypeOfTravel;
    }

    public void setIdTypeOfTravel(int idTypeOfTravel) {
        this.idTypeOfTravel = idTypeOfTravel;
    }

    public String getTypeTH() {
        return typeTH;
    }

    public void setTypeTH(String typeTH) {
        this.typeTH = typeTH;
    }

    public String getTypeEN() {
        return typeEN;
    }

    public void setTypeEN(String typeEN) {
        this.typeEN = typeEN;
    }

    public String getTypeLAO() {
        return typeLAO;
    }

    public void setTypeLAO(String typeLAO) {
        this.typeLAO = typeLAO;
    }

    public ArrayList<ModelItem> getModelEmergency() {
        return modelEmergency;
    }

    public void setModelEmergency(ArrayList<ModelItem> modelEmergency) {
        this.modelEmergency = modelEmergency;
    }

    protected ModelTypeOfTravel(Parcel in) {
        idTypeOfTravel = in.readInt();
        typeTH = in.readString();
        typeEN = in.readString();
        typeLAO = in.readString();
        modelEmergency = in.createTypedArrayList(ModelItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idTypeOfTravel);
        dest.writeString(typeTH);
        dest.writeString(typeEN);
        dest.writeString(typeLAO);
        dest.writeTypedList(modelEmergency);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTypeOfTravel> CREATOR = new Creator<ModelTypeOfTravel>() {
        @Override
        public ModelTypeOfTravel createFromParcel(Parcel in) {
            return new ModelTypeOfTravel(in);
        }

        @Override
        public ModelTypeOfTravel[] newArray(int size) {
            return new ModelTypeOfTravel[size];
        }
    };
}
