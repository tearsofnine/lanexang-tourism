package com.ipanda.lanexangtourism.model.util;

import android.widget.ImageView;

import com.ipanda.lanexangtourism.model.ModelItem;

public interface ItemClickFavorite {
    void itemClicked(ModelItem item, ImageView imageView);

}
