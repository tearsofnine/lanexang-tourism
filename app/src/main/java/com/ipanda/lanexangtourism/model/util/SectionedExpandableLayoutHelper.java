package com.ipanda.lanexangtourism.model.util;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SectionedExpandableLayoutHelper implements SectionStateChangeListener{

    //data list
    private LinkedHashMap<ModelTypeOfItem, ArrayList<ModelItem>> mSectionDataMap = new LinkedHashMap<ModelTypeOfItem, ArrayList<ModelItem>>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();

    //section map
    private HashMap<String, ModelTypeOfItem> mSectionMap = new HashMap<String, ModelTypeOfItem>();

    //adapter
    private SectionedExpandableGridAdapter mSectionedExpandableGridAdapter;

    //recycler view
    RecyclerView mRecyclerView;

    public SectionedExpandableLayoutHelper(Context context, RecyclerView recyclerView, ItemClickListener itemClickListener,
                                           int gridSpanCount) {

        //setting the recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, gridSpanCount);
        recyclerView.setLayoutManager(gridLayoutManager);
        mSectionedExpandableGridAdapter = new SectionedExpandableGridAdapter(context, mDataArrayList,
                gridLayoutManager, itemClickListener, this);
        recyclerView.setAdapter(mSectionedExpandableGridAdapter);

        mRecyclerView = recyclerView;
    }

    public void notifyDataSetChanged() {
        generateDataList();
        mSectionedExpandableGridAdapter.notifyDataSetChanged();
    }

    public void addSection(int typeID, String typeTH, String typeEN, String typeLAO,String count, ArrayList<ModelItem> items) {
        ModelTypeOfItem newType;
        mSectionMap.put(typeTH, (newType = new ModelTypeOfItem(typeID,  typeTH,  typeEN, typeLAO,count)));
        mSectionDataMap.put(newType, items);
    }

    public void addItem(String type, ModelItem item) {
        mSectionDataMap.get(mSectionMap.get(type)).add(item);
    }

    public void removeItem(String type, ModelItem item) {
        mSectionDataMap.get(mSectionMap.get(type)).remove(item);
    }

    public void removeSection(String type) {
        mSectionDataMap.remove(mSectionMap.get(type));
        mSectionMap.remove(type);
    }

    private void generateDataList () {
        mDataArrayList.clear();
        for (Map.Entry<ModelTypeOfItem, ArrayList<ModelItem>> entry : mSectionDataMap.entrySet()) {
            ModelTypeOfItem key;
            mDataArrayList.add((key = entry.getKey()));
            if (key.isExpanded)
                mDataArrayList.addAll(entry.getValue());
        }
    }




    @Override
    public void onSectionStateChanged(ModelTypeOfItem type, boolean isOpen) {
        if (!mRecyclerView.isComputingLayout()) {
            type.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }
}
