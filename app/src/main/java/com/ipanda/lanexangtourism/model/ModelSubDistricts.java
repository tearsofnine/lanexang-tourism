package com.ipanda.lanexangtourism.model;

public class ModelSubDistricts {

    private String id;
    private String code;
    private String zipCode;
    private String nameTH;
    private String nameEN;
    private String nameLAO;

    public ModelSubDistricts() {

    }

    public ModelSubDistricts(String id, String code, String zipCode, String nameTH, String nameEN, String nameLAO) {
        this.id = id;
        this.code = code;
        this.zipCode = zipCode;
        this.nameTH = nameTH;
        this.nameEN = nameEN;
        this.nameLAO = nameLAO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameLAO() {
        return nameLAO;
    }

    public void setNameLAO(String nameLAO) {
        this.nameLAO = nameLAO;
    }
}
