package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelEmergency implements Parcelable {

    private String emergencyID;
    private String idTypeOfEmergency;
    private String topicTH;
    private String topicEN;
    private String topicLAO;
    private String tel;
    private String addressTH;
    private String addressEN;
    private String addressLAO;
    private String dateOpen;
    private String dateClose;
    private String timeStart;
    private String timeEnd;
    private String detailTH;
    private String detailEN;
    private String detailLAO;
    private double latitude;
    private double longitude;
    private String arLink;
    private ModelFilePaths modelFilePaths = new ModelFilePaths();

    public ModelEmergency() {

    }

    public String getEmergencyID() {
        return emergencyID;
    }

    public void setEmergencyID(String emergencyID) {
        this.emergencyID = emergencyID;
    }

    public String getIdTypeOfEmergency() {
        return idTypeOfEmergency;
    }

    public void setIdTypeOfEmergency(String idTypeOfEmergency) {
        this.idTypeOfEmergency = idTypeOfEmergency;
    }

    public String getTopicTH() {
        return topicTH;
    }

    public void setTopicTH(String topicTH) {
        this.topicTH = topicTH;
    }

    public String getTopicEN() {
        return topicEN;
    }

    public void setTopicEN(String topicEN) {
        this.topicEN = topicEN;
    }

    public String getTopicLAO() {
        return topicLAO;
    }

    public void setTopicLAO(String topicLAO) {
        this.topicLAO = topicLAO;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddressTH() {
        return addressTH;
    }

    public void setAddressTH(String addressTH) {
        this.addressTH = addressTH;
    }

    public String getAddressEN() {
        return addressEN;
    }

    public void setAddressEN(String addressEN) {
        this.addressEN = addressEN;
    }

    public String getAddressLAO() {
        return addressLAO;
    }

    public void setAddressLAO(String addressLAO) {
        this.addressLAO = addressLAO;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getDetailTH() {
        return detailTH;
    }

    public void setDetailTH(String detailTH) {
        this.detailTH = detailTH;
    }

    public String getDetailEN() {
        return detailEN;
    }

    public void setDetailEN(String detailEN) {
        this.detailEN = detailEN;
    }

    public String getDetailLAO() {
        return detailLAO;
    }

    public void setDetailLAO(String detailLAO) {
        this.detailLAO = detailLAO;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getArLink() {
        return arLink;
    }

    public void setArLink(String arLink) {
        this.arLink = arLink;
    }

    public ModelFilePaths getModelFilePaths() {
        return modelFilePaths;
    }

    public void setModelFilePaths(ModelFilePaths modelFilePaths) {
        this.modelFilePaths = modelFilePaths;
    }

    protected ModelEmergency(Parcel in) {
        emergencyID = in.readString();
        idTypeOfEmergency = in.readString();
        topicTH = in.readString();
        topicEN = in.readString();
        topicLAO = in.readString();
        tel = in.readString();
        addressTH = in.readString();
        addressEN = in.readString();
        addressLAO = in.readString();
        dateOpen = in.readString();
        dateClose = in.readString();
        timeStart = in.readString();
        timeEnd = in.readString();
        detailTH = in.readString();
        detailEN = in.readString();
        detailLAO = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        arLink = in.readString();
        modelFilePaths = in.readParcelable(ModelFilePaths.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emergencyID);
        dest.writeString(idTypeOfEmergency);
        dest.writeString(topicTH);
        dest.writeString(topicEN);
        dest.writeString(topicLAO);
        dest.writeString(tel);
        dest.writeString(addressTH);
        dest.writeString(addressEN);
        dest.writeString(addressLAO);
        dest.writeString(dateOpen);
        dest.writeString(dateClose);
        dest.writeString(timeStart);
        dest.writeString(timeEnd);
        dest.writeString(detailTH);
        dest.writeString(detailEN);
        dest.writeString(detailLAO);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(arLink);
        dest.writeParcelable(modelFilePaths, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelEmergency> CREATOR = new Creator<ModelEmergency>() {
        @Override
        public ModelEmergency createFromParcel(Parcel in) {
            return new ModelEmergency(in);
        }

        @Override
        public ModelEmergency[] newArray(int size) {
            return new ModelEmergency[size];
        }
    };
}
