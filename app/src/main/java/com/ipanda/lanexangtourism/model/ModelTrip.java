package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelTrip implements Parcelable {

    private int tripID;
    private String tripTH;
    private String tripEN;
    private String tripLAO;
    private String pathImg;
    private String areaTH;
    private String areaEN;
    private String areaLAO;
    private int key;
    private ModelTypeOfItem modelTypeOfItem;
    private ModelTypeOfTrip modelTypeOfTrip;
    private ArrayList<ModelItem> modelItems;
    private ArrayList<ModelFilePaths> modelFilePaths;

    public ModelTrip() {
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getTripTH() {
        return tripTH;
    }

    public void setTripTH(String tripTH) {
        this.tripTH = tripTH;
    }

    public String getTripEN() {
        return tripEN;
    }

    public void setTripEN(String tripEN) {
        this.tripEN = tripEN;
    }

    public String getTripLAO() {
        return tripLAO;
    }

    public void setTripLAO(String tripLAO) {
        this.tripLAO = tripLAO;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public String getAreaTH() {
        return areaTH;
    }

    public void setAreaTH(String areaTH) {
        this.areaTH = areaTH;
    }

    public String getAreaEN() {
        return areaEN;
    }

    public void setAreaEN(String areaEN) {
        this.areaEN = areaEN;
    }

    public String getAreaLAO() {
        return areaLAO;
    }

    public void setAreaLAO(String areaLAO) {
        this.areaLAO = areaLAO;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public ModelTypeOfItem getModelTypeOfItem() {
        return modelTypeOfItem;
    }

    public void setModelTypeOfItem(ModelTypeOfItem modelTypeOfItem) {
        this.modelTypeOfItem = modelTypeOfItem;
    }

    public ModelTypeOfTrip getModelTypeOfTrip() {
        return modelTypeOfTrip;
    }

    public void setModelTypeOfTrip(ModelTypeOfTrip modelTypeOfTrip) {
        this.modelTypeOfTrip = modelTypeOfTrip;
    }

    public ArrayList<ModelItem> getModelItems() {
        return modelItems;
    }

    public void setModelItems(ArrayList<ModelItem> modelItems) {
        this.modelItems = modelItems;
    }

    public ArrayList<ModelFilePaths> getModelFilePaths() {
        return modelFilePaths;
    }

    public void setModelFilePaths(ArrayList<ModelFilePaths> modelFilePaths) {
        this.modelFilePaths = modelFilePaths;
    }

    protected ModelTrip(Parcel in) {
        tripID = in.readInt();
        tripTH = in.readString();
        tripEN = in.readString();
        tripLAO = in.readString();
        pathImg = in.readString();
        areaTH = in.readString();
        areaEN = in.readString();
        areaLAO = in.readString();
        key = in.readInt();
        modelTypeOfItem = in.readParcelable(ModelTypeOfItem.class.getClassLoader());
        modelTypeOfTrip = in.readParcelable(ModelTypeOfTrip.class.getClassLoader());
        modelItems = in.createTypedArrayList(ModelItem.CREATOR);
        modelFilePaths = in.createTypedArrayList(ModelFilePaths.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tripID);
        dest.writeString(tripTH);
        dest.writeString(tripEN);
        dest.writeString(tripLAO);
        dest.writeString(pathImg);
        dest.writeString(areaTH);
        dest.writeString(areaEN);
        dest.writeString(areaLAO);
        dest.writeInt(key);
        dest.writeParcelable(modelTypeOfItem, flags);
        dest.writeParcelable(modelTypeOfTrip, flags);
        dest.writeTypedList(modelItems);
        dest.writeTypedList(modelFilePaths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTrip> CREATOR = new Creator<ModelTrip>() {
        @Override
        public ModelTrip createFromParcel(Parcel in) {
            return new ModelTrip(in);
        }

        @Override
        public ModelTrip[] newArray(int size) {
            return new ModelTrip[size];
        }
    };
}