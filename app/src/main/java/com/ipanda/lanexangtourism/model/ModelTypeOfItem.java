package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelTypeOfItem implements Parcelable {

    private int typeID;
    private String typeTH;
    private String typeEN;
    private String typeLAO;
    private String count;
    public boolean isExpanded = false;
    private ArrayList<ModelItem> modelItems;

    public ModelTypeOfItem() {

    }

    public ModelTypeOfItem(int typeID, String typeTH, String typeEN, String typeLAO,String count) {
        this.typeID = typeID;
        this.typeTH = typeTH;
        this.typeEN = typeEN;
        this.typeLAO = typeLAO;
        this.count = count;
        isExpanded = true;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getTypeTH() {
        return typeTH;
    }

    public void setTypeTH(String typeTH) {
        this.typeTH = typeTH;
    }

    public String getTypeEN() {
        return typeEN;
    }

    public void setTypeEN(String typeEN) {
        this.typeEN = typeEN;
    }

    public String getTypeLAO() {
        return typeLAO;
    }

    public void setTypeLAO(String typeLAO) {
        this.typeLAO = typeLAO;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public ArrayList<ModelItem> getModelItems() {
        return modelItems;
    }

    public void setModelItems(ArrayList<ModelItem> modelItems) {
        this.modelItems = modelItems;
    }

    protected ModelTypeOfItem(Parcel in) {
        typeID = in.readInt();
        typeTH = in.readString();
        typeEN = in.readString();
        typeLAO = in.readString();
        count = in.readString();
        isExpanded = in.readByte() != 0;
        modelItems = in.createTypedArrayList(ModelItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeID);
        dest.writeString(typeTH);
        dest.writeString(typeEN);
        dest.writeString(typeLAO);
        dest.writeString(count);
        dest.writeByte((byte) (isExpanded ? 1 : 0));
        dest.writeTypedList(modelItems);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTypeOfItem> CREATOR = new Creator<ModelTypeOfItem>() {
        @Override
        public ModelTypeOfItem createFromParcel(Parcel in) {
            return new ModelTypeOfItem(in);
        }

        @Override
        public ModelTypeOfItem[] newArray(int size) {
            return new ModelTypeOfItem[size];
        }
    };
}
