package com.ipanda.lanexangtourism.model;

public class ModelTypeOfUsers {

    private int typeOfUserID;
    private String type;

    public ModelTypeOfUsers() {

    }

    public ModelTypeOfUsers(int typeOfUserID, String type) {
        this.typeOfUserID = typeOfUserID;
        this.type = type;
    }

    public int getTypeOfUserID() {
        return typeOfUserID;
    }

    public void setTypeOfUserID(int typeOfUserID) {
        this.typeOfUserID = typeOfUserID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
