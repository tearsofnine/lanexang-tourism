package com.ipanda.lanexangtourism.model.util;

import android.os.Parcel;
import android.os.Parcelable;

public class Version implements Parcelable {
    private int id;
    private int upGrade;

    public Version() {
    }

    public Version(int id, int upGrade) {
        this.id = id;
        this.upGrade = upGrade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUpGrade() {
        return upGrade;
    }

    public void setUpGrade(int upGrade) {
        this.upGrade = upGrade;
    }

    protected Version(Parcel in) {
        id = in.readInt();
        upGrade = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(upGrade);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Version> CREATOR = new Creator<Version>() {
        @Override
        public Version createFromParcel(Parcel in) {
            return new Version(in);
        }

        @Override
        public Version[] newArray(int size) {
            return new Version[size];
        }
    };
}
