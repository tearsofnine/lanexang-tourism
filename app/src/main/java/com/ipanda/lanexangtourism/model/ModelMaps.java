package com.ipanda.lanexangtourism.model;

public class ModelMaps {

    private int mapsID;
    private String placeTH;
    private float latitude;
    private float longitude;

    public ModelMaps() {
    }

    public ModelMaps(int mapsID, String placeTH, float latitude, float longitude) {
        this.mapsID = mapsID;
        this.placeTH = placeTH;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getMapsID() {
        return mapsID;
    }

    public void setMapsID(int mapsID) {
        this.mapsID = mapsID;
    }

    public String getPlaceTH() {
        return placeTH;
    }

    public void setPlaceTH(String placeTH) {
        this.placeTH = placeTH;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
