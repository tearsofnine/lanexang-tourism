package com.ipanda.lanexangtourism.model.util;

import com.ipanda.lanexangtourism.model.ModelTypeOfItem;

public interface SectionStateChangeListener {
    void onSectionStateChanged(ModelTypeOfItem type, boolean isOpen);
}
