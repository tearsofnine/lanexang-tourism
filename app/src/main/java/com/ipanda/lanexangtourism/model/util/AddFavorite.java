package com.ipanda.lanexangtourism.model.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.ipanda.lanexangtourism.GoogleMapActivity;
import com.ipanda.lanexangtourism.ItemsDetailActivity;
import com.ipanda.lanexangtourism.SearchActivity;
import com.ipanda.lanexangtourism.fragment_menu.Fragment_menu_category_8;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.support.constraint.Constraints.TAG;

public class AddFavorite extends AsyncTask<String,Integer,String> {

    public AddFavorite(ItemsDetailActivity context){

    }

    public AddFavorite(SearchActivity context){

    }

    public AddFavorite(GoogleMapActivity context){

    }

    public AddFavorite(Context context) {
    }


    private String addFavorite(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            return null;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            return addFavorite(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
