package com.ipanda.lanexangtourism.model;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public class ModelProvinces {

    private String id;
    private String code;
    private String nameTH;
    private String nameEN;
    private String nameLAO;
    private String backGroundTH;
    private String backGroundEN;
    private String backGroundLAO;
    private ArrayList<ModelDistrict> modelDistricts;
    private ArrayList<ModelFilePaths> modelFilePaths;

    public ModelProvinces(String id, String code, String nameTH, String nameEN, String nameLAO, String backGroundTH, String backGroundEN, String backGroundLAO, ArrayList<ModelDistrict> modelDistricts,ArrayList<ModelFilePaths> modelFilePaths) {
        this.id = id;
        this.code = code;
        this.nameTH = nameTH;
        this.nameEN = nameEN;
        this.nameLAO = nameLAO;
        this.backGroundTH = backGroundTH;
        this.backGroundEN = backGroundEN;
        this.backGroundLAO = backGroundLAO;
        this.modelDistricts = new ArrayList<ModelDistrict>();
        this.modelFilePaths = new ArrayList<ModelFilePaths>();
    }

    public ArrayList<ModelFilePaths> getModelFilePaths() {
        return modelFilePaths;
    }

    public void setModelFilePaths(ArrayList<ModelFilePaths> modelFilePaths) {
        this.modelFilePaths = modelFilePaths;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameLAO() {
        return nameLAO;
    }

    public void setNameLAO(String nameLAO) {
        this.nameLAO = nameLAO;
    }

    public String getBackGroundTH() {
        return backGroundTH;
    }

    public void setBackGroundTH(String backGroundTH) {
        this.backGroundTH = backGroundTH;
    }

    public String getBackGroundEN() {
        return backGroundEN;
    }

    public void setBackGroundEN(String backGroundEN) {
        this.backGroundEN = backGroundEN;
    }

    public String getBackGroundLAO() {
        return backGroundLAO;
    }

    public void setBackGroundLAO(String backGroundLAO) {
        this.backGroundLAO = backGroundLAO;
    }

    public ArrayList<ModelDistrict> getModelDistricts() {
        return modelDistricts;
    }

    public void setModelDistricts(ArrayList<ModelDistrict> modelDistricts) {
        this.modelDistricts = modelDistricts;
    }

    private Context context;
    public ModelProvinces(Context context) {
        this.context = context;
    }
    @NonNull
    @Override
    public String toString() {
        String name ="";
        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                name= nameTH;
                break;
            case "en":
                name= nameEN;
                break;
            case "lo":
                name= nameLAO;
                break;
        }
        return name;
    }
}
