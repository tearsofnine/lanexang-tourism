package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelTypeOfTrip  implements Parcelable {

    private int typeID;
    private String typeTH;
    private String typeEN;
    private String typeLAO;
    private ArrayList<ModelTrip> modelTrips;

    public ModelTypeOfTrip() {

    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getTypeTH() {
        return typeTH;
    }

    public void setTypeTH(String typeTH) {
        this.typeTH = typeTH;
    }

    public String getTypeEN() {
        return typeEN;
    }

    public void setTypeEN(String typeEN) {
        this.typeEN = typeEN;
    }

    public String getTypeLAO() {
        return typeLAO;
    }

    public void setTypeLAO(String typeLAO) {
        this.typeLAO = typeLAO;
    }

    public ArrayList<ModelTrip> getModelTrips() {
        return modelTrips;
    }

    public void setModelTrips(ArrayList<ModelTrip> modelTrips) {
        this.modelTrips = modelTrips;
    }

    public static Creator<ModelTypeOfTrip> getCREATOR() {
        return CREATOR;
    }

    protected ModelTypeOfTrip(Parcel in) {
        typeID = in.readInt();
        typeTH = in.readString();
        typeEN = in.readString();
        typeLAO = in.readString();
        modelTrips = in.createTypedArrayList(ModelTrip.CREATOR);
    }

    public static final Creator<ModelTypeOfTrip> CREATOR = new Creator<ModelTypeOfTrip>() {
        @Override
        public ModelTypeOfTrip createFromParcel(Parcel in) {
            return new ModelTypeOfTrip(in);
        }

        @Override
        public ModelTypeOfTrip[] newArray(int size) {
            return new ModelTypeOfTrip[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(typeID);
        dest.writeString(typeTH);
        dest.writeString(typeEN);
        dest.writeString(typeLAO);
        dest.writeTypedList(modelTrips);
    }
}
