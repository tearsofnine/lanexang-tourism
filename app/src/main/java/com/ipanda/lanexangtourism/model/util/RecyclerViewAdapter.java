package com.ipanda.lanexangtourism.model.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ipanda.lanexangtourism.MainActivity;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.fragment_menu.Fragment_menu_category_8;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> implements ItemMoveCallback.ItemTouchHelperContract{
    private ArrayList<ModelItem> modelItems;
    private Context context;
    private LayoutInflater layoutInflater;
    private ItemTouchHelper touchHelper;
    private  ItemClickListener mItemClickListener;
    private final StartDragListener mStartDragListener;
    private String userId;
    private String url_moving = null;
    private int getPosition;
    private Fragment fragment;
    private Bundle bundle;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        View rowView;
        int viewType;
        ToggleButton sectionToggleButton;
        TextView text_title,text_result,title_item,topic_item,distance,count_user_id,text_title_user_matching;
        ImageView img_menu_categoryID,img_item_list_great_places,img_favorite,img_position;
        RelativeLayout user_matching;



        public MyViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            title_item =  itemView.findViewById(R.id.title_item);
            topic_item =  itemView.findViewById(R.id.topic_item);
            distance =  itemView.findViewById(R.id.distance);
            img_item_list_great_places =  itemView.findViewById(R.id.img_item_list_great_places);
            img_favorite =  itemView.findViewById(R.id.img_favorite);
            img_position =  itemView.findViewById(R.id.img_position);
            count_user_id = itemView.findViewById(R.id.count_user_id);
            user_matching = itemView.findViewById(R.id.user_matching);
            text_title_user_matching  = itemView.findViewById(R.id.text_title_user_matching);
        }
    }

    public RecyclerViewAdapter(ArrayList<ModelItem> modelItems, StartDragListener startDragListener, ItemClickListener mItemClickListener, Context context, String userId) {
        this.modelItems = modelItems;
        mStartDragListener = startDragListener;
        this.mItemClickListener = mItemClickListener;
        this.context = context;
        this.userId = userId;
//        this.layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_favorite, parent, false);

//        return new MyViewHolder(layoutInflater.inflate(R.layout.list_filter,parent,false));

        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final ModelItem item = (ModelItem) modelItems.get(position);
        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                holder.title_item.setText(item.getTopicTH());
                holder.topic_item.setText(item.getAddressTH());
                break;
            case "en":
                if (item.getTopicEN().equals("") || item.getTopicEN() == null || item.getTopicEN().equals("null")) {
                    holder.title_item.setText(item.getTopicTH());
                    holder.topic_item.setText(item.getAddressTH());

                } else {
                    holder.title_item.setText(item.getTopicEN());
                    holder.topic_item.setText(item.getAddressEN());

                }
                break;
            case "lo":
                if (item.getTopicLAO().equals("") || item.getTopicLAO() == null || item.getTopicLAO().equals("null")) {
                    holder.title_item.setText(item.getTopicTH());
                    holder.topic_item.setText(item.getAddressTH());
                } else {
                    holder.title_item.setText(item.getTopicLAO());
                    holder.topic_item.setText(item.getAddressLAO());
                }
                break;
        }

        holder.distance.setText(item.getDistance());

        Picasso.get().load(item.getModelFilePaths().getPathImg()).into(holder.img_item_list_great_places);
        holder.img_favorite.setImageResource(R.drawable.ic_favorite_click);
        holder.img_position.setImageResource(R.mipmap.ic_position);
        holder.count_user_id.setText(item.getFavCount());

        String loca =  context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th");
        if(!loca.equals("th"))
            holder.text_title_user_matching.setTextSize(13.0f);

        holder.img_item_list_great_places.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() ==
                        MotionEvent.ACTION_DOWN) {
                    mStartDragListener.requestDrag(holder);
                }
                return false;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.itemClicked(item);
                getPosition = position;
            }
        });

        holder.user_matching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.itemClickedUserMatching(item);
            }
        });

        holder.img_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("LANEXANG INFO");
                builder.setIcon(R.mipmap.ic_logo_app);
                builder.setMessage("Do you want to remove item?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mItemClickListener.itemClickedUnFavorite(item,holder.img_favorite,position);
                                modelItems.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }

        });


    }


    @Override
    public int getItemCount() {
//        return modelItems.size();
        return modelItems == null ? 0 : modelItems.size();
    }


    @Override
    public void onRowMoved(int oldPosition, int newPosition) {

        ModelItem targetItem = modelItems.get(oldPosition);
        ModelItem item = new ModelItem(targetItem);
        modelItems.remove(oldPosition);
        modelItems.add(newPosition, item);
        notifyItemMoved(oldPosition, newPosition);

        url_moving = context.getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/UpdatePosFavorite?idUsers="+userId+"&oldPosition="+(oldPosition+1)+"&newPosition="+(newPosition+1)+"";



        //Toast.makeText(context, (newPosition)+"", Toast.LENGTH_SHORT).show();
        new MovingFavorite(this).execute(url_moving);

    }


    @Override
    public void onRowSelected(MyViewHolder myViewHolder) {
//        myViewHolder.rowView.setBackgroundColor(Color.WHITE);

    }

    @Override
    public void onRowClear(MyViewHolder myViewHolder) {
//        myViewHolder.rowView.setBackgroundColor(Color.WHITE);

    }

    @Override
    public void onViewSwiped(int position) {

    }


    public class MovingFavorite extends AsyncTask<String,Integer,String>{

        public MovingFavorite(RecyclerViewAdapter context){

        }

        private String addFavorite(String myUrl) throws IOException {
            InputStream is = null;
            try {
                URL url = new URL(myUrl);
                Log.e("url", myUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is : " + response);
                Log.e("response", "" + response);
                is = conn.getInputStream();
                Log.e("is", is.toString());
                return null;
            }catch(Exception e) {
                Log.e("error exp", e.getMessage());
                return "error";
            }finally {
                if (is != null) {
                    is.close();

                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                return addFavorite(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }


    private void getFragment() {
        fragment = new Fragment_menu_category_8();
        bundle = new Bundle();
        fragment.setArguments(bundle);
        switchContent(R.id.contentContainer, fragment);
    }

    private void switchContent(int frameLayout_main, Fragment fragment) {
        if (context == null)
            return;
        if (context instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) context;
            Fragment frag = fragment;
            mainActivity.switchContent(frameLayout_main, frag);
        }
    }

}
