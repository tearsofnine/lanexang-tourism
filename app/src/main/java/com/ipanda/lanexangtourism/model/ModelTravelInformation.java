package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelTravelInformation implements Parcelable {
    private String idTravelInformation;
    private String topic_th;
    private String topic_en;
    private String topic_lao;
    private String history_th;
    private String history_en;
    private String history_lao;
    private String government_th;
    private String government_en;
    private String government_lao;
    private String population_th;
    private String population_en;
    private String population_lao;
    private String language_th;
    private String language_en;
    private String language_lao;
    private String religion_th;
    private String religion_en;
    private String religion_lao;
    private String countriesID;
    private ModelFilePaths modelFilePaths = new ModelFilePaths();

    public ModelTravelInformation() {

    }

    public String getIdTravelInformation() {
        return idTravelInformation;
    }

    public void setIdTravelInformation(String idTravelInformation) {
        this.idTravelInformation = idTravelInformation;
    }

    public String getTopic_th() {
        return topic_th;
    }

    public void setTopic_th(String topic_th) {
        this.topic_th = topic_th;
    }

    public String getTopic_en() {
        return topic_en;
    }

    public void setTopic_en(String topic_en) {
        this.topic_en = topic_en;
    }

    public String getTopic_lao() {
        return topic_lao;
    }

    public void setTopic_lao(String topic_lao) {
        this.topic_lao = topic_lao;
    }

    public String getHistory_th() {
        return history_th;
    }

    public void setHistory_th(String history_th) {
        this.history_th = history_th;
    }

    public String getHistory_en() {
        return history_en;
    }

    public void setHistory_en(String history_en) {
        this.history_en = history_en;
    }

    public String getHistory_lao() {
        return history_lao;
    }

    public void setHistory_lao(String history_lao) {
        this.history_lao = history_lao;
    }

    public String getGovernment_th() {
        return government_th;
    }

    public void setGovernment_th(String government_th) {
        this.government_th = government_th;
    }

    public String getGovernment_en() {
        return government_en;
    }

    public void setGovernment_en(String government_en) {
        this.government_en = government_en;
    }

    public String getGovernment_lao() {
        return government_lao;
    }

    public void setGovernment_lao(String government_lao) {
        this.government_lao = government_lao;
    }

    public String getPopulation_th() {
        return population_th;
    }

    public void setPopulation_th(String population_th) {
        this.population_th = population_th;
    }

    public String getPopulation_en() {
        return population_en;
    }

    public void setPopulation_en(String population_en) {
        this.population_en = population_en;
    }

    public String getPopulation_lao() {
        return population_lao;
    }

    public void setPopulation_lao(String population_lao) {
        this.population_lao = population_lao;
    }

    public String getLanguage_th() {
        return language_th;
    }

    public void setLanguage_th(String language_th) {
        this.language_th = language_th;
    }

    public String getLanguage_en() {
        return language_en;
    }

    public void setLanguage_en(String language_en) {
        this.language_en = language_en;
    }

    public String getLanguage_lao() {
        return language_lao;
    }

    public void setLanguage_lao(String language_lao) {
        this.language_lao = language_lao;
    }

    public String getReligion_th() {
        return religion_th;
    }

    public void setReligion_th(String religion_th) {
        this.religion_th = religion_th;
    }

    public String getReligion_en() {
        return religion_en;
    }

    public void setReligion_en(String religion_en) {
        this.religion_en = religion_en;
    }

    public String getReligion_lao() {
        return religion_lao;
    }

    public void setReligion_lao(String religion_lao) {
        this.religion_lao = religion_lao;
    }

    public String getCountriesID() {
        return countriesID;
    }

    public void setCountriesID(String countriesID) {
        this.countriesID = countriesID;
    }

    public ModelFilePaths getModelFilePaths() {
        return modelFilePaths;
    }

    public void setModelFilePaths(ModelFilePaths modelFilePaths) {
        this.modelFilePaths = modelFilePaths;
    }

    protected ModelTravelInformation(Parcel in) {
        idTravelInformation = in.readString();
        topic_th = in.readString();
        topic_en = in.readString();
        topic_lao = in.readString();
        history_th = in.readString();
        history_en = in.readString();
        history_lao = in.readString();
        government_th = in.readString();
        government_en = in.readString();
        government_lao = in.readString();
        population_th = in.readString();
        population_en = in.readString();
        population_lao = in.readString();
        language_th = in.readString();
        language_en = in.readString();
        language_lao = in.readString();
        religion_th = in.readString();
        religion_en = in.readString();
        religion_lao = in.readString();
        countriesID = in.readString();
        modelFilePaths = in.readParcelable(ModelFilePaths.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idTravelInformation);
        dest.writeString(topic_th);
        dest.writeString(topic_en);
        dest.writeString(topic_lao);
        dest.writeString(history_th);
        dest.writeString(history_en);
        dest.writeString(history_lao);
        dest.writeString(government_th);
        dest.writeString(government_en);
        dest.writeString(government_lao);
        dest.writeString(population_th);
        dest.writeString(population_en);
        dest.writeString(population_lao);
        dest.writeString(language_th);
        dest.writeString(language_en);
        dest.writeString(language_lao);
        dest.writeString(religion_th);
        dest.writeString(religion_en);
        dest.writeString(religion_lao);
        dest.writeString(countriesID);
        dest.writeParcelable(modelFilePaths, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTravelInformation> CREATOR = new Creator<ModelTravelInformation>() {
        @Override
        public ModelTravelInformation createFromParcel(Parcel in) {
            return new ModelTravelInformation(in);
        }

        @Override
        public ModelTravelInformation[] newArray(int size) {
            return new ModelTravelInformation[size];
        }
    };
}
