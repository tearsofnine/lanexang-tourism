package com.ipanda.lanexangtourism.model;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public class ModelDistrict {

    private String id;
    private String code;
    private String nameTH;
    private String nameEN;
    private String nameLAO;
    private ArrayList<ModelSubDistricts> modelSubdistricts;
    private Context context;
    public ModelDistrict(Context context) {
        this.context = context;
    }
    public ModelDistrict() {

    }
    public ModelDistrict(String id, String code, String nameTH, String nameEN, String nameLAO, ArrayList<ModelSubDistricts> modelSubdistricts) {
        this.id = id;
        this.code = code;
        this.nameTH = nameTH;
        this.nameEN = nameEN;
        this.nameLAO = nameLAO;
        this.modelSubdistricts = new ArrayList<ModelSubDistricts>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameLAO() {
        return nameLAO;
    }

    public void setNameLAO(String nameLAO) {
        this.nameLAO = nameLAO;
    }

    public ArrayList<ModelSubDistricts> getModelSubdistricts() {
        return modelSubdistricts;
    }

    public void setModelSubdistricts(ArrayList<ModelSubDistricts> modelSubdistricts) {
        this.modelSubdistricts = modelSubdistricts;
    }

    @NonNull
    @Override
    public String toString() {
        String name ="";
        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                name= nameTH;
                break;
            case "en":
                name= nameEN;
                break;
            case "lo":
                name= nameLAO;
                break;
        }
        return name;
    }
}
