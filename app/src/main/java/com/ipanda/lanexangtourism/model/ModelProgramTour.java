package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


public class ModelProgramTour implements Parcelable {

    private int programTourID;
    private String programTourTH;
    private String programTourEN;
    private String programTourLAO;
    private String pathImg;
    private ArrayList<ModelTypeOfTrip> modelTypeOfTrips;
    private ArrayList<ModelTrip> modelTrips;

    public ModelProgramTour() {

    }

    public int getProgramTourID() {
        return programTourID;
    }

    public void setProgramTourID(int programTourID) {
        this.programTourID = programTourID;
    }

    public String getProgramTourTH() {
        return programTourTH;
    }

    public void setProgramTourTH(String programTourTH) {
        this.programTourTH = programTourTH;
    }

    public String getProgramTourEN() {
        return programTourEN;
    }

    public void setProgramTourEN(String programTourEN) {
        this.programTourEN = programTourEN;
    }

    public String getProgramTourLAO() {
        return programTourLAO;
    }

    public void setProgramTourLAO(String programTourLAO) {
        this.programTourLAO = programTourLAO;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public ArrayList<ModelTypeOfTrip> getModelTypeOfTrips() {
        return modelTypeOfTrips;
    }

    public void setModelTypeOfTrips(ArrayList<ModelTypeOfTrip> modelTypeOfTrips) {
        this.modelTypeOfTrips = modelTypeOfTrips;
    }

    public ArrayList<ModelTrip> getModelTrips() {
        return modelTrips;
    }

    public void setModelTrips(ArrayList<ModelTrip> modelTrips) {
        this.modelTrips = modelTrips;
    }

    protected ModelProgramTour(Parcel in) {
        programTourID = in.readInt();
        programTourTH = in.readString();
        programTourEN = in.readString();
        programTourLAO = in.readString();
        pathImg = in.readString();
        modelTypeOfTrips = in.createTypedArrayList(ModelTypeOfTrip.CREATOR);
        modelTrips = in.createTypedArrayList(ModelTrip.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(programTourID);
        dest.writeString(programTourTH);
        dest.writeString(programTourEN);
        dest.writeString(programTourLAO);
        dest.writeString(pathImg);
        dest.writeTypedList(modelTypeOfTrips);
        dest.writeTypedList(modelTrips);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelProgramTour> CREATOR = new Creator<ModelProgramTour>() {
        @Override
        public ModelProgramTour createFromParcel(Parcel in) {
            return new ModelProgramTour(in);
        }

        @Override
        public ModelProgramTour[] newArray(int size) {
            return new ModelProgramTour[size];
        }
    };
}
