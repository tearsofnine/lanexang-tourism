package com.ipanda.lanexangtourism.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class ModelItem implements Parcelable {

    private String itemID;
    private String topicTH;
    private String topicEN;
    private String topicLAO;
    private String tel;
    private String addressTH;
    private String addressEN;
    private String addressLAO;
    private String dateOpen;
    private String dateClose;
    private String timeStart;
    private String timeEnd;
    private String detailTH;
    private String detailEN;
    private String detailLAO;
    private String distance;
    private int distance_metr;
    private int idTypeOfTravel;
    private double latitude;
    private double longitude;
    private boolean favorites;
    private String position;
    private String arLink;
    private String favCount;
    private ModelSubDistricts modelSubDistricts = new ModelSubDistricts();
    private ModelMaps modelMaps = new ModelMaps();
    private ModelFilePaths modelFilePaths = new ModelFilePaths();
    private ModelTypeOfItem modelTypeOfItem = new ModelTypeOfItem();
    private ArrayList<ModelUsers> modelUsers = new ArrayList<>();

    public ModelItem() {

    }

    public ModelItem(ModelItem item){
        this.itemID = item.itemID;
        this.topicTH = item.topicTH;
        this.topicEN = item.topicEN;
        this.topicLAO = item.topicLAO;
        this.tel = item.tel;
        this.addressTH = item.addressTH;
        this.addressEN = item.addressEN;
        this.addressLAO = item.addressLAO;
        this.dateOpen = item.dateOpen;
        this.dateClose = item.dateClose;
        this.timeStart = item.timeStart;
        this.timeEnd = item.timeEnd;
        this.detailTH = item.detailTH;
        this.detailEN = item.detailEN;
        this.detailLAO = item.detailLAO;
        this.distance = item.distance;
        this.distance_metr = item.distance_metr;
        this.idTypeOfTravel = item.idTypeOfTravel;
        this.latitude = item.latitude;
        this.longitude = item.longitude;
        this.favorites = item.favorites;
        this.position = item.position;
        this.arLink = item.arLink;
        this.favCount = item.favCount;
        this.modelFilePaths = item.modelFilePaths;
        this.modelTypeOfItem = item.modelTypeOfItem;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getTopicTH() {
        return topicTH;
    }

    public void setTopicTH(String topicTH) {
        this.topicTH = topicTH;
    }

    public String getTopicEN() {
        return topicEN;
    }

    public void setTopicEN(String topicEN) {
        this.topicEN = topicEN;
    }

    public String getTopicLAO() {
        return topicLAO;
    }

    public void setTopicLAO(String topicLAO) {
        this.topicLAO = topicLAO;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddressTH() {
        return addressTH;
    }

    public void setAddressTH(String addressTH) {
        this.addressTH = addressTH;
    }

    public String getAddressEN() {
        return addressEN;
    }

    public void setAddressEN(String addressEN) {
        this.addressEN = addressEN;
    }

    public String getAddressLAO() {
        return addressLAO;
    }

    public void setAddressLAO(String addressLAO) {
        this.addressLAO = addressLAO;
    }

    public String getDateOpen() {
        return dateOpen;
    }

    public void setDateOpen(String dateOpen) {
        this.dateOpen = dateOpen;
    }

    public String getDateClose() {
        return dateClose;
    }

    public void setDateClose(String dateClose) {
        this.dateClose = dateClose;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getDetailTH() {
        return detailTH;
    }

    public void setDetailTH(String detailTH) {
        this.detailTH = detailTH;
    }

    public String getDetailEN() {
        return detailEN;
    }

    public void setDetailEN(String detailEN) {
        this.detailEN = detailEN;
    }

    public String getDetailLAO() {
        return detailLAO;
    }

    public void setDetailLAO(String detailLAO) {
        this.detailLAO = detailLAO;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getDistance_metr() {
        return distance_metr;
    }

    public void setDistance_metr(int distance_metr) {
        this.distance_metr = distance_metr;
    }

    public int getIdTypeOfTravel() {
        return idTypeOfTravel;
    }

    public void setIdTypeOfTravel(int idTypeOfTravel) {
        this.idTypeOfTravel = idTypeOfTravel;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isFavorites() {
        return favorites;
    }

    public void setFavorites(boolean favorites) {
        this.favorites = favorites;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getArLink() {
        return arLink;
    }

    public void setArLink(String arLink) {
        this.arLink = arLink;
    }

    public String getFavCount() {
        return favCount;
    }

    public void setFavCount(String favCount) {
        this.favCount = favCount;
    }

    public ModelSubDistricts getModelSubDistricts() {
        return modelSubDistricts;
    }

    public void setModelSubDistricts(ModelSubDistricts modelSubDistricts) {
        this.modelSubDistricts = modelSubDistricts;
    }

    public ModelMaps getModelMaps() {
        return modelMaps;
    }

    public void setModelMaps(ModelMaps modelMaps) {
        this.modelMaps = modelMaps;
    }

    public ModelFilePaths getModelFilePaths() {
        return modelFilePaths;
    }

    public void setModelFilePaths(ModelFilePaths modelFilePaths) {
        this.modelFilePaths = modelFilePaths;
    }

    public ModelTypeOfItem getModelTypeOfItem() {
        return modelTypeOfItem;
    }

    public void setModelTypeOfItem(ModelTypeOfItem modelTypeOfItem) {
        this.modelTypeOfItem = modelTypeOfItem;
    }

    public ArrayList<ModelUsers> getModelUsers() {
        return modelUsers;
    }

    public void setModelUsers(ArrayList<ModelUsers> modelUsers) {
        this.modelUsers = modelUsers;
    }

    protected ModelItem(Parcel in) {
        itemID = in.readString();
        topicTH = in.readString();
        topicEN = in.readString();
        topicLAO = in.readString();
        tel = in.readString();
        addressTH = in.readString();
        addressEN = in.readString();
        addressLAO = in.readString();
        dateOpen = in.readString();
        dateClose = in.readString();
        timeStart = in.readString();
        timeEnd = in.readString();
        detailTH = in.readString();
        detailEN = in.readString();
        detailLAO = in.readString();
        distance = in.readString();
        distance_metr = in.readInt();
        idTypeOfTravel = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        favorites = in.readByte() != 0;
        position = in.readString();
        arLink = in.readString();
        favCount = in.readString();
        modelFilePaths = in.readParcelable(ModelFilePaths.class.getClassLoader());
        modelTypeOfItem = in.readParcelable(ModelTypeOfItem.class.getClassLoader());
        modelUsers = in.createTypedArrayList(ModelUsers.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemID);
        dest.writeString(topicTH);
        dest.writeString(topicEN);
        dest.writeString(topicLAO);
        dest.writeString(tel);
        dest.writeString(addressTH);
        dest.writeString(addressEN);
        dest.writeString(addressLAO);
        dest.writeString(dateOpen);
        dest.writeString(dateClose);
        dest.writeString(timeStart);
        dest.writeString(timeEnd);
        dest.writeString(detailTH);
        dest.writeString(detailEN);
        dest.writeString(detailLAO);
        dest.writeString(distance);
        dest.writeInt(distance_metr);
        dest.writeInt(idTypeOfTravel);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeByte((byte) (favorites ? 1 : 0));
        dest.writeString(position);
        dest.writeString(arLink);
        dest.writeString(favCount);
        dest.writeParcelable(modelFilePaths, flags);
        dest.writeParcelable(modelTypeOfItem, flags);
        dest.writeTypedList(modelUsers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelItem> CREATOR = new Creator<ModelItem>() {
        @Override
        public ModelItem createFromParcel(Parcel in) {
            return new ModelItem(in);
        }

        @Override
        public ModelItem[] newArray(int size) {
            return new ModelItem[size];
        }
    };
}
