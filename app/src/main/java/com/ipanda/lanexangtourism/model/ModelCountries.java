package com.ipanda.lanexangtourism.model;

import java.util.ArrayList;

public class ModelCountries {

    private String id;
    private String code;
    private String nameTH;
    private String nameEN;
    private String nameLAO;
    private ArrayList<ModelProvinces> modelProvinces;


    public ModelCountries() {

    }


    public ModelCountries(String id, String code, String nameTH, String nameEN, String nameLAO, ArrayList<ModelProvinces> modelProvinces) {
        this.id = id;
        this.code = code;
        this.nameTH = nameTH;
        this.nameEN = nameEN;
        this.nameLAO = nameLAO;
        this.modelProvinces = new ArrayList<ModelProvinces>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameTH() {
        return nameTH;
    }

    public void setNameTH(String nameTH) {
        this.nameTH = nameTH;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameLAO() {
        return nameLAO;
    }

    public void setNameLAO(String nameLAO) {
        this.nameLAO = nameLAO;
    }

    public ArrayList<ModelProvinces> getModelProvinces() {
        return modelProvinces;
    }

    public void setModelProvinces(ArrayList<ModelProvinces> modelProvinces) {
        this.modelProvinces = modelProvinces;
    }

}
