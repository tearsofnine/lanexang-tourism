package com.ipanda.lanexangtourism;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskTrip;
import com.ipanda.lanexangtourism.adapter.AdapterTrip;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceTrip;
import com.ipanda.lanexangtourism.model.ModelTrip;

import java.util.ArrayList;

public class TripActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener , CallBackServiceTrip, TextWatcher, View.OnClickListener {

    private ArrayList<ModelTrip> tripArrayList = null;
    private ArrayList<ModelTrip> modelTrips = new ArrayList<>();
    private AdapterTrip adapter;
    private ProgressDialog alertDialog = null;
    private int json_index = 0;

    private Toolbar toolbar;
    private TextView toolbar_title,textViewTypTrip;
    private LinearLayout[] ClickNavigation_Array;
    private TextView[] TextView_Array;
    private String url_json = "/lanexang_tourism/user/GetData/GetProgramTour";
    private Button btnDay,btnLong;
    private TableRow cardDayTrip,cardLongDayTrip;
    private TextView editText;
    private Intent intent;
    private ListView listView;
    private ViewGroup header;
    private DatabaseHelper myDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title.setText(getIntent().getStringExtra("ProgramTourName"));

        json_index = getIntent().getIntExtra("JSON_POSITION",0);
        listView =  findViewById(R.id.list_trip);
        tripArrayList = new ArrayList<>();
//        new AsyncTaskTrip(this,json_index).execute(getResources().getString(R.string.host_ip)+url_json);


        myDB = new DatabaseHelper(this);
        String UserId = this.getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
        String app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            Toast.makeText(this, "Offline", Toast.LENGTH_SHORT).show();
            tripArrayList = myDB.getModelTrip(json_index+1);
            onSetAdapter();
        }else {
            new AsyncTaskTrip(this,json_index).execute(getResources().getString(R.string.host_ip)+url_json+"?UserId="+UserId);
        }


        try {
            Initializing(8);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        SetNameMenu();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //closeKeyboard(drawerView);
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //closeKeyboard();

            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onClick(View v) {
        RelativeLayout layout = findViewById(R.id.RelativeLayout_btn_shape_trip);
        switch(v.getId()){
            case R.id.day_trip:
                editText.setText("1-Day Trip");
//                layout.setBackground(getDrawable(R.drawable.button_shape_trip_blue));
                cardDayTrip.setBackgroundColor(getResources().getColor(R.color.colorTeal));
                cardLongDayTrip.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.long_day_trip:
                editText.setText("Long Day Trip");
                //layout.setBackground(getDrawable(R.drawable.button_shape_trip_skyblue));
                cardDayTrip.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                cardLongDayTrip.setBackgroundColor(getResources().getColor(R.color.colorTeal));
                break;

        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
    public void Initializing(int numMenu) throws InstantiationException, IllegalAccessException {

        TextView_Array = new TextView[numMenu];
        ClickNavigation_Array = new LinearLayout[numMenu];
        for(int i=0; i<numMenu; i++) {

            String TextViewID = "TvCategory_" + (i + 1);
            int TextViewResID = getResources().getIdentifier(TextViewID, "id", getPackageName());

            String LinearLayoutID = "menu_category_" + (i + 1);
            int LinearLayoutResID = getResources().getIdentifier(LinearLayoutID, "id", getPackageName());

            TextView_Array[i] = (TextView) findViewById(TextViewResID);
            ClickNavigation_Array[i] = (LinearLayout) findViewById(LinearLayoutResID);

        }
    }
    public void SetNameMenu(){
        String[] NameMenuCategory;
        Resources res = getResources();
        NameMenuCategory = res.getStringArray(R.array.NameCategory);
        for(int i=0;i<NameMenuCategory.length;i++) {
            TextView_Array[i].setText(NameMenuCategory[i]);
        }
    }
    public void ClickNavigation(View view){
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        switch (view.getId()){
            case R.id.menu_category_1:
                intent.putExtra("retrunMain2fragment",0);
                break;
            case R.id.menu_category_2:
                intent.putExtra("retrunMain2fragment",1);
                break;
            case R.id.menu_category_3:
                intent.putExtra("retrunMain2fragment",2);
                break;
            case R.id.menu_category_4:
                intent.putExtra("retrunMain2fragment",3);
                break;
            case R.id.menu_category_5:
                intent.putExtra("retrunMain2fragment",4);
                break;
            case R.id.menu_category_6:
                intent.putExtra("retrunMain2fragment",5);
                break;
            case R.id.menu_category_7:
                intent.putExtra("retrunMain2fragment",6);
                break;
            case R.id.menu_category_8:
                intent.putExtra("retrunMain2fragment",7);
                break;
        }

        startActivity(intent);
        finish();
    }




    @Override
    public void onPreCallService() {
        this.showProgressDialog();

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelTrip> tripArrayList) {
        if (tripArrayList != null && tripArrayList.size() > 0){
            Log.i("Check data", "" + tripArrayList.size());
            this.tripArrayList = tripArrayList;
            onSetAdapter();
            adapter.notifyDataSetChanged();
        }else if(tripArrayList != null){
            showDataNotFound();
        }
        this.dismissProgressDialog();
    }

    private void onSetAdapter() {
        LayoutInflater layoutinflater = getLayoutInflater();
        header = (ViewGroup) layoutinflater.inflate(R.layout.layout_toolbar_trip,listView,false);
        listView.addHeaderView(header);
        adapter = new AdapterTrip(this, tripArrayList,json_index);
        listView.setAdapter(adapter);

        editText = findViewById(R.id.editText);
        editText.setVisibility(View.VISIBLE);
        editText.addTextChangedListener(this);
        editText.setText("1-Day Trip");
        btnDay = findViewById(R.id.day_trip);
        btnLong = findViewById(R.id.long_day_trip);
        btnDay.setOnClickListener(this);
        btnLong.setOnClickListener(this);

        cardDayTrip = findViewById(R.id.line_day_trip);
        cardLongDayTrip = findViewById(R.id.line_long_day_trip);
        textViewTypTrip = findViewById(R.id.type_trip);
        cardDayTrip.setBackgroundColor(getResources().getColor(R.color.colorTeal));

//        listView.setOnItemClickListener(this);
    }

    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        this.adapter.getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage(" "+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void showDataNotFound(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.text_data_not_found));
        builder.setMessage(getResources().getString(R.string.text_back_main));
                builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }


//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent intent = new Intent(this,TripDescriptionActivity.class);
//
//        Toast.makeText(this, tripArrayList.get(position).getTripTH()+"", Toast.LENGTH_SHORT).show();
//
//        intent.putExtra("TRIP_TH",tripArrayList.get(position).getTripTH());
//        intent.putExtra("JSON_POSITION",json_index);
//        startActivity(intent);
//    }
}
