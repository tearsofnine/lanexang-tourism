package com.ipanda.lanexangtourism.fragment_information;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.InformationProvinceDetailActivity;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskTravelInformation;
import com.ipanda.lanexangtourism.adapter.AdapterInformationProvince;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceTravelInformation;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_information_province extends Fragment implements CallBackServiceTravelInformation {

    private ListView mListView;
    private String url_json = "/lanexang_tourism/user/GetData/getDataTravelInformation";
    private ArrayList<ModelTravelInformation> modelTravelInformation = null;
    private ViewGroup header;
    private String[] img_country = { "bg_th", "bg_la"};
    private DatabaseHelper myDB;

    public Fragment_information_province() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Toast toast = Toast.makeText ( this.getActivity(), getArguments().getString("params"), Toast.LENGTH_LONG );
        toast.show ( );*/
        myDB = new DatabaseHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_province, container, false);


        String imageViewID = img_country[getArguments().getInt("selectPosition")];
        int imageViewResID = getResources().getIdentifier(imageViewID, "drawable", getActivity().getPackageName());
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.fragment_information_province_bg);
        constraintLayout.setBackground(ResourcesCompat.getDrawable(getActivity().getResources(), imageViewResID, null));

        mListView = (ListView) view.findViewById(R.id.InforList);
        header = (ViewGroup)inflater.inflate(R.layout.header_information_province,mListView,false);

        String app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            Toast.makeText(getContext(), "Offline", Toast.LENGTH_SHORT).show();
            modelTravelInformation = myDB.getInformations(String.valueOf((getArguments().getInt("selectPosition")+1)));
            onSetAdapter();
        }else {
            new AsyncTaskTravelInformation(this).execute(getResources().getString(R.string.host_ip)+url_json+"?countries="+(getArguments().getInt("selectPosition")+1));
        }

        return view;
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelTravelInformation> modelTravelInformation) {
        if (modelTravelInformation != null && modelTravelInformation.size() > 0) {
            Log.i("Check data", "" + modelTravelInformation.size());
            this.modelTravelInformation = modelTravelInformation;
           onSetAdapter();

        }
    }

    private void onSetAdapter() {
        AdapterInformationProvince aAdapter =  new AdapterInformationProvince(this.getActivity(),modelTravelInformation);
        TextView textViewHeader = (TextView)header.findViewById(R.id.headNameCountry);
        textViewHeader.setText(getArguments().getString("selectCountry"));

        mListView.addHeaderView(header);
        mListView.setAdapter(aAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position !=0) {
                    Intent intent = new Intent(getActivity(), InformationProvinceDetailActivity.class);
                    intent.putExtra("modelTravelInformation", modelTravelInformation.get(position-1));
                    startActivity(intent);

                }
            }
        });
    }

    @Override
    public void onRequestFailed(String result) {

    }
}
