package com.ipanda.lanexangtourism.fragment_information;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_information_province_detail extends Fragment {

    public Fragment_information_province_detail() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_province_detail, container, false);
        TextView textViewNameProvince =(TextView) view.findViewById(R.id.Namecountry);
        ImageView head_img =(ImageView) view.findViewById(R.id.img_country);

        ModelTravelInformation modelTravelInformation = getArguments().getParcelable("modelTravelInformation");


        Picasso.get().load(modelTravelInformation.getModelFilePaths().getPathImg()).into(head_img);


        TextView history = (TextView) view.findViewById(R.id.history);
        TextView government = (TextView) view.findViewById(R.id.government);
        TextView population = (TextView) view.findViewById(R.id.population);
        TextView language = (TextView) view.findViewById(R.id.language);
        TextView religion = (TextView) view.findViewById(R.id.religion);
        switch (this.getContext().getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                textViewNameProvince.setText(modelTravelInformation.getTopic_th());
                history.setText(modelTravelInformation.getHistory_th());
                government.setText(modelTravelInformation.getGovernment_th());
                population.setText(modelTravelInformation.getPopulation_th());
                language.setText(modelTravelInformation.getLanguage_th());
                religion.setText(modelTravelInformation.getReligion_th());
                break;
            case "en":
                if (modelTravelInformation.getTopic_en().equals("") || modelTravelInformation.getTopic_en() == null || modelTravelInformation.getTopic_en().equals("null")) {
                    textViewNameProvince.setText(modelTravelInformation.getTopic_th());
                    history.setText(modelTravelInformation.getHistory_th());
                    government.setText(modelTravelInformation.getGovernment_th());
                    population.setText(modelTravelInformation.getPopulation_th());
                    language.setText(modelTravelInformation.getLanguage_th());
                    religion.setText(modelTravelInformation.getReligion_th());
                } else {
                    textViewNameProvince.setText(modelTravelInformation.getTopic_en());
                    history.setText(modelTravelInformation.getHistory_en());
                    government.setText(modelTravelInformation.getGovernment_en());
                    population.setText(modelTravelInformation.getPopulation_en());
                    language.setText(modelTravelInformation.getLanguage_en());
                    religion.setText(modelTravelInformation.getReligion_en());
                }
                break;
            case "lo":
                if (modelTravelInformation.getTopic_lao().equals("") || modelTravelInformation.getTopic_lao() == null || modelTravelInformation.getTopic_lao().equals("null")) {
                    textViewNameProvince.setText(modelTravelInformation.getTopic_th());
                    history.setText(modelTravelInformation.getHistory_th());
                    government.setText(modelTravelInformation.getGovernment_th());
                    population.setText(modelTravelInformation.getPopulation_th());
                    language.setText(modelTravelInformation.getLanguage_th());
                    religion.setText(modelTravelInformation.getReligion_th());
                } else {
                    textViewNameProvince.setText(modelTravelInformation.getTopic_lao());
                    history.setText(modelTravelInformation.getHistory_lao());
                    government.setText(modelTravelInformation.getGovernment_lao());
                    population.setText(modelTravelInformation.getPopulation_lao());
                    language.setText(modelTravelInformation.getLanguage_lao());
                    religion.setText(modelTravelInformation.getReligion_lao());
                }
                break;
        }
        return view;
    }


}
