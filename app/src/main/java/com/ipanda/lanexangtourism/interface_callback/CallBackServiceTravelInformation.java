package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelTravelInformation;

import java.util.ArrayList;

public interface CallBackServiceTravelInformation {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelTravelInformation> modelTravelInformation);
    void onRequestFailed(String result);
}
