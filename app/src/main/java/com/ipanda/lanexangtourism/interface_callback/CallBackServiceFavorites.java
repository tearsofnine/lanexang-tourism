package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelItem;

import java.util.ArrayList;

public interface CallBackServiceFavorites {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelItem> modelItem);
    void onRequestFailed(String result);
}
