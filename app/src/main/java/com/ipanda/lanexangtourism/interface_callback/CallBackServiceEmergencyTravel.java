package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;

import java.util.ArrayList;

public interface CallBackServiceEmergencyTravel {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelTypeOfTravel> modelTypeOfTravel);
    void onRequestFailed(String result);
}
