package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelItem;

import java.util.ArrayList;

public interface CallBackServiceDescription {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelItem> modelItems);
    void onRequestFailed(String result);
}
