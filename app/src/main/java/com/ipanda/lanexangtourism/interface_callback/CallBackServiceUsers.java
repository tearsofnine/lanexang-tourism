package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelUsers;

public interface CallBackServiceUsers {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ModelUsers modelUsers);
    void onRequestFailed(String result);
}
