package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelTrip;

import java.util.ArrayList;

public interface CallBackServiceTrip {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelTrip> tripArrayList);
    void onRequestFailed(String result);
}
