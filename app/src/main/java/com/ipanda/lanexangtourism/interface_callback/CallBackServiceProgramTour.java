package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelProgramTour;

import java.util.ArrayList;

public interface CallBackServiceProgramTour {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelProgramTour> programToursArrayList);
    void onRequestFailed(String result);
}
