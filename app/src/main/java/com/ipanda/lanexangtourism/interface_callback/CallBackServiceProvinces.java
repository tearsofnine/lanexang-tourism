package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelProvinces;

import java.util.ArrayList;

public interface CallBackServiceProvinces {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelProvinces> modelProvinces);
    void onRequestFailed(String result);
}
