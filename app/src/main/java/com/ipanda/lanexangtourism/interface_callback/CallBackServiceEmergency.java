package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelEmergency;

import java.util.ArrayList;

public interface CallBackServiceEmergency {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelEmergency> modelEmergency);
    void onRequestFailed(String result);
}
