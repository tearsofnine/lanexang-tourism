package com.ipanda.lanexangtourism.interface_callback;

import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;

import java.util.ArrayList;

public interface CallBackServiceListPhoto {
    void onPreCallService();
    void onCallService();
    void onRequestCompleteListener(ArrayList<ModelFilePaths> modelFilePaths);
    void onRequestFailed(String result);
}
