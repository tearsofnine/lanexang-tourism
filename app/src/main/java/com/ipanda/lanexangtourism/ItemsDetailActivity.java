package com.ipanda.lanexangtourism;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskListPhoto;
import com.ipanda.lanexangtourism.adapter.AdapterSearch;
import com.ipanda.lanexangtourism.adapter.AdapterViewPagerItmes;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceListPhoto;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.util.AddFavorite;
import com.ipanda.lanexangtourism.model.util.ItemClickListener;
import com.ipanda.lanexangtourism.other.SetGoogleMapMin;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemsDetailActivity extends AppCompatActivity implements LocationListener, NavigationView.OnNavigationItemSelectedListener , CallBackServiceListPhoto {
    private static final String TAG = "ItemsDetailActivity";
    private ModelItem modelItem;
    private static final  int CAMERA_PERMISSION_REQ = 1150;
    private static final int REQ_CODE = 123;
    private MapView mapView;
    private Bundle _savedInstanceState;
    private Intent intentCameraActivity;
    private Location locationResult = null;
    private ImageView icon_favorite,mode_ar;
    private boolean favorite = false;
    private String app_mode;
    private TextView[] TextView_Array;
    private int numMenu = 8;
    private LinearLayout[] ClickNavigation_Array;
    private ArrayList<ModelItem> ItemhasBack = new ArrayList<>();
    private ViewPager viewPager;
    private String typeItem;
    private String url_getPathPhoto = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_detail);
        _savedInstanceState = savedInstanceState;

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        modelItem = this.getIntent().getParcelableExtra("ITEMS_DETAIL");
        typeItem = this.getIntent().getStringExtra("typeItem");

        ImageView imageViewItem = (ImageView) findViewById(R.id.img_item_detail);
        TextView textViewTopic = (TextView) findViewById(R.id.topic_item_detail);
        TextView textViewAddress = (TextView) findViewById(R.id.address_item_detail);
        TextView textViewTel = (TextView) findViewById(R.id.tel_item_detail);
        TextView textViewOC = (TextView) findViewById(R.id.open_or_close_item_detail);
        TextView textViewDetail = (TextView) findViewById(R.id.detail_item_detail);
        Button button_navi = (Button) findViewById(R.id.button_navi);

        mode_ar = findViewById(R.id.mode_ar);

        TextView_Array = new TextView[numMenu];
        ClickNavigation_Array = new LinearLayout[numMenu];
        for(int i=0; i<numMenu; i++) {

            String TextViewID = "TvCategory_" + (i + 1);
            int TextViewResID = getResources().getIdentifier(TextViewID, "id", getPackageName());

            String LinearLayoutID = "menu_category_" + (i + 1);
            int LinearLayoutResID = getResources().getIdentifier(LinearLayoutID, "id", getPackageName());

            TextView_Array[i] = (TextView) findViewById(TextViewResID);
            ClickNavigation_Array[i] = (LinearLayout) findViewById(LinearLayoutResID);

        }
        SetNameMenu();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ImageView menuRight = (ImageView) findViewById(R.id.menuRight);
        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });

        icon_favorite = (ImageView) findViewById(R.id.icon_favorite);
        icon_favorite.setColorFilter(null);
        if (modelItem.isFavorites()){
            icon_favorite.setColorFilter(getResources().getColor(R.color.red));
        }
        else
        {
            icon_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
        }

        app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");


        Picasso.get().load(modelItem.getModelFilePaths().getPathImg()).into(imageViewItem);
        String TopicName ="";
        switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                toolbar_title.setText(modelItem.getTopicTH());
                TopicName = modelItem.getTopicTH();
                textViewTopic.setText(modelItem.getTopicTH());
                textViewAddress.setText(getResources().getString(R.string.text_address)+" "+modelItem.getAddressTH());
                textViewDetail.setText(modelItem.getDetailTH());
                break;
            case "en":
                if (modelItem.getTopicEN().equals("") || modelItem.getTopicEN() == null || modelItem.getTopicEN().equals("null")) {
                    toolbar_title.setText(modelItem.getTopicTH());
                    TopicName = modelItem.getTopicTH();
                    textViewTopic.setText(modelItem.getTopicTH());
                    textViewAddress.setText(getResources().getString(R.string.text_address)+" "+modelItem.getAddressTH());
                    textViewDetail.setText(modelItem.getDetailTH());
                } else {
                    toolbar_title.setText(modelItem.getTopicEN());
                    TopicName = modelItem.getTopicEN();
                    textViewTopic.setText(modelItem.getTopicEN());
                    textViewAddress.setText(getResources().getString(R.string.text_address)+" "+modelItem.getAddressEN());
                    textViewDetail.setText(modelItem.getDetailEN());
                }
                button_navi.setTextSize(13.0f);
                break;
            case "lo":
                if (modelItem.getTopicLAO().equals("") || modelItem.getTopicLAO() == null || modelItem.getTopicLAO().equals("null")) {
                    toolbar_title.setText(modelItem.getTopicTH());
                    TopicName = modelItem.getTopicTH();
                    textViewTopic.setText(modelItem.getTopicTH());
                    textViewAddress.setText(getResources().getString(R.string.text_address)+" "+modelItem.getAddressTH());
                    textViewDetail.setText(modelItem.getDetailTH());
                } else {
                    toolbar_title.setText(modelItem.getTopicLAO());
                    TopicName = modelItem.getTopicLAO();
                    textViewTopic.setText(modelItem.getTopicLAO());
                    textViewAddress.setText(getResources().getString(R.string.text_address)+" "+modelItem.getAddressLAO());
                    textViewDetail.setText(modelItem.getDetailLAO());
                }
                button_navi.setTextSize(13.0f);
                break;
        }
        String DateOpen ="";
        switch (modelItem.getDateOpen())
        {
            case "ทุกวัน":
                switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        DateOpen = modelItem.getDateOpen();
                        break;
                    case "en":
                        DateOpen = "Every day";
                        break;
                    case "lo":
                        DateOpen = "ທຸກໆມື້";
                        break;
                }
             break;
            case "หยุดทุกวันเสาร์":
                switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        DateOpen = modelItem.getDateOpen();
                        break;
                    case "en":
                        DateOpen = "Closed on Saturdays";
                        break;
                    case "lo":
                        DateOpen = "ປິດໃນວັນເສົາ";
                        break;
                }
            break;
            case "หยุดทุกวันอาทิตย์":
                switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        DateOpen = modelItem.getDateOpen();
                        break;
                    case "en":
                        DateOpen = "Closed on Sundays";
                        break;
                    case "lo":
                        DateOpen = "ປິດໃນວັນອາທິດ";
                        break;
                }
            break;
            case "ปิด เสาร์-อาทิตย์":
                switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        DateOpen = modelItem.getDateOpen();
                        break;
                    case "en":
                        DateOpen = "Closed on Saturday-Sunday";
                        break;
                    case "lo":
                        DateOpen = "ປິດໃນວັນເສົາ - ວັນອາທິດ";
                        break;
                }
            break;
        }
        textViewOC.setText(DateOpen + " " + modelItem.getTimeStart() + " - " +modelItem.getTimeEnd());
        textViewTel.setText(modelItem.getTel());


        intentCameraActivity = new Intent(ItemsDetailActivity.this, CameraActivity.class);
        intentCameraActivity.putExtra("TopicName",TopicName);

        mapView = (MapView) findViewById(R.id.mapViewNear);
        SetGoogleMapMin setGoogleMapMin = new SetGoogleMapMin(mapView,_savedInstanceState,this,true,modelItem.getModelTypeOfItem().getTypeID(),modelItem.getLatitude(),modelItem.getLongitude());
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap map) {

                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        if (app_mode.equals("offline")){
                            alertDialogCheckOffLineMode();
                        }else {
                            String Lat = String.valueOf(locationResult.getLatitude());
                            String Lng = String.valueOf(locationResult.getLongitude());
                            Intent intent = new Intent(getApplicationContext(), GoogleMapActivity.class);
                            intent.putExtra("Iditem", modelItem.getItemID());
                            intent.putExtra("itemType", modelItem.getModelTypeOfItem().getTypeID());
                            intent.putExtra("LatU", Lat);
                            intent.putExtra("LngU", Lng);
                            intent.putExtra("Lat", String.valueOf(modelItem.getLatitude()));
                            intent.putExtra("Lng", String.valueOf(modelItem.getLongitude()));
                            intent.putExtra("current_item", modelItem.isFavorites());
                            intent.putExtra("ITEMS_DETAIL", modelItem);
//                            startActivity(intent);
                            startActivityForResult(intent,1);
                        }
                    }
                });


            }
        });
        if (app_mode.equals("offline")){

        }else {
            if (modelItem.getArLink().equals("") || modelItem.getArLink().equals("null") || modelItem.getArLink() == null || modelItem.getArLink() == ""){
                mode_ar.setVisibility(View.INVISIBLE);
            }else {
                mode_ar.setVisibility(View.VISIBLE);
            }
            mode_ar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + modelItem.getArLink().split("=")[1])));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(modelItem.getArLink())));
                    }
                }
            });
        }



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationResult = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationResult == null)
                locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        viewPager = findViewById(R.id.slideViewPager);
        url_getPathPhoto = getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getPhoto?typeItem="+typeItem+"&idItems="+modelItem.getItemID();
        new AsyncTaskListPhoto(this).execute(url_getPathPhoto);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        if(ItemhasBack.size() != 0) {
            returnIntent.putParcelableArrayListExtra("ItemhasBack", ItemhasBack);
        }
        else
        {
            //ArrayList<ModelItem> ItemhasBacks = new ArrayList<>();
            ItemhasBack.add(0,modelItem);
            returnIntent.putParcelableArrayListExtra("ItemhasBack",ItemhasBack);
        }

        returnIntent.putExtra("hasBackPressed",true);
        setResult(Activity.RESULT_OK,returnIntent);

        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mapView = (MapView)findViewById(R.id.mapViewNear);
                    SetGoogleMapMin setGoogleMapMin = new SetGoogleMapMin(mapView,_savedInstanceState,this,true,modelItem.getModelTypeOfItem().getTypeID(),modelItem.getLatitude(),modelItem.getLongitude());
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case CAMERA_PERMISSION_REQ:{
                if (grantResults.length > 0 && grantResults[0]  == PackageManager.PERMISSION_GRANTED && grantResults[1]  == PackageManager.PERMISSION_GRANTED) {
                    startActivity(intentCameraActivity);
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void onNaviClick(View view)
    {
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                String url = "http://maps.google.com/maps?saddr="+modelItem.getLatitude()+","+modelItem.getLongitude()+"&daddr="+locationResult.getLatitude()+","+locationResult.getLongitude()+"&mode=driving";
                Uri gmmIntentUri = Uri.parse(url);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void onClickLinearLayoutFavorite (View view){
        Log.e(TAG,": ---> Click Favorite");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            if(modelItem.getModelTypeOfItem().getTypeID() != 9) {
                if (getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken", "") != "") {
                    icon_favorite.setColorFilter(null);
                    if (modelItem.isFavorites()) {
                        icon_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                        modelItem.setFavorites(false);

                    } else {
                        icon_favorite.setColorFilter(getResources().getColor(R.color.red));
                        modelItem.setFavorites(true);
                    }

                    String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                    String url_add_favorite = getResources().getString(R.string.host_ip) + "/lanexang_tourism/user/GetData/AddFavorite?typeItem=" + modelItem.getModelTypeOfItem().getTypeID() + "&idUsers=" + UserId + "&idItems=" + modelItem.getItemID() + "";
                    new AddFavorite(this).execute(url_add_favorite);

                } else {
                    Intent intent = new Intent(this, SocialLoginHelperActivity.class);
                    startActivity(intent);
                }
            }
        }
    }

    public void onClickLinearLayoutShare(View view){
        Log.e(TAG,": ---> Click Share");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            Double latitude = modelItem.getLatitude();
            Double longitude = modelItem.getLongitude();
            String uri = "http://maps.google.com/maps?saddr=" + latitude + "," + longitude;
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String ShareSub = modelItem.getTopicTH();
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(sharingIntent, ShareSub));
        }

    }

    public  void onClickLinearLayoutCamera(View view){
        Log.e(TAG,": ---> Click Camera");
        if (ContextCompat.checkSelfPermission(ItemsDetailActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(ItemsDetailActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

            startActivity(intentCameraActivity);

        }else {
            ActivityCompat.requestPermissions(ItemsDetailActivity.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSION_REQ);
        }
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
    public void SetNameMenu(){
        String[] NameMenuCategory;
        Resources res = getResources();
        NameMenuCategory = res.getStringArray(R.array.NameCategory);
        for(int i=0;i<NameMenuCategory.length;i++) {
            TextView_Array[i].setText(NameMenuCategory[i]);
        }
    }
    public void ClickNavigation(View view){
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        switch (view.getId()){
            case R.id.menu_category_1:
                intent.putExtra("retrunMain2fragment",0);
                break;
            case R.id.menu_category_2:
                intent.putExtra("retrunMain2fragment",1);
                break;
            case R.id.menu_category_3:
                intent.putExtra("retrunMain2fragment",2);
                break;
            case R.id.menu_category_4:
                intent.putExtra("retrunMain2fragment",3);
                break;
            case R.id.menu_category_5:
                intent.putExtra("retrunMain2fragment",4);
                break;
            case R.id.menu_category_6:
                intent.putExtra("retrunMain2fragment",5);
                break;
            case R.id.menu_category_7:
                intent.putExtra("retrunMain2fragment",6);
                break;
            case R.id.menu_category_8:
                intent.putExtra("retrunMain2fragment",7);
                break;
        }

        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                ItemhasBack = data.getParcelableArrayListExtra("ItemhasBack");
                icon_favorite.setColorFilter(null);
                boolean hasBackPressed = data.getBooleanExtra("currentItem",false);

                if (hasBackPressed){
                    icon_favorite.setColorFilter(getResources().getColor(R.color.red));
                }
                else
                {
                    icon_favorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelFilePaths> modelFilePaths) {
        if (modelFilePaths != null && modelFilePaths.size() > 0){
            Log.i("Check data",""+modelFilePaths.size());
            AdapterViewPagerItmes adapterViewPagerItmes = new AdapterViewPagerItmes(this,modelFilePaths);
            viewPager.setAdapter(adapterViewPagerItmes);
        }

    }

    @Override
    public void onRequestFailed(String result) {

    }
}
