package com.ipanda.lanexangtourism;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskSearch;
import com.ipanda.lanexangtourism.adapter.AdapterSearch;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceSearch;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.util.AddFavorite;
import com.ipanda.lanexangtourism.model.util.ItemClickFavorite;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements CallBackServiceSearch, AdapterView.OnItemClickListener, NavigationView.OnNavigationItemSelectedListener, ItemClickFavorite {

    private String url_json = "";
    private ArrayList<ModelItem> modelItems = null;
    private AdapterSearch adapter;
    private ProgressDialog alertDialog = null;

    private LayoutInflater layoutinflater;
    private ViewGroup header;
    private ListView listView;

    private TextView[] TextView_Array;
    private LinearLayout[] ClickNavigation_Array;
    private int numMenu = 8;

    private DatabaseHelper myDB;
    private String app_mode;
    private String typeItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.toolbar_search);
        TextView text_toolbar = findViewById(R.id.toolbar_title_contract);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        text_toolbar.setText(getIntent().getStringExtra("NameMenuCategory"));
        myDB = new DatabaseHelper(this);
        app_mode = this.getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");


        if (app_mode.equals("offline")){
            Toast.makeText(this, "Offline", Toast.LENGTH_SHORT).show();
            if (getIntent().getBooleanExtra("AsyncTask",true)){
                modelItems = myDB.getModelTypeOfItem(getIntent().getStringExtra("ConditionSQL"));
                onSetAdapter();
                adapter.notifyDataSetChanged();
            }else{
                modelItems = getIntent().getParcelableArrayListExtra("ItemEmergency");
                onSetAdapter();
                adapter.notifyDataSetChanged();
            }
        }else {
            if(getIntent().getBooleanExtra("AsyncTask",true)) {
                url_json = getIntent().getStringExtra("URL_JSON");
                typeItem = "Item";
                new AsyncTaskSearch(this).execute(url_json);
            }
            else {

                modelItems = getIntent().getParcelableArrayListExtra("ItemEmergency");
                typeItem = "Emergency";
                onSetAdapter();
                adapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("hasBackPressed",true);
        setResult(Activity.RESULT_OK,returnIntent);

        finish();
    }
    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelItem> modelItems) {
        if (modelItems != null && modelItems.size() > 0){
            Log.i("Check data", "" + modelItems.size());
            this.modelItems = modelItems;
            onSetAdapter();
            adapter.notifyDataSetChanged();
        }else if(modelItems != null){
        showDataNotFound();
    }
        this.dismissProgressDialog();
    }

    private void onSetAdapter() {
        adapter = new AdapterSearch(getBaseContext(), modelItems,this);
        listView = findViewById(R.id.list_item_search);

        listView.setAdapter(adapter);
        if(header == null)
        {
            layoutinflater = getLayoutInflater();
            header = (ViewGroup) layoutinflater.inflate(R.layout.title_list_filter,listView,false);
            ImageView ic = header.findViewById(R.id.img_menu_categoryID);
            TextView text = header.findViewById(R.id.text_title);
            TextView result = header.findViewById(R.id.text_result);
            ic.setImageDrawable(getDrawable(getIntent().getIntExtra("imageViewCategoryResID",0)));
            text.setText(getIntent().getStringExtra("NameMenuCategory"));
            String size = String.valueOf((listView.getAdapter().getCount()));
            result.setText(size +" "+getResources().getString(R.string.text_result));

            listView.addHeaderView(header);
        }




        listView.setOnItemClickListener(this);

        TextView_Array = new TextView[numMenu];
        ClickNavigation_Array = new LinearLayout[numMenu];
        for(int i=0; i<numMenu; i++) {

            String TextViewID = "TvCategory_" + (i + 1);
            int TextViewResID = getResources().getIdentifier(TextViewID, "id", getPackageName());

            String LinearLayoutID = "menu_category_" + (i + 1);
            int LinearLayoutResID = getResources().getIdentifier(LinearLayoutID, "id", getPackageName());

            TextView_Array[i] = (TextView) findViewById(TextViewResID);
            ClickNavigation_Array[i] = (LinearLayout) findViewById(LinearLayoutResID);

        }
        SetNameMenu();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ImageView menuRight = (ImageView) findViewById(R.id.menuRight);
        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });
    }


    @Override
    public void onCreateSupportNavigateUpTaskStack(@NonNull TaskStackBuilder builder) {
        super.onCreateSupportNavigateUpTaskStack(builder);
        Toast.makeText(this, builder+"xxxx", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage(""+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void showDataNotFound(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(" "+getResources().getString(R.string.text_data_not_found));
        builder.setMessage(""+getResources().getString(R.string.text_back_main));
        builder.setPositiveButton(""+getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, ItemsDetailActivity.class);
        if (position != 0) {
            intent.putExtra("ITEMS_DETAIL", modelItems.get((position - 1)));
            intent.putExtra("typeItem", typeItem);
            if(getIntent().getBooleanExtra("AsyncTask",true))
                startActivityForResult(intent,1);
            else
                startActivityForResult(intent,2);
        }else {

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                listView = null;
                url_json = getIntent().getStringExtra("URL_JSON");
                new AsyncTaskSearch(this).execute(url_json);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){

                ArrayList<ModelItem> ItemhasBack = new ArrayList<>();
                ItemhasBack = data.getParcelableArrayListExtra("ItemhasBack");

                modelItems = getIntent().getParcelableArrayListExtra("ItemEmergency");

                for(ModelItem item : ItemhasBack) {
                    if(item.getModelTypeOfItem().getTypeID() == 10 || item.getModelTypeOfItem().getTypeID() == 11 || item.getModelTypeOfItem().getTypeID() == 12 || item.getModelTypeOfItem().getTypeID() == 13)
                    {
                        for(ModelItem ItemEmergency : modelItems) {
                            if(item.getItemID().equals(ItemEmergency.getItemID()))
                                ItemEmergency.setFavorites(item.isFavorites());
                        }
                    }
                }
                onSetAdapter();
                adapter.notifyDataSetChanged();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
    public void SetNameMenu(){
        String[] NameMenuCategory;
        Resources res = getResources();
        NameMenuCategory = res.getStringArray(R.array.NameCategory);
        for(int i=0;i<NameMenuCategory.length;i++) {
            TextView_Array[i].setText(NameMenuCategory[i]);
        }
    }
    public void ClickNavigation(View view){
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        switch (view.getId()){
            case R.id.menu_category_1:
                intent.putExtra("retrunMain2fragment",0);
                break;
            case R.id.menu_category_2:
                intent.putExtra("retrunMain2fragment",1);
                break;
            case R.id.menu_category_3:
                intent.putExtra("retrunMain2fragment",2);
                break;
            case R.id.menu_category_4:
                intent.putExtra("retrunMain2fragment",3);
                break;
            case R.id.menu_category_5:
                intent.putExtra("retrunMain2fragment",4);
                break;
            case R.id.menu_category_6:
                intent.putExtra("retrunMain2fragment",5);
                break;
            case R.id.menu_category_7:
                intent.putExtra("retrunMain2fragment",6);
                break;
            case R.id.menu_category_8:
                intent.putExtra("retrunMain2fragment",7);
                break;
        }

        startActivity(intent);
        finish();
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void itemClicked(ModelItem item,ImageView imgFavorite) {
//        Toast.makeText(this, item.getTopicTH()+"", Toast.LENGTH_SHORT).show();

        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            if (getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken", "") != "") {
                imgFavorite.setColorFilter(null);
                if (item.isFavorites()){
                    imgFavorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                    item.setFavorites(false);
                }else {
                    imgFavorite.setColorFilter(getResources().getColor(R.color.red));
                    item.setFavorites(true);
                }

                String UserId = getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                String url_add_favorite = getResources().getString(R.string.host_ip) + "/lanexang_tourism/user/GetData/AddFavorite?typeItem=" + item.getModelTypeOfItem().getTypeID() + "&idUsers=" + UserId + "&idItems=" + item.getItemID() + "";
                new AddFavorite(this).execute(url_add_favorite);
            }else {
                Intent intent = new Intent(this, SocialLoginHelperActivity.class);
                startActivity(intent);
            }

        }
    }
}
