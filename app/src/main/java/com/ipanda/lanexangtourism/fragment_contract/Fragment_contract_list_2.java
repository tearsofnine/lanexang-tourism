package com.ipanda.lanexangtourism.fragment_contract;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskEmergency;
import com.ipanda.lanexangtourism.adapter.AdapterContract;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceEmergency;
import com.ipanda.lanexangtourism.model.ModelEmergency;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_contract_list_2 extends Fragment implements CallBackServiceEmergency, LocationListener {

    private ListView mListView;
    private String url_json = "/lanexang_tourism/user/GetData/getEmergency";
    private ArrayList<ModelEmergency> modelEmergency = null;
    private ViewGroup header;
    private DatabaseHelper myDB;
    private String app_mode;
    public Fragment_contract_list_2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDB = new DatabaseHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contract_list_2, container, false);


        mListView = (ListView) view.findViewById(R.id.listViewContract);
        header = (ViewGroup) inflater.inflate(R.layout.header_contract, mListView, false);

        app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            Toast.makeText(getContext(), "Offline", Toast.LENGTH_SHORT).show();
            modelEmergency = myDB.getModelEmergency(RetrunTitle(getArguments().getString("select"))[2]);
            onSetAdapter();
        }else {
            new AsyncTaskEmergency(this).execute(getResources().getString(R.string.host_ip)+url_json + "?idTypeOfEmergency=" + RetrunTitle(getArguments().getString("select"))[2]);
        }

        return view;
    }

    private String[] RetrunTitle(String select) {

        String[] string = new String[3];
        switch (select) {
            case "travel":
                string[0] = getString(R.string.tVtravel);
                string[1] = "0";
                string[2] = "1";
                break;
            case "emergency":
                string[0] = getString(R.string.tVemergency);
                string[1] = "1";
                string[2] = "2";
                break;
            case "hospital":
                string[0] = getString(R.string.tVhospital);
                string[1] = "2";
                string[2] = "3";
                break;
            case "inforcenter":
                string[0] = getString(R.string.tVtourist_infor_center);
                string[1] = "3";
                string[2] = "4";
                break;
        }
        return string;
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelEmergency> modelEmergency) {
        if (modelEmergency != null && modelEmergency.size() > 0) {
            Log.i("Check data", "" + modelEmergency.size());
            this.modelEmergency = modelEmergency;
            onSetAdapter();
        }
    }

    public void onSetAdapter(){
        Location locationResult;

        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            locationResult= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(locationResult == null)
                locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            AdapterContract aAdapter =  new AdapterContract(this.getActivity(),modelEmergency, getArguments().getString("select"),locationResult,app_mode);

            TextView textViewNameMenu = (TextView)header.findViewById(R.id.tVNameMenu_inlist);
            textViewNameMenu.setText(RetrunTitle(getArguments().getString("select"))[0]);

            mListView.addHeaderView(header);
            mListView.setAdapter(aAdapter);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
