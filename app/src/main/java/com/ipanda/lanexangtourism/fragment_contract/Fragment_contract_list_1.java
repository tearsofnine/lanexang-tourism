package com.ipanda.lanexangtourism.fragment_contract;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.SearchActivity;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskEmergencyTravel;
import com.ipanda.lanexangtourism.adapter.AdapterContractTravel;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceEmergencyTravel;
import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_contract_list_1 extends Fragment implements CallBackServiceEmergencyTravel, LocationListener {

    private ListView mListView;
    private ViewGroup header;
    private Location locationResult;
    private TextView textViewNameMenu;
    private AdapterContractTravel aAdapter;
    private String url_json = "/lanexang_tourism/user/GetData/getEmergencyTravel";
    private ArrayList<ModelTypeOfTravel> modelTypeOfTravel = null;
    private DatabaseHelper myDB;
    public Fragment_contract_list_1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDB = new DatabaseHelper(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contract_list_1, container, false);

        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            locationResult = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationResult == null)
                locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        mListView = (ListView) view.findViewById(R.id.listViewContract);
        header = (ViewGroup)inflater.inflate(R.layout.header_contract,mListView,false);
        textViewNameMenu = header.findViewById(R.id.tVNameMenu_inlist);
        textViewNameMenu.setText(getString(R.string.tVtravel));
        mListView.addHeaderView(header);

        String app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            Toast.makeText(getContext(), "Offline", Toast.LENGTH_SHORT).show();
            modelTypeOfTravel = myDB.getModelTypeOfTravel();
            onSetAdapter();
        }else {
            String UserId = getActivity().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
            new AsyncTaskEmergencyTravel(this).execute(getResources().getString(R.string.host_ip)+url_json+"?"+"&Lat="+locationResult.getLatitude()+"&Lng="+locationResult.getLongitude()+"&UserId="+UserId);

        }

        return view;
    }

    @Override
    public void onPreCallService() {

    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelTypeOfTravel> modelTypeOfTravel) {

        if (modelTypeOfTravel != null && modelTypeOfTravel.size() > 0) {
            Log.i("Check data", "" + modelTypeOfTravel.size());
            this.modelTypeOfTravel = modelTypeOfTravel;
            onSetAdapter();

        }
    }

    private void onSetAdapter() {


        aAdapter =  new AdapterContractTravel(this.getActivity(),modelTypeOfTravel);

        mListView.setAdapter(aAdapter);

        aAdapter.notifyDataSetChanged();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), SearchActivity.class);
                intent.putExtra("AsyncTask",false);
                if(position != 0) {
                    switch (getContext().getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                        case "th":
                            intent.putExtra("NameMenuCategory", modelTypeOfTravel.get(position - 1).getTypeTH());
                            break;
                        case "en":
                            if (modelTypeOfTravel.get(position - 1).getTypeEN().equals("") || modelTypeOfTravel.get(position - 1).getTypeEN() == null || modelTypeOfTravel.get(position - 1).getTypeEN().equals("null")) {
                                intent.putExtra("NameMenuCategory", modelTypeOfTravel.get(position - 1).getTypeTH());
                            } else {
                                intent.putExtra("NameMenuCategory", modelTypeOfTravel.get(position - 1).getTypeEN());
                            }
                            break;
                        case "lo":
                            if (modelTypeOfTravel.get(position - 1).getTypeLAO().equals("") || modelTypeOfTravel.get(position - 1).getTypeLAO() == null || modelTypeOfTravel.get(position - 1).getTypeLAO().equals("null")) {
                                intent.putExtra("NameMenuCategory", modelTypeOfTravel.get(position - 1).getTypeTH());
                            } else {
                                intent.putExtra("NameMenuCategory", modelTypeOfTravel.get(position - 1).getTypeLAO());
                            }
                            break;
                    }
                }


                intent.putParcelableArrayListExtra("ItemEmergency",modelTypeOfTravel.get(position-1).getModelEmergency());

                int imageViewCategoryResID = getContext().getResources().getIdentifier("typeitem_1"+position, "drawable", getContext().getPackageName());
                intent.putExtra("imageViewCategoryResID",imageViewCategoryResID);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                mListView.removeAllViewsInLayout();
                String UserId = getActivity().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
                new AsyncTaskEmergencyTravel(this).execute(getResources().getString(R.string.host_ip)+url_json+"?"+"&Lat="+locationResult.getLatitude()+"&Lng="+locationResult.getLongitude()+"&UserId="+UserId);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
