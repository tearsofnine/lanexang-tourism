package com.ipanda.lanexangtourism.abstract_asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.interface_callback.CallBackServiceEmergencyTravel;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfTravel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskEmergencyTravel extends AsyncTask<String,Integer,String> {

    private ArrayList<ModelTypeOfTravel> arrayLists = new ArrayList<ModelTypeOfTravel>();
    private CallBackServiceEmergencyTravel callBackService;

    public AsyncTaskEmergencyTravel(CallBackServiceEmergencyTravel callBackService) {
        this.callBackService = callBackService;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelTypeOfTravel> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelTypeOfTravel> arrayList = new ArrayList<>();


        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsontypeoftravel = jsonObject.optJSONArray("typeoftravel");
            JSONArray jsonEmergencyTravel = jsonObject.optJSONArray("EmergencyTravel");

            if (jsontypeoftravel != null){
                for(int i=0;i < jsontypeoftravel.length();i++) {
                    Log.e("Message", "loop");
                    ModelTypeOfTravel modelTypeOfTravel = new ModelTypeOfTravel();
                    ArrayList<ModelItem> list_emergency = new ArrayList<>();

                    JSONObject js_typeoftravel = jsontypeoftravel.optJSONObject(i);
                    modelTypeOfTravel.setIdTypeOfTravel(js_typeoftravel.getInt("idTypeOfTravel"));
                    modelTypeOfTravel.setTypeTH(js_typeoftravel.getString("type_th"));
                    modelTypeOfTravel.setTypeEN(js_typeoftravel.getString("type_en"));
                    modelTypeOfTravel.setTypeLAO(js_typeoftravel.getString("type_lao"));

                    for (int j = 0; j < jsonEmergencyTravel.length() ; j++){
                        ModelItem modelEmergency = new ModelItem();
                        ModelFilePaths paths = new ModelFilePaths();
                        ModelTypeOfItem modelTypeOfItem = new ModelTypeOfItem();
                        JSONObject js_item_emergency = jsonEmergencyTravel.optJSONObject(j);
                        if (js_typeoftravel.getInt("idTypeOfTravel") == js_item_emergency.getInt("idTypeOfTravel")) {

                            modelEmergency.setItemID(String.valueOf(js_item_emergency.getInt("idEmergency")));
                            modelEmergency.setTopicTH(js_item_emergency.getString("topic_th"));
                            modelEmergency.setTopicEN(js_item_emergency.getString("topic_en"));
                            modelEmergency.setTopicLAO(js_item_emergency.getString("topic_lao"));
                            modelEmergency.setAddressTH(js_item_emergency.getString("address_th"));
                            modelEmergency.setAddressEN(js_item_emergency.getString("address_en"));
                            modelEmergency.setAddressLAO(js_item_emergency.getString("address_lao"));
                            modelEmergency.setDetailTH(js_item_emergency.getString("details_th"));
                            modelEmergency.setDetailEN(js_item_emergency.getString("details_en"));
                            modelEmergency.setDetailLAO(js_item_emergency.getString("details_lao"));
                            modelEmergency.setDateOpen(js_item_emergency.getString("dateOpen"));
                            modelEmergency.setDateClose(js_item_emergency.getString("dateClose"));
                            modelEmergency.setTel(js_item_emergency.getString("tel"));
                            modelEmergency.setTimeStart(js_item_emergency.getString("timeStart"));
                            modelEmergency.setTimeEnd(js_item_emergency.getString("timeEnd"));
                            modelEmergency.setLatitude(js_item_emergency.getDouble("latitude"));
                            modelEmergency.setLongitude(js_item_emergency.getDouble("longitude"));
                            modelEmergency.setDistance(js_item_emergency.getString("distance"));
                            modelEmergency.setDistance_metr(js_item_emergency.getInt("distance_metr"));
                            modelTypeOfItem.setTypeID(js_item_emergency.getInt("idTypeOfTravel")+10);
                            modelEmergency.setFavorites(js_item_emergency.getBoolean("favorites"));
                            modelEmergency.setModelTypeOfItem(modelTypeOfItem);
                            paths.setPathImg(js_item_emergency.getString("path"));
                            modelEmergency.setModelFilePaths(paths);
                            modelEmergency.setArLink(js_item_emergency.getString("arLink"));
                            list_emergency.add(modelEmergency);
                        }
                    }
                    modelTypeOfTravel.setModelEmergency(list_emergency);
                    arrayList.add(modelTypeOfTravel);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return arrayList;
    }
}
