package com.ipanda.lanexangtourism.abstract_asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.interface_callback.CallBackServiceSearch;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskSearch extends AsyncTask<String,Integer,String> {

    private ArrayList<ModelItem> arrayLists = new ArrayList<ModelItem>();
    CallBackServiceSearch callBackService;

    public AsyncTaskSearch(CallBackServiceSearch callBackService){
        this.callBackService = callBackService;

    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelItem> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelItem> arrayList = new ArrayList<>();
        try {

            JSONArray jsonArray = new JSONArray(dataJSon);

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");

                    ModelItem items = new ModelItem();
                    ModelTypeOfItem typeOfItem = new ModelTypeOfItem();
                    ModelFilePaths paths = new ModelFilePaths();
                    JSONObject json = jsonArray.optJSONObject(i);

                    items.setItemID(json.getString("idItems"));
                    typeOfItem.setTypeID(Integer.parseInt(json.getString("idTypeOfItems")));
                    items.setModelTypeOfItem(typeOfItem);
                    items.setTopicTH(json.getString("topic_th"));
                    items.setTopicEN(json.getString("topic_en"));
                    items.setTopicLAO(json.getString("topic_lao"));
                    items.setAddressTH(json.getString("address_th"));
                    items.setAddressEN(json.getString("address_en"));
                    items.setAddressLAO(json.getString("address_lao"));
                    items.setDetailTH(json.getString("details_th"));
                    items.setDetailEN(json.getString("details_en"));
                    items.setDetailLAO(json.getString("details_lao"));
                    items.setDateOpen(json.getString("dateOpen"));
                    items.setDateClose(json.getString("dateClose"));
                    items.setTel(json.getString("tel"));
                    items.setTimeStart(json.getString("timeStart"));
                    items.setTimeEnd(json.getString("timeEnd"));
                    items.setDistance(json.getString("distance"));
                    items.setLatitude(json.getDouble("latitude"));
                    items.setLongitude(json.getDouble("longitude"));
                    items.setFavorites(json.getBoolean("favorites"));
                    paths.setPathImg(json.getString("path"));
                    items.setModelFilePaths(paths);
                    items.setArLink(json.getString("arLink"));
                    arrayList.add(items);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return arrayList;
    }


}
