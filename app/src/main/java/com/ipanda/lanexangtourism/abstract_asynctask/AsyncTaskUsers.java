package com.ipanda.lanexangtourism.abstract_asynctask;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.facebook.GraphRequest;
import com.ipanda.lanexangtourism.MainActivity;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceUsers;
import com.ipanda.lanexangtourism.model.ModelUsers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskUsers extends AsyncTask<String,Integer,String> {

    private ModelUsers modelUsers = new ModelUsers();
    CallBackServiceUsers callBackService;


    public AsyncTaskUsers(CallBackServiceUsers callBackService){
        this.callBackService = callBackService;

    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            modelUsers = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(modelUsers);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ModelUsers onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ModelUsers users = new ModelUsers();
        try {
            JSONObject jsonArray = new JSONObject(dataJSon);
            if (jsonArray != null){

                    users.setIdUsers(jsonArray.getString("idUsers"));
                    users.setEmail(jsonArray.getString("email"));
                    users.setToken(jsonArray.getString("token"));


            }

        } catch (JSONException e) {
            e.printStackTrace();

        }

        return users;
    }

}
