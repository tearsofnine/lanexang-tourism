package com.ipanda.lanexangtourism.abstract_asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceProvinces;
import com.ipanda.lanexangtourism.model.ModelDistrict;
import com.ipanda.lanexangtourism.model.ModelProvinces;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskProvinces extends AsyncTask<String,Integer,String> {

    private ArrayList<ModelProvinces> arrayLists = new ArrayList<>();
    private CallBackServiceProvinces callBackService;
    private Context context;
    public AsyncTaskProvinces(CallBackServiceProvinces callBackService, Context context) {
        this.callBackService = callBackService;
        this.context = context;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelProvinces> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelProvinces> arrayList = new ArrayList<>();
        ModelProvinces p = new ModelProvinces(context);
        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                p.setNameTH(context.getResources().getString(R.string.textView12));
                break;
            case "en":
                p.setNameEN(context.getResources().getString(R.string.textView12));
                break;
            case "lo":
                p.setNameLAO(context.getResources().getString(R.string.textView12));
                break;
        }

        ArrayList<ModelDistrict> ld = new ArrayList<>();
        ModelDistrict d = new ModelDistrict(context);
        switch (context.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                d.setNameTH(context.getResources().getString(R.string.textView13));
                break;
            case "en":
                d.setNameEN(context.getResources().getString(R.string.textView13));
                break;
            case "lo":
                d.setNameLAO(context.getResources().getString(R.string.textView13));
                break;
        }
        ld.add(d);
        p.setModelDistricts(ld);
        arrayList.add(p);

        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonArray = jsonObject.optJSONArray("Provinces");
            JSONArray jsonDistrict = jsonObject.optJSONArray("District");

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");
                    ModelProvinces provinces = new ModelProvinces(context);
                    ArrayList<ModelDistrict> list_District = new ArrayList<>();
                    JSONObject js_provinces = jsonArray.optJSONObject(i);
                    provinces.setId(js_provinces.getString("idProvinces"));
                    provinces.setNameTH(js_provinces.getString("name_th"));
                    provinces.setNameEN(js_provinces.getString("name_en"));
                    provinces.setNameLAO(js_provinces.getString("name_lao"));

                    for (int j = 0; j < jsonDistrict.length() ; j++){
                        ModelDistrict district = new ModelDistrict(context);
                        JSONObject js_district = jsonDistrict.optJSONObject(j);
                        if (js_provinces.getString("idProvinces").equals(js_district.getString("idProvinces"))) {
                            district.setId(js_district.getString("idDistrict"));
                            district.setNameTH(js_district.getString("name_th"));
                            district.setNameEN(js_district.getString("name_en"));
                            district.setNameLAO(js_district.getString("name_lao"));
                            list_District.add(district);
                        }

                    }
                    provinces.setModelDistricts(list_District);
                    arrayList.add(provinces);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return arrayList;
    }
}
