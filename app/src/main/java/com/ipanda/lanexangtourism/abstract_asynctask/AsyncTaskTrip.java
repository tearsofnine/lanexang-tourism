package com.ipanda.lanexangtourism.abstract_asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.interface_callback.CallBackServiceTrip;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTrip;
import com.ipanda.lanexangtourism.model.ModelTypeOfTrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskTrip extends AsyncTask<String,Integer,String> {

    private ArrayList<ModelTrip> arrayLists = new ArrayList<ModelTrip>();
    CallBackServiceTrip callBackService;
    private int json_position = 0;
    private int json_KeyValue = 0;

    public AsyncTaskTrip(CallBackServiceTrip callBackService,int json_position ){
        this.callBackService = callBackService;
        this.json_position = json_position;
    }


    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelTrip> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelTrip> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonArray = jsonObject.optJSONArray("ProgramTour").getJSONObject(json_position).getJSONArray("Trip");

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
                    Log.e("Message", "loop");
                    ModelTrip model = new ModelTrip();
                    ModelTypeOfTrip type = new ModelTypeOfTrip();
                    ModelItem item = new ModelItem();
                    JSONObject json = jsonArray.optJSONObject(i);
                        model.setTripID(json.getInt("idTrip"));
                        model.setTripTH(json.getString("trip_th"));
                        model.setTripEN(json.getString("trip_en"));
                        model.setTripLAO(json.getString("trip_lao"));
                        model.setPathImg(json.getString("path"));
                        model.setAreaTH(json.getString("areaTH"));
                        model.setAreaEN(json.getString("areaEN"));
                        model.setAreaLAO(json.getString("areaLAO"));
                        model.setKey(json.getInt("key"));
                        type.setTypeTH(json.getString("types_th"));
                        type.setTypeEN(json.getString("types_en"));
                        type.setTypeLAO(json.getString("types_lao"));
                        model.setModelTypeOfTrip(type);

                    arrayList.add(model);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return arrayList;
    }

}
