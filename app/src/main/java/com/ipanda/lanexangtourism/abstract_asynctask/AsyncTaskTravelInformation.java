package com.ipanda.lanexangtourism.abstract_asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.interface_callback.CallBackServiceTravelInformation;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelTravelInformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskTravelInformation extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelTravelInformation> arrayLists = new ArrayList<ModelTravelInformation>();
    CallBackServiceTravelInformation callBackService;

    public AsyncTaskTravelInformation(CallBackServiceTravelInformation callBackService){
        this.callBackService = callBackService;

    }

    @Override
    protected String doInBackground(String... params) {
        try {
//            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelTravelInformation> onParserContentToModel(String dataJSon) {
//        Log.e("data json", dataJSon);
        ArrayList<ModelTravelInformation> arrayList = new ArrayList<>();
        try {

            JSONArray jsonArray = new JSONArray(dataJSon);

            if (jsonArray != null){
                for(int i=0;i < jsonArray.length();i++) {
//                    Log.e("Message", "loop");

                    ModelTravelInformation items = new ModelTravelInformation();
                    ModelFilePaths paths = new ModelFilePaths();
                    JSONObject json = jsonArray.optJSONObject(i);

                    items.setIdTravelInformation(json.getString("idTravelInformation"));
                    items.setTopic_th(json.getString("topic_th"));
                    items.setTopic_en(json.getString("topic_en"));
                    items.setTopic_lao(json.getString("topic_lao"));

                    items.setHistory_th(json.getString("history_th"));
                    items.setGovernment_th(json.getString("history_th"));
                    items.setLanguage_th(json.getString("language_th"));
                    items.setPopulation_th(json.getString("population_th"));
                    items.setReligion_th(json.getString("religion_th"));

                    items.setHistory_en(json.getString("history_en"));
                    items.setGovernment_en(json.getString("history_en"));
                    items.setLanguage_en(json.getString("language_en"));
                    items.setPopulation_en(json.getString("population_en"));
                    items.setReligion_en(json.getString("religion_en"));

                    items.setHistory_lao(json.getString("history_lao"));
                    items.setGovernment_lao(json.getString("history_lao"));
                    items.setLanguage_lao(json.getString("language_lao"));
                    items.setPopulation_lao(json.getString("population_lao"));
                    items.setReligion_lao(json.getString("religion_lao"));

                    paths.setPathImg(json.getString("path"));
                    items.setModelFilePaths(paths);

                    arrayList.add(items);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
        return arrayList;
    }

}
