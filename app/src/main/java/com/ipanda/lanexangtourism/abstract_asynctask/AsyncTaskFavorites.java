package com.ipanda.lanexangtourism.abstract_asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.ipanda.lanexangtourism.interface_callback.CallBackServiceFavorites;
import com.ipanda.lanexangtourism.model.ModelFilePaths;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.ModelUsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class AsyncTaskFavorites extends AsyncTask<String,Integer,String> {
    private ArrayList<ModelItem> arrayLists = new ArrayList<ModelItem>();
    CallBackServiceFavorites callBackService;

    public AsyncTaskFavorites(CallBackServiceFavorites callBackService){
        this.callBackService = callBackService;

    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Log.i(TAG,"Call Service");
            if(callBackService != null){
                callBackService.onCallService();
            }
            return downloadContent(params[0]);
        } catch (IOException e) {
            return null;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (callBackService != null) {
            callBackService.onPreCallService();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callBackService != null) {
            arrayLists = onParserContentToModel(s);
            callBackService.onRequestCompleteListener(arrayLists);
        }
    }

    private String downloadContent(String myUrl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myUrl);
            Log.e("url", myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is : " + response);
            Log.e("response", "" + response);
            is = conn.getInputStream();
            Log.e("is", is.toString());
            String result = convertInputStreamToString(is);
            return result;
        }catch(Exception e) {
            Log.e("error exp", e.getMessage());
            return "error";
        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            sb.append(line + "\n");
        }
        br.close();
        return sb.toString();
    }

    public ArrayList<ModelItem> onParserContentToModel(String dataJSon) {
        Log.e("data json", dataJSon);
        ArrayList<ModelItem> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(dataJSon);
            JSONArray jsonFavorite = jsonObject.optJSONArray("dataFavorite");
            JSONArray jsonUsersMatching = jsonObject.optJSONArray("UsersMatching");
//            JSONArray jsonTypeOfItems = jsonObject.optJSONArray("TypeOfItems");

            if (jsonFavorite != null){
//                for(int i=0;i < jsonTypeOfItems.length();i++) {
//                    Log.e("Message", "loop");
//                    JSONObject json = jsonTypeOfItems.optJSONObject(i);
//                    ModelTypeOfItem typeOfItem = new ModelTypeOfItem();
//                    ArrayList<ModelItem> list_Item = new ArrayList<>();
//                    typeOfItem.setTypeID(Integer.parseInt(json.getString("idTypeOfItems")));
//                    typeOfItem.setTypeTH(json.getString("type_th"));
//                    typeOfItem.setTypeEN(json.getString("type_en"));
//                    typeOfItem.setTypeLAO(json.getString("type_lao"));

                    for (int j = 0; j < jsonFavorite.length(); j++) {
                        ModelItem items = new ModelItem();
                        ModelTypeOfItem type = new ModelTypeOfItem();
                        ModelFilePaths paths = new ModelFilePaths();
                        ModelTypeOfItem typeOfItem = new ModelTypeOfItem();
                        JSONObject json_Favorite = jsonFavorite.optJSONObject(j);
//                        if (json_Favorite.getString("idTypeOfItems").equals(json.getString("idTypeOfItems"))) {
                            type.setTypeID(Integer.parseInt(json_Favorite.getString("idTypeOfItems")));
                            items.setModelTypeOfItem(type);
                            items.setItemID(json_Favorite.getString("idItems"));
                            typeOfItem.setTypeID(Integer.parseInt(json_Favorite.getString("idTypeOfItems")));


                            ArrayList<ModelUsers> array_user = new ArrayList<>();
                                if (jsonUsersMatching != null){
                                    for (int k = 0; k < jsonUsersMatching.length(); k++){
                                        ModelUsers users = new ModelUsers();
                                        JSONObject json_Users = jsonUsersMatching.optJSONObject(k);
                                        if (json_Favorite.getString("idItems").equals(json_Users.getString("idItems"))){

                                            users.setPersonPhoto(json_Users.getString("UrlPhoto"));
                                            users.setEmail(json_Users.getString("email"));
                                            users.setName(json_Users.getString("name"));
                                            users.setLoginWith(json_Users.getString("TypeUserRegister"));
                                            array_user.add(users);

                                        }
                                    }
                                }

                            items.setModelUsers(array_user);
                            items.setTopicTH(json_Favorite.getString("topic_th"));
                            items.setTopicEN(json_Favorite.getString("topic_en"));
                            items.setTopicLAO(json_Favorite.getString("topic_lao"));
                            items.setAddressTH(json_Favorite.getString("address_th"));
                            items.setAddressEN(json_Favorite.getString("address_en"));
                            items.setAddressLAO(json_Favorite.getString("address_lao"));
                            items.setDetailTH(json_Favorite.getString("details_th"));
                            items.setDetailEN(json_Favorite.getString("details_en"));
                            items.setDetailLAO(json_Favorite.getString("details_lao"));
                            items.setDateOpen(json_Favorite.getString("dateOpen"));
                            items.setDateClose(json_Favorite.getString("dateClose"));
                            items.setTel(json_Favorite.getString("tel"));
                            items.setTimeStart(json_Favorite.getString("timeStart"));
                            items.setTimeEnd(json_Favorite.getString("timeEnd"));
                            items.setDistance(json_Favorite.getString("distance"));
                            items.setLatitude(json_Favorite.getDouble("latitude"));
                            items.setLongitude(json_Favorite.getDouble("longitude"));
                            items.setFavorites(json_Favorite.getBoolean("favorites"));
                            items.setPosition(json_Favorite.getString("Pos"));
                            paths.setPathImg(json_Favorite.getString("path"));
                            items.setModelFilePaths(paths);
                            items.setArLink(json_Favorite.getString("arLink"));
                            items.setFavCount(json_Favorite.getString("FavCount"));
                        arrayList.add(items);
                        }

                    }
//                    typeOfItem.setModelItems(list_Item);
//                }
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


}
