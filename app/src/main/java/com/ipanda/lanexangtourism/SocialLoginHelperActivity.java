package com.ipanda.lanexangtourism;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;

import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskUsers;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceUsers;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelUsers;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.ipanda.lanexangtourism.model.util.LineLogin;
import com.linecorp.linesdk.LineApiResponse;
import com.linecorp.linesdk.LoginDelegate;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.api.LineApiClient;
import com.linecorp.linesdk.api.LineApiClientBuilder;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class SocialLoginHelperActivity extends AppCompatActivity implements View.OnClickListener, CallBackServiceUsers {

    private static final String TAG = "SocialLoginHelperActivity";
    private static final int RC_SIGN_IN = 101;
    private static final int RC_SIGN_IN_FACEBOOK = 102;
    private static final int RC_SIGN_IN_LINE = 103;
    private GoogleSignInClient googleSignInClient;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private LoginDelegate loginDelegate;
    private static LineApiClient lineApiClient;
    private ModelUsers modelUsers = null;
    private String url_json = "";
    private ProgressDialog alertDialog = null;
    private ImageView sign_in_facebook_button,sign_in_line_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        showHashKey(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_social_login_helper);
        ImageView imageView_btnClose = findViewById(R.id.imageView_btnClose);
        sign_in_facebook_button = findViewById(R.id.sign_in_facebook_button);
        sign_in_facebook_button.setOnClickListener(this);
        sign_in_line_button = findViewById(R.id.sign_in_line_button);
        sign_in_line_button.setOnClickListener(this);

        //Login Google
        ImageView viewLoginGoogle = findViewById(R.id.sign_in_button);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("640642346948-q2nkb7189j5ovejqsqhtdonmfc9iopn4.apps.googleusercontent.com")
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        viewLoginGoogle.setOnClickListener(this);



        imageView_btnClose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        if(getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken","") != "") {
            if(getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserLoginType","").equals("gmail")) {
                signOutGoogle();
            }else if (getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserLoginType","").equals("facebook")){
                signOutFacebook();
            }else if (getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserLoginType","").equals("line")){
                signOutLine();
            }
        }
        LineApiClientBuilder apiClientBuilder = new LineApiClientBuilder(getApplicationContext(), LineLogin.CHANNEL_ID);
        lineApiClient = apiClientBuilder.build();
    }
    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.ipanda.lanexangtourism", PackageManager.GET_SIGNATURES); //Your      package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("KeyHash:", "error");
        } catch (NoSuchAlgorithmException e) {
            Log.i("KeyHash:", "error");
        }
    }
    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signInGoogle();
                break;
//            case R.id.sign_out_button:
//                signOutGoogle();
//                break;
            case R.id.sign_in_facebook_button:
                signInFacebook();
                break;

            case R.id.sign_in_line_button:
                signInLine();
                break;
        }
    }

    //Result Login Google
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else if (requestCode == RC_SIGN_IN_LINE){
            LineLoginResult result = LineLoginApi.getLoginResultFromIntent(data);
            try {
                if (result != null){
                    loadProfileLine(result);
                }
            }catch (Exception e){
                Log.v("LoginLine",e.toString());
            }

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    //Get Profile Google
    @SuppressLint("LongLogTag")
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                String idToken = account.getIdToken();
                String personEmail = account.getEmail();
                String personId = account.getId();
                Uri personPhoto = account.getPhotoUrl();
            }

            updateUI(account);
        } catch (ApiException e) {

            Log.e(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    //Login
    private void signInGoogle() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    //LogOut
    public void signOutGoogle() {
        googleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", "").apply();
                        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "").apply();
                        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserID", "").apply();
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        finish();

                    }
                });
    }
    //Intent to ..
    private void updateUI(GoogleSignInAccount googleSignInAccount) {
        String NameUser = googleSignInAccount.getDisplayName().replace(" ","||");
        String url = getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/RegisterLogin?idToken="+googleSignInAccount.getId()+"&email="+googleSignInAccount.getEmail()+"&name="+NameUser+"&UrlPhoto="+googleSignInAccount.getPhotoUrl()+"&TypeUserRegister=GOOGLE";
        new AsyncTaskUsers(SocialLoginHelperActivity.this).execute(url);
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", googleSignInAccount.getId()).apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "gmail").apply();


    }

    //Login with facebook
    private void signInFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email,public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Toast.makeText(SocialLoginHelperActivity.this, "onSuccess", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    Log.v("LoginActivity", "cancel");
                    Toast.makeText(SocialLoginHelperActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.v("LoginActivity", exception.toString());
                    Toast.makeText(SocialLoginHelperActivity.this, "FacebookException", Toast.LENGTH_SHORT).show();
                }
            });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken != null){
                    loadUserProfile(currentAccessToken);
                }else {

                }
            }
        };
    }



    private void loadUserProfile(AccessToken newAccessToken){
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    ModelUsers users = new ModelUsers();
                    users.setName(object.getString("first_name")+"||"+object.getString("last_name"));
                    users.setEmail(object.getString("email"));
                    String profile_id = object.getString("id");
                    String image_url = "https://graph.facebook.com/"+profile_id+"/picture?type=normal";
                    users.setToken(object.getString("id"));
                    users.setPersonPhoto(image_url);

                    upLoadProfileFaceBook(users);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void upLoadProfileFaceBook(ModelUsers users){
        url_json =  getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/RegisterLogin?idToken="+users.getToken()+"&email="+users.getEmail()+"&name="+users.getName()+"&UrlPhoto="+users.getPersonPhoto()+"&TypeUserRegister=FACEBOOK";
        new AsyncTaskUsers(SocialLoginHelperActivity.this).execute(url_json);
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", users.getToken()).apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "facebook").apply();



    }

    //LogOutFacebook
    public void signOutFacebook() {

        FacebookSdk.sdkInitialize(this);
        LoginManager.getInstance().logOut();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", "").apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "").apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserID", "").apply();
        Intent intent = new Intent(SocialLoginHelperActivity.this,MainActivity.class);
        startActivity(intent);
        finish();

    }

    //Login Line
    private void signInLine() {
        try {

            loginDelegate = LoginDelegate.Factory.create();
            Intent loginIntent = LineLoginApi.getLoginIntent(this,
                    LineLogin.CHANNEL_ID, new LineAuthenticationParams.Builder()
                            .scopes(Arrays.asList(Scope.PROFILE))
                            .build());
            startActivityForResult(loginIntent, RC_SIGN_IN_LINE);
        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        }
    }

    private void loadProfileLine(LineLoginResult result){
        switch (result.getResponseCode()) {
            case SUCCESS:
                // Login successful
                String accessToken = result.getLineCredential().getAccessToken().getTokenString();
//                Toast.makeText(this, accessToken+"", Toast.LENGTH_SHORT).show();
                ModelUsers users = new ModelUsers();
                users.setToken(result.getLineProfile().getUserId());
                users.setPersonPhoto(result.getLineProfile().getPictureUrl().toString());
                users.setName(result.getLineProfile().getDisplayName().replace(" ","||"));
                users.setEmail("");

                url_json =  getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/RegisterLogin?idToken="+users.getToken()+"&email="+users.getEmail()+"&name="+users.getName()+"&UrlPhoto="+users.getPersonPhoto()+"&TypeUserRegister=LINE";
                new AsyncTaskUsers(SocialLoginHelperActivity.this).execute(url_json);
                getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", users.getToken()).apply();
                getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "line").apply();

                break;
            case CANCEL:
                Log.e("ERROR", "LINE Login Canceled by user.");
                break;
            default:
                Log.e("ERROR", "Login FAILED!");
                Log.e("ERROR", result.getErrorData().toString());
        }
    }

    private void signOutLine(){

        new LogoutTask().execute();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", "").apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "").apply();
        getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserID", "").apply();

    }

    public class LogoutTask extends AsyncTask<Void, Void, LineApiResponse> {

        final static String TAG = "LogoutTask";

        @Override
        protected LineApiResponse doInBackground(Void... voids) {
            return lineApiClient.logout();
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(LineApiResponse lineApiResponse) {
            super.onPostExecute(lineApiResponse);
                        if(lineApiResponse.isSuccess()){
                Toast.makeText(getApplicationContext(), "Logout Successful", Toast.LENGTH_SHORT).show();
                            getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserIdToken", "").apply();
                            getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserLoginType", "").apply();
                            getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserID", "").apply();
                            Intent intent = new Intent(SocialLoginHelperActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();
            } else {
                Toast.makeText(getApplicationContext(), "Logout Failed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Logout Failed: " + lineApiResponse.getErrorData().toString());
            }
        }

    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ModelUsers modelUsers) {
        if (modelUsers != null ){
            Log.i("Check data", "" + modelUsers);
            this.modelUsers = modelUsers;
            getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).edit().putString("APP_UserID", modelUsers.getIdUsers()).apply();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestFailed(String result) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(this);
        alertDialog.setMessage(" "+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }
}
