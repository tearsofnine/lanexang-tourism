package com.ipanda.lanexangtourism;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ipanda.lanexangtourism.adapter.AdapterUserMatching;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelUsers;

import java.util.ArrayList;

public class UserMatchingActivity extends AppCompatActivity {

    private ArrayList<ModelUsers> modelUsers = null;
    private ModelItem modelItem = null;
    private AdapterUserMatching mAdapter;
    private RecyclerView recyclerView;
    private TextView toolbar_title,toolbar_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_matching);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = findViewById(R.id.RecyclerView_User);
        modelUsers = new ArrayList<>();
        modelItem = new ModelItem();
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_address = findViewById(R.id.toolbar_address);

        modelUsers = this.getIntent().getParcelableArrayListExtra("MODEL_USERS");
        modelItem = this.getIntent().getParcelableExtra("MODEL_ITEM");

        switch (this.getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
            case "th":
                toolbar_title.setText(modelItem.getTopicTH());
                toolbar_address.setText(modelItem.getAddressTH());
                break;
            case "en":
                if (modelItem.getTopicEN().equals("") || modelItem.getTopicEN() == null || modelItem.getTopicEN().equals("null")) {
                    toolbar_title.setText(modelItem.getTopicTH());
                    toolbar_address.setText(modelItem.getAddressTH());
                } else {
                    toolbar_title.setText(modelItem.getTopicEN());
                    toolbar_address.setText(modelItem.getAddressTH());
                }
                break;
            case "lo":
                if (modelItem.getTopicLAO().equals("") || modelItem.getTopicLAO() == null || modelItem.getTopicLAO().equals("null")) {
                    toolbar_title.setText(modelItem.getTopicTH());
                    toolbar_address.setText(modelItem.getAddressTH());
                } else {
                    toolbar_title.setText(modelItem.getTopicLAO());
                    toolbar_address.setText(modelItem.getAddressLAO());
                }
                break;
        }


        setAdapter();
    }

    private void setAdapter() {
        mAdapter = new AdapterUserMatching(modelUsers,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
