package com.ipanda.lanexangtourism.fragment_menu;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.ipanda.lanexangtourism.GoogleMapActivity;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.SearchActivity;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskProvinces;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceProvinces;
import com.ipanda.lanexangtourism.model.ModelDistrict;
import com.ipanda.lanexangtourism.model.ModelProvinces;
import com.ipanda.lanexangtourism.other.SetFragmentMenu;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_category_2 extends Fragment implements View.OnClickListener , CallBackServiceProvinces {


    private static String TAG = "Fragment_menu_category_2";
    private String[] NameMenuCategory;
    private Button buttonSearching,btnOnClickSearching;
    private Intent intent;
    private EditText edit_Search;
    private MapView mapView;
    private Activity mActivity;
    private ProgressDialog alertDialog = null;
    private String url_json = "/lanexang_tourism/user/GetData/getArea";
    private ArrayList<ModelProvinces> modelProvinces = null;
    private Spinner spinner_Provinces, spinner_District;
    private String app_mode;

    private String search_EditText , search_Provinces , search_District ;

    private String Lat = "",Lng = "";
    public Fragment_menu_category_2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState != null){

        }else {
            NameMenuCategory = getResources().getStringArray(R.array.NameCategory);
            mActivity = this.getActivity();

            new AsyncTaskProvinces(this, getApplicationContext()).execute(getResources().getString(R.string.host_ip) + url_json);

            spinner_Provinces = new Spinner(getApplicationContext());
            spinner_District = new Spinner(getApplicationContext());

            app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE", "");
            if (app_mode.equals("offline")) {
                alertDialogCheckOffLineMode();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_category_2,container, false);

        spinner_Provinces = view.findViewById(R.id.spinner_province);
        spinner_District = view.findViewById(R.id.spinner_district);
        edit_Search = view.findViewById(R.id.editText_Search);

        buttonSearching = view.findViewById(R.id.button_searching_filter);


        btnOnClickSearching = view.findViewById(R.id.button_searching);
        btnOnClickSearching.setOnClickListener(this);


        SetFragmentMenu setFragmentMenu = new SetFragmentMenu();
        mapView = setFragmentMenu.SetLayout(view,this.getActivity(),savedInstanceState,NameMenuCategory,2,mActivity);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                 Lat = String.valueOf(googleMap.getCameraPosition().target.latitude);
                 Lng = String.valueOf(googleMap.getCameraPosition().target.longitude);
            }
        });


        buttonSearching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (app_mode.equals("offline")){
                    alertDialogCheckOffLineMode();
                }else {
                    boolean result = isNetworkConnected();
                    if (result){
                        Intent intent = new Intent(getActivity(), GoogleMapActivity.class);
                        intent.putExtra("itemType", 1);
                        intent.putExtra("Lat", Lat);
                        intent.putExtra("Lng", Lng);

                        startActivity(intent);
                    }else {
                        alertDialogConnectInternet();
                    }
                }
            }
        });

        if (app_mode.equals("offline")){
            edit_Search.setEnabled(false);
            spinner_Provinces.setEnabled(false);
            spinner_District.setEnabled(false);
        }

        return view;
    }




    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onPause() {
       mapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_searching:
                onSearching();
                break;
        }
    }

    private void onSearching() {
        search_EditText = edit_Search.getText().toString();
        Intent intent = new Intent(getContext(), SearchActivity.class);
        String UserId = getActivity().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
        if (search_Provinces != null){
            intent.putExtra("URL_JSON",getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getDataItme?idTypeOfItems=1|2|3|7&topicName="+search_EditText+"&idProvinces="+search_Provinces+"&idDistrict="+search_District+"&Lat="+Lat+"&Lng="+Lng+"&UserId="+UserId);
        }else {
            search_Provinces ="";
            search_District ="";
            intent.putExtra("URL_JSON",getResources().getString(R.string.host_ip)+"/lanexang_tourism/user/GetData/getDataItme?idTypeOfItems=1|2|3|7&topicName="+search_EditText+"&idProvinces="+search_Provinces+"&idDistrict="+search_District+"&Lat="+Lat+"&Lng="+Lng+"&UserId="+UserId);
        }

        String imageViewCategoryID = "category_" + 2 ;
        int imageViewCategoryResID = getContext().getResources().getIdentifier(imageViewCategoryID, "drawable", getContext().getPackageName());
        intent.putExtra("NameMenuCategory",NameMenuCategory[1]);
        intent.putExtra("imageViewCategoryResID",imageViewCategoryResID);
        intent.putExtra("ConditionSQL","idTypeOfItems = 1 or idTypeOfItems = 2 or idTypeOfItems = 3 or idTypeOfItems = 7");


        startActivity(intent);
    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelProvinces> modelProvinces) {
        if (modelProvinces != null && modelProvinces.size() > 0) {
            Log.i("Check data", "" + modelProvinces.size());
            this.modelProvinces = modelProvinces;
//            onSetSpinnerProvinces();
            onSetSpinner();

        }
        this.dismissProgressDialog();
    }

    private void onSetSpinner() {

        ArrayAdapter<ModelProvinces> adapter = new ArrayAdapter<ModelProvinces>(getApplicationContext(), android.R.layout.simple_spinner_item, modelProvinces);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Provinces.setAdapter(adapter);
        spinner_Provinces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                search_Provinces = modelProvinces.get(position).getId();
                onSelectedProvinces(position);
            }

            private void onSelectedProvinces(int i) {
                ArrayAdapter<ModelDistrict> adapter = new ArrayAdapter<ModelDistrict>(getApplicationContext(), android.R.layout.simple_spinner_item, modelProvinces.get(i).getModelDistricts());

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_District.setAdapter(adapter);
                spinner_District.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        search_District = modelProvinces.get(i).getModelDistricts().get(position).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @Override
    public void onRequestFailed(String result) {

    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage(""+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        try {

            outState.putStringArray("NameMenuCategory",NameMenuCategory);

            if (0 < getFragmentManager().getBackStackEntryCount()) {
                getFragmentManager().putFragment(outState, "mContent", this);
            }
        } catch (Exception ex) {
            Log.i(TAG,ex.toString());
        }
    }
}
