package com.ipanda.lanexangtourism.fragment_menu;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.TripActivity;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskProgramTour;
import com.ipanda.lanexangtourism.adapter.AdapterProgramTour;
import com.ipanda.lanexangtourism.connect.DatabaseHelper;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceProgramTour;

import com.ipanda.lanexangtourism.model.ModelProgramTour;


import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Fragment_menu_category_1 extends Fragment implements CallBackServiceProgramTour {

    private static String TAG ="Fragment_menu_category_1";
    private String url_json = "/lanexang_tourism/user/GetData/GetProgramTour";
    private ArrayList<ModelProgramTour> programToursArrayList = null;
    private AdapterProgramTour adapter;
    private ProgressDialog alertDialog = null;
    private ListView listView;
    private Intent intent;
    private DatabaseHelper myDB;
    private Context context;

    public Fragment_menu_category_1() {
        // Required empty public constructor
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (savedInstanceState == null) {
            programToursArrayList = new ArrayList<>();
            myDB = new DatabaseHelper(getContext());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_category_1, container, false);

        listView =  view.findViewById(R.id.list_program_tour);
        ImageView imageView = view.findViewById(R.id.img_menu_categoryID);
        TextView textView = view.findViewById(R.id.text_titleID);
//

        imageView.setImageResource(R.drawable.category_1);
        textView.setText("LANEXANG INFO");
        String UserId = getActivity().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
        String app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
            Toast.makeText(getContext(), "Offline", Toast.LENGTH_SHORT).show();
            programToursArrayList = myDB.getModelProgramTour();
            onSetAdapter();
        }else {
            boolean result = isNetworkConnected();
            if (result){
                new AsyncTaskProgramTour(this).execute(getResources().getString(R.string.host_ip)+url_json+"?UserId="+UserId);
            }else {
                alertDialogConnectInternet();
            }
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null){
            programToursArrayList = savedInstanceState.getParcelableArrayList("GetProgramTours");
            url_json = savedInstanceState.getString("JSON");
        }
    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelProgramTour> programToursArrayList) {
        if (programToursArrayList != null && programToursArrayList.size() > 0) {
            Log.i("Check data", "" + programToursArrayList.size());
            this.programToursArrayList = programToursArrayList;
            onSetAdapter();
            adapter.notifyDataSetChanged();

        }
        this.dismissProgressDialog();
    }

    private void onSetAdapter() {
        adapter = new AdapterProgramTour(getApplicationContext(),programToursArrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intent = new Intent(getContext(),TripActivity.class);
                intent.putExtra("JSON_POSITION",position);
                switch (getActivity().getSharedPreferences("PREF_SET_LANGUAGE", Context.MODE_PRIVATE).getString("LANGUAGE", "th")) {
                    case "th":
                        intent.putExtra("ProgramTourName", programToursArrayList.get(position).getProgramTourTH());
                        break;
                    case "en":
                        intent.putExtra("ProgramTourName", programToursArrayList.get(position).getProgramTourEN());
                        break;
                    case "lo":
                        intent.putExtra("ProgramTourName", programToursArrayList.get(position).getProgramTourLAO());
                        break;
                }
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }

    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage(getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        try {
            outState.putParcelableArrayList("GetProgramTours",programToursArrayList);
            outState.putString("JSON",url_json);

            if (0 < getFragmentManager().getBackStackEntryCount()) {
                getFragmentManager().putFragment(outState, "mContent", this);
            }
        } catch (Exception ex) {
            Log.i(TAG,ex.toString());
        }
    }
}
