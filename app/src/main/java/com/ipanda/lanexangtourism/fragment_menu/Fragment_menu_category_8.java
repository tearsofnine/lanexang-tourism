package com.ipanda.lanexangtourism.fragment_menu;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipanda.lanexangtourism.ItemsDetailActivity;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.SocialLoginHelperActivity;
import com.ipanda.lanexangtourism.UserMatchingActivity;
import com.ipanda.lanexangtourism.abstract_asynctask.AsyncTaskFavorites;
import com.ipanda.lanexangtourism.interface_callback.CallBackServiceFavorites;
import com.ipanda.lanexangtourism.model.ModelItem;
import com.ipanda.lanexangtourism.model.ModelTypeOfItem;
import com.ipanda.lanexangtourism.model.util.AddFavorite;
import com.ipanda.lanexangtourism.model.util.ItemClickListener;
import com.ipanda.lanexangtourism.model.util.ItemMoveCallback;
import com.ipanda.lanexangtourism.model.util.RecyclerViewAdapter;
import com.ipanda.lanexangtourism.model.util.SectionedExpandableLayoutHelper;
import com.ipanda.lanexangtourism.model.util.StartDragListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_category_8 extends Fragment implements ItemClickListener, CallBackServiceFavorites, LocationListener , StartDragListener {

    private RecyclerView recyclerView;
    private ArrayList<ModelItem> modelItem = null;
    private ProgressDialog alertDialog = null;
    private RecyclerViewAdapter mAdapter;
    private ItemTouchHelper touchHelper;
    private String url_json = "";
    private TextView icon_favorite;

    protected LocationManager locationManager;

    public Fragment_menu_category_8() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            if (getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken", "") != "") {

                getLocationPermission();

            } else {
                boolean result = isNetworkConnected();
                if (result){
                    Intent intent = new Intent(getContext(), SocialLoginHelperActivity.class);
                    startActivity(intent);
                }else {
                    alertDialogConnectInternet();
                }
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_category_8, container, false);
        recyclerView =  view.findViewById(R.id.RecyclerView_Favorite);


        return view;
    }

    @Override
    public void itemClicked(ModelTypeOfItem type) {
        //Toast.makeText(getContext(),  type.getTypeTH() + " clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void itemClicked(ModelItem item) {
//        Toast.makeText(getContext(),  item.getTopicTH() + " clicked", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getContext(), ItemsDetailActivity.class);
        if (item.getModelTypeOfItem().getTypeID() < 10){
            intent.putExtra("typeItem","Item");
        }else {
            intent.putExtra("typeItem","Emergency");
        }
        intent.putExtra("ITEMS_DETAIL", item);
        startActivityForResult(intent,1);

    }

    @Override
    public void itemClickedFavorite(ModelItem item, ImageView imgFavorite,int position) {

            if (getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserIdToken", "") != "") {
                imgFavorite.setColorFilter(null);
                if (item.isFavorites()){
                    imgFavorite.setColorFilter(getResources().getColor(R.color.darkGrey));
                    item.setFavorites(false);
                }else {
                    imgFavorite.setColorFilter(getResources().getColor(R.color.red));
                    item.setFavorites(true);
                }

                String UserId = getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
                String url_add_favorite = getResources().getString(R.string.host_ip) + "/lanexang_tourism/user/GetData/AddFavorite?typeItem=" + item.getModelTypeOfItem().getTypeID() + "&idUsers=" + UserId + "&idItems=" + item.getItemID() + "";
                new AddFavorite(getContext()).execute(url_add_favorite);
                mAdapter.notifyDataSetChanged();
            }else {
                Intent intent = new Intent(getContext(), SocialLoginHelperActivity.class);
                startActivity(intent);
            }


    }

    @Override
    public void itemClickedUnFavorite(ModelItem item, ImageView image, int position) {

        String UserId = getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID", "");
        String url_add_favorite = getResources().getString(R.string.host_ip) + "/lanexang_tourism/user/GetData/AddFavorite?typeItem=" + item.getModelTypeOfItem().getTypeID() + "&idUsers=" + UserId + "&idItems=" + item.getItemID() + "";
        new AddFavorite(getContext()).execute(url_add_favorite);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void itemClickedUserMatching(ModelItem item) {
//        Toast.makeText(getContext(), item.getTopicTH()+"", Toast.LENGTH_SHORT).show();
        if (item.getModelUsers() != null) {
        Intent intent = new Intent(getContext(), UserMatchingActivity.class);
        intent.putExtra("MODEL_USERS", item.getModelUsers());
        intent.putExtra("MODEL_ITEM",item);
            startActivity(intent);
        }
    }

    @Override
    public void onPreCallService() {
        this.showProgressDialog();
    }

    @Override
    public void onCallService() {

    }

    @Override
    public void onRequestCompleteListener(ArrayList<ModelItem> modelItem) {
        if (modelItem != null && modelItem.size() > 0){
            Log.i("Check data", "" + modelItem.size());
            this.modelItem = modelItem;
            setDataAdapterDragAndDrop();
        }else if(modelItem != null){
            showDataNotFound();
        }
        this.dismissProgressDialog();

    }

    private void setDataAdapterDragAndDrop() {
        String UserId = getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
        mAdapter = new RecyclerViewAdapter(modelItem,this,this,getContext(),UserId);

        ItemTouchHelper.Callback callback = new ItemMoveCallback(mAdapter);
        touchHelper  = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
    }

    private void setDataAdapterGroup() {
        SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(getContext(),
                recyclerView, this, 1);

//        for (ModelTypeOfItem type : modelTypeOfItems){
//            if (type.getModelItems().size() != 0){
//                String imageViewCategoryID = "typeitem_" + type.getTypeID() ;
//                int imageViewCategoryResID = getContext().getResources().getIdentifier(imageViewCategoryID, "drawable", getContext().getPackageName());
//                sectionedExpandableLayoutHelper.addSection(imageViewCategoryResID,type.getTypeTH(),type.getTypeEN(),type.getTypeLAO(), type.getModelItems().size()+" "+getResources().getString(R.string.text_result),type.getModelItems());
//            }else {
//                sectionedExpandableLayoutHelper.removeSection(type.getTypeTH());
//            }
//        }
//        sectionedExpandableLayoutHelper.notifyDataSetChanged();
    }



    @Override
    public void onRequestFailed(String result) {
        this.dismissProgressDialog();
    }


    public void showProgressDialog(){
        alertDialog = new ProgressDialog(getContext());
        alertDialog.setMessage(""+getResources().getString(R.string.text_download_content));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();

        }
    }

    public void showDataNotFound(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(" "+getResources().getString(R.string.text_data_not_found));
        builder.setMessage(""+getResources().getString(R.string.text_back_main));
        builder.setPositiveButton(""+getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            Location locationResult = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationResult == null)
                locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            String UserId = getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
            url_json = "/lanexang_tourism/user/GetData/GetFavorite?idUsers="+UserId+"&Lat="+locationResult.getLatitude()+"&Lng="+locationResult.getLongitude();
            new AsyncTaskFavorites(this).execute(getResources().getString(R.string.host_ip)+url_json);

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION}, 3);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(getContext(), MainActivity.class));
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

                    Location locationResult = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (locationResult == null)
                        locationResult = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    String UserId = getContext().getSharedPreferences("PREF_APP_USER", Context.MODE_PRIVATE).getString("APP_UserID","");
                    url_json = "/lanexang_tourism/user/GetData/GetFavorite?idUsers="+UserId+"&Lat="+locationResult.getLatitude()+"&Lng="+locationResult.getLongitude();
                    new AsyncTaskFavorites(this).execute(getResources().getString(R.string.host_ip)+url_json);

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, 3);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
