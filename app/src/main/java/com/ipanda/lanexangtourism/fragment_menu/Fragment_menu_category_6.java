package com.ipanda.lanexangtourism.fragment_menu;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ipanda.lanexangtourism.ContactListActivity;

import com.ipanda.lanexangtourism.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_category_6 extends Fragment implements LocationListener {

    private TextView textViewNameMenu;
    private String[] NameMenuCategory;

    private CardView list_menu_1,list_menu_2,list_menu_3,list_menu_4;

    protected LocationManager locationManager;
    private String app_mode;

    public Fragment_menu_category_6() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NameMenuCategory = getResources().getStringArray(R.array.NameCategory);

        app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            boolean result = isNetworkConnected();
            if (result){

            }else {
                alertDialogConnectInternet();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_category_6, container, false);
        getLocationPermission();
        textViewNameMenu = (TextView) view.findViewById(R.id.tVNameMenu);
        textViewNameMenu.setText(NameMenuCategory[5]);
        Initializing(view);

        list_menu_1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                actionActivity("travel");
            }
        });
        list_menu_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                actionActivity("emergency");
            }
        });
        list_menu_3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                actionActivity("hospital");
            }
        });
        list_menu_4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                actionActivity("inforcenter");
            }
        });

        return view;
    }

    private void Initializing(View view){
        list_menu_1 = view.findViewById(R.id.travel);
        list_menu_2 = view.findViewById(R.id.emergency);
        list_menu_3 = view.findViewById(R.id.hospital);
        list_menu_4 = view.findViewById(R.id.inforcenter);


    }

    private void actionActivity(String page){
        Intent intent = new Intent(getActivity(), ContactListActivity.class);
        intent.putExtra("select", page);
        startActivity(intent);
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.CALL_PHONE}, 2);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
