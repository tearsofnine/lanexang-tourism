package com.ipanda.lanexangtourism.fragment_menu;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ipanda.lanexangtourism.InformationProvinceActivity;
import com.ipanda.lanexangtourism.R;
import com.ipanda.lanexangtourism.adapter.AdapterInformationCountry;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_category_7 extends Fragment {

    private ListView mListView;
    private String[] img_country = { "bg_th", "bg_la"};
    private String app_mode;

    public Fragment_menu_category_7() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app_mode = getContext().getSharedPreferences("PREF_APP_MODE", Context.MODE_PRIVATE).getString("APP_MODE","");
        if (app_mode.equals("offline")){
            alertDialogCheckOffLineMode();
        }else {
            boolean result = isNetworkConnected();
            if (result){

            }else {
                alertDialogConnectInternet();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_category_7, container, false);

        mListView = (ListView) view.findViewById(R.id.CountryList);

        AdapterInformationCountry aAdapter =  new AdapterInformationCountry(this.getActivity(),img_country);

        mListView.setAdapter(aAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), InformationProvinceActivity.class);
                intent.putExtra("selectCountry",  (String) mListView.getItemAtPosition(position));
                intent.putExtra("selectPosition",  position);
                startActivity(intent);
            }
        });

        return view;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogConnectInternet(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_cannot_connect_internet));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertDialogCheckOffLineMode(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
        builder.setMessage("    "+getResources().getString(R.string.text_check_mode));
        builder.setPositiveButton(getResources().getString(R.string.text_ok_mode), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
